---
tags: [System, AIX]
title: Limits
---

# AIX - Limits

## Commands

Display user's limits:

```bash
ulimit -a
```

## Procedures

### Modify limit

Edit file `/etc/security/limits`.

Example:

```ini
# * Sizes are in multiples of 512 byte blocks, CPU time is in seconds
# *
# * fsize      - soft file size in blocks
# * core       - soft core file size in blocks
# * cpu        - soft per process CPU time limit in seconds
# * data       - soft data segment size in blocks
# * stack      - soft stack segment size in blocks
# * rss        - soft real memory usage in blocks
# * nofiles    - soft file descriptor limit
# * fsize_hard - hard file size in blocks
# * core_hard  - hard core file size in blocks
# * cpu_hard   - hard per process CPU time limit in seconds
# * data_hard  - hard data segment size in blocks
# * stack_hard - hard stack segment size in blocks
# * rss_hard   - hard real memory usage in blocks
# * nofiles_hard - hard file descriptor limit

database:
        fsize = -1
        data = -1
        stack = -1
        rss = -1
        coredump = -1
        nofiles = -1
```

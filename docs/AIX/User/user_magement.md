---
tags: [System, AIX]
title: User/Group
---

# AIX - User/Group

## Files

|File |Description
|--- |---
|`/etc/passwd` |Contains the basic attributes of users.
|`/etc/security/user` |Contains the extended attributes of users.
|`/etc/security/user.roles` |Contains the administrative role attributes of users.
|`/etc/security/limits` |Defines resource quotas and limits for each user.
|`/etc/security/environ` |Contains the environment attributes of users.
|`/etc/security/audit/config` |Contains audit configuration information.
|`/etc/security/lastlog` |Contains the last login attributes of users.
|`/etc/group` |Contains the basic attributes of groups.
|`/etc/security/group` |Contains the extended attributes of groups.
|`/etc/security/passwd` |Contains the user password.

## Commands

### User

Create user:

```bash
mkuser \
  id='{uid}' \
  admin='[true|false]' \
  pgrp='{group}' \
  home='{home-dir}' \
  shell='{shell}' \
  {user}
# Ex: mkuser id='1001' admin='false' pgrp='lkone' home='/home/lkone' shell='/usr/bin/ksh' lkone
```

Change user's password:

```bash
echo "{user}:{passwd}" | chpasswd -c
# Ex: echo "lkone:xxxXXXxxx" | chpasswd -c
```

Delete user:

```bash
userdel (-r) {user}
# -r: To delete home dir
```

### Group

Create group:

```bash
mkgroup -A id={gid} {group}
# Ex: mkgroup -A id=30 dba
```

## Procedures

### Problem with a user

Start `pwdck` command:

```bash
pwdck -y {user}
```

Add line `registry=files` in this file:

```bash
vi /etc/security/user
```


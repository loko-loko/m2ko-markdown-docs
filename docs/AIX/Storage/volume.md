---
tags: [System, AIX]
title: Volume
---

# AIX - Volume

## Commands

Display attribute of a volume:

```bash
lsattr -El {hdisk}
# Ex: lsattr -El hdisk1
```

Read vgda table of a volume:

```bash
readvgda {hdisk}
```

Modify attributes of a volume:

```bash
chdev {hdisk} -a {attr}={value}
# Ex: chdev hdisk1 -a reserve_policy=no_reserve
```

Create or delete hdisk pvid:

```bash
chdev -l {hdisk} -a pv=[yes|clear]
# Ex:
#   Create PVID:
#     chdev -l hdisk1 -a pv=yes
#   Clean PVID:
#     chdev -l hdisk1 -a pv=clear
```

Display volume's path:

```bash
lspath -l {hdisk} (-F {attr})
# Ex: lspath -l hdisk1
#     lspath -l hdisk1 -F "connection:parent:path_status:status"
```

Switch path to defined or delete:

```bash
rmpath -l {hdisk} -p {vscsi} (-w {id})
# Ex: rmpath -l hdisk1 -p vscsi1
#     rmpath -l hdisk1 -p vscsi1 -w 830000000000
```

Delete path:

```bash
rmpath -dl {hdisk} -p {vscsi} (-w {id})
# Ex: rmpath -dl hdisk1 -p vscsi1
#     rmpath -dl hdisk1 -p vscsi1 -w 830000000000
```

Activate path:

```bash
chpath -l {hdisk} -p {vscsi} (-w {id}) -s enable
# Ex: chpath -l hdisk1 -p vscsi0 -s enable
#     chpath -l hdisk1 -p vscsi0 -w 810000000000 -s enable
```

## Scripts

Switch from `failed` to `enabled` on all failed path:

```bash
lspath -F"name connection parent path_status status" | grep Failed | while read hd conn fc pathstat stat; do
  chpath -l $hd -p $fc -w "$conn" -s enabled
done
```
---
tags: [System, AIX]
title: FC Card
---

# AIX - FC Card

## Commands

Scanner nouvelle device:

```
cfgmgr
```

Afficher d&eacute;tail Carte FC:

```
lscfg -vl fcs0
```

Attribut Carte FC:

```
lsattr -El fscsi0
```

Afficher option disponible pour une variable:

```
lsattr -Rl fscsi0 -a fc_err_recov
```

Modifier Attribut Carte FC (Option `-P`: prend en compte au prochain red&eacute;marrage):

```
chdev -l fscsi0 -a fc_err_recov=fast_fail -P
chdev -l fscsi0 -a dyntrk=yes -P
```

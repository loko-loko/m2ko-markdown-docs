---
tags: [System, AIX]
title: LVM
---

# AIX - LVM

## Commands

### Volume Group (VG)

Display VG's PV:

```bash
lsvg -p {vg}
```

Display VG's LV:

```bash
lsvg -l {vg}
```

Create VG:

```bash
mkvg -y {vg} -s {pp-size} {hdisk (hdisk)}
# Ex: mkvg -y vg_softwares -s 64 hdisk1 hdisk2
```

Save VG structure:

```bash
savevg -r -f /path/to/backup/{vg}.bak {vg}
```

Restore VG:

```bash
restvg -r -f /path/to/backup/{vg}.bak {hdisk}
```

VG resize (After block volume resize):

```bash
chvg -g {vg}
```

Import VG from a volume:

```bash
importvg -y {vg} {hdisk}
# Ex: importvg -y vg_soft hdisk2
```

### Logical Volume (LV)

Display LV map:

```bash
lslv -m {lv}
```

Create LV:

```bash
mklv -t jfs2 -s y (-c {copy}) -y {lv} {vg} {size} ({hdisk (hdisk)})
# Ex: mklv -t jfs2 -s y -y lv_oracle vg_softwares 50G
```

Switch LV to spread mode (Write to all disks):

```bash
chlv -e x {lv} 
```

Changing LV's MAX LPs:

```bash
chlv -x 20000 {lv}
```

Display LV attribute:

```bash
lsattr -El {lv}
```

### Filesystem (FS)

Display Filesystems in order of filling:

```bash
df -g | sort -nk4
```

Create FS:

```bash
crfs -v jfs2 -d {lv} -m {fs} (-a logname=INLINE) -A yes
# Ex: crfs -v jfs2 -d lv_oracle -a logname=INLINE -m /softwares/oracle -A yes
```

Increase FS size:

```bash
chfs -a size=+1G {fs}
# Ex: chfs -a size=+1G /tmp
```

Delete FS:

```bash
rmfs {fs}
# Ex: rmfs /app/data
```

Change FS Options:

```bash
chfs -a {attr} {fs}
# Examples:
#   chfs -a options=ro /tmp
#   chfs -a size=+2G /tmp
```

## Scripts

List distribution of map volumes by LV:

```bash
lslv -m {lv} | awk '! /^LP|:/ {print $NF}' | sort | uniq -c
```

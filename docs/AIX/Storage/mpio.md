---
tags: [System, AIX, Multipath]
title: MPio
---

# AIX - Mpio

## Commands

### Display

Display paths status:

```bash
lsmpio
# Similar to: lspath -F "path_id status parent connection"
```

Display paths status of a volume:

```bash
lsmpio -l <hdisk>
# Ex: lsmpio -l hdisk3
```

Display Mpio stats of a volume:

```bash
lsmpio -Sdl <hdisk>
# Ex: lsmpio -Sdl hdisk3
```

Display adapter info:

```bash
lsmpio -ar
```

Display Mpio device info:

```bash
lsmpio -ql <hdisk>
# Ex: lsmpio -ql hdisk48
```


Display local and remote port error counts:

```bash
lsmpio -are
```

Display device priority:

```bash
lspath -l <hdisk> -a priority -i <id> -F value
# Ex: lspath -l hdisk1 -a priority -i 6 -F value
```

### Actions

Changing path state to `enabled`:

```bash
chpath -s enabled -l <hdisk> -p <parent>
# Ex: chpath -s enabled -l hdisk1 -p vscsi0
```

Change prority of a path (Max: `255`, Min `0`):

```bash
chpath -l <hdisk> -a priority=<prority> -p <parent> -w <wwn>
# Ex: chpath -l hdisk1 -a priority=255 -p fscsi0 -w 20080022a10bb2d5,1000000000000
```

Delete Path:

```bash
rmpath -dl <hdisk> -p <parent> -w <wwn>
# Ex: rmpath -dl hdisk1 -p fscsi0 -w 5000097598088186,3000000000000
```


## Links

- http://aix4admins.blogspot.com/2011/05/mpio-included-as-part-of-aix-at-no.html
- https://www.ibm.com/docs/en/aix/7.2?topic=l-lsmpio-command

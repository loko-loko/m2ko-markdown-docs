---
tags: [System, AIX]
title: FileSystem
---

# AIX - FileSystem

## Commands

List all FS with their options:

```bash
lsfs -q
```

Display FS's definitions:

```bash
cat /etc/filesystems
```

Change FS Options:

```bash
chfs -a {attr} {fs}
# Examples:
#   chfs -a options=ro /tmp
#   chfs -a size=+2G /tmp
```

## Procedures

### Remove NOAC Option on Network FS (NFS)

```bash
# Retrieve FS info
grep -p "/stor/save" /etc/filesystems

# Change FS Option
chfs -a options=rw,bg,hard,nointr,rsize=32768,wsize=32768,timeo=600,proto=tcp,sec=sys /stor/save

# Ensure there are no process
fuser /stor/rrzxx1/save
lsof /stor/rrzxx1/save

# Re-mount FS
umount /stor/rrzxx1/save && mount /stor/rrzxx1/save
```

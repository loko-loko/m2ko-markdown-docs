---
tags: [System, AIX, Perl]
title: Perl
---

# AIX - Perl

## CPAN

### Basics

Start interactive cpan shell

```bash
perl -MCPAN -e shell
```

Update CPAN:

```bash
perl -MCPAN -e 'install CPAN'
```

### Install Oracle Module

```bash
# Export proxy (if needed)
export http_proxy=http://myproxy:8080
export https_proxy=http://myproxy:8080
export no_proxy=.m2ko.net

export LDR_CNTRL=MAXDATA=0x80000000@DSA
export PERL_MM_USE_DEFAULT=1

perl -MCPAN -e 'get DBI'
perl -MCPAN -e 'get DBD::Oracle'

cd /root/.cpan/build/DBD-Oracle-1.83-0

perl Makefile.PL

make
make test
make install
```

## Links

- http://www.cpan.org/modules/by-module/


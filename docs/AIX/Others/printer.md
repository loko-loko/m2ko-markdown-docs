---
tags: [System, AIX]
title: Printers
---

# AIX - Printers

## Commands

Start printer smitty menu:

```bash
smitty spooler
```

List printer services:

```bash
lssrc -g spooler
```

## Spool directories

The following directories may need to be accessed to clean up aborted jobs:

- `/var/spool/lpd/qdir`: Accepts requests for print jobs
- `/var/spool/lpd/stat`: Status files for a print job. The backend communicates with the qdaemon through these files.
- `/var/spool/lpd/spool`: qdaemon requests are spooled here

The following directories are used to store virtual printer attributes:

- `/var/spool/lpd/pio/burst`: Stores header/trailer format page
- `/var/spool/lpd/pio/predef`: Default attributes
- `/var/spool/lpd/pio/custom`: ASCII virtual printer attributes
- `/var/spool/lpd/pio/ddi`: Digested virtual printer attributes

---
tags: [System, AIX]
title: Mail
---

# AIX - Mail

## Sendmail

Basic configuration of `/etc/sendmail.cf`:

```ini
# "Smart" relay host (may be null)
DS mail-server

# privacy flags
O PrivacyOptions=authwarnings noexpn novrfy
```

Reload sendmail service:

```bash
refresh -s sendmail
```

Start sendmail service:

```bash
startsrc -s sendmail -a "-bd -q30m"
```

Stop sendmail service:

```bash
stopsrc -s sendmail
```
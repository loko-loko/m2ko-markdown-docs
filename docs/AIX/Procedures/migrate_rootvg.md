---
tags: [System, AIX]
title: Migrate rootvg
---

# AIX - Migrate rootvg

## With migratepv

Voici une petit proc&eacute;dure pour migrer un volume rootvg du `hdisk0` vers le `hdisk1`.

Tout d&#39;abord il faut ajouter le volume cible au VG du `rootvg`:

```
extendvg rootvg hdisk1
```

Puis migrer le volume source vers le cible:

```
migratepv hdisk0 hdisk1
```

Une fois migrer retirer le volume source du VG `rootvg`:

```
reducevg rootvg hdisk0
```

Maintenant que le volume du rootvg est migr&eacute;, nous pouvons cr&eacute;er la `bootlist` sur le nouveau volume:

```
bosboot -ad /dev/hdisk1
```

Puis changer l&#39;ordre de boot:

```
bootlist -om normal hdisk1
```

Il ne reste plus qu&#39;a v&eacute;rifier l&#39;&eacute;at de la `bootlist` avec la commande suivante:

```
bootlist -om normal
# hdisk1 blv=hd5
```

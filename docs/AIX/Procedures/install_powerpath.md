---
tags: [System, Storage, EMC, AIX]
title: Install PowerPath
---

# AIX - Install PowerPath

## Package ODM

### Vérification

Il est nécessaire de vérifier la version des packages ODM avant de lancer l'installation de Powerpath. Pour ce faire lancer la commande suivante:

```
# lslpp -l | grep 'EMC\.'
  EMC.CLARiiON.aix.rte       5.3.1.0  COMMITTED  EMC CLARiiON AIX Support
  EMC.CLARiiON.fcp.rte       5.3.1.0  COMMITTED  EMC CLARiiON FCP Support
  EMC.Symmetrix.aix.rte      5.3.1.0  COMMITTED  EMC Symmetrix AIX Support
  EMC.Symmetrix.fcp.rte      5.3.1.0  COMMITTED  EMC Symmetrix FCP Support
  EMC.XtremIO.aix.rte        5.3.1.0  COMMITTED  EMC XtremIO AIX Support
  EMC.XtremIO.fcp.rte        5.3.1.0  COMMITTED  EMC XtremIO FCP Support
```

Dans le cas o&ugrave; la version d'ODM ne correspondrait pas au standard préconisé par le constructeur (Voir matrice de compatibilité sur [https://support.emc.com](https://support.emc.com)), il sera nécessaire de mettre à jours les packages ODM.

### Installation

Dans notre cas nous allons passer de la version 5.3.1.0 à la version 5.3.1.2. Pour ce faire télécharger les packages ODM à l'adresse suivante:

[ftp://ftp.EMC.com/pub/elab/aix/ODM_DEFINITIONS/](ftp://ftp.EMC.com/pub/elab/aix/ODM_DEFINITIONS/)

Puis suivre la procédure d'installation suivante:

*   Se logger sur AIX en root
*   Copier le package `EMC.AIX.5.3.1.2` dans le répertoire &quot;`/usr/sys/inst.images`&quot;
*   Lancer la commande  `inutoc .`
*   Lancer la commande `smit installp`
*   Sélectionner Option : `Install and Update from LATEST Available Software`
*   Utiliser la fonction lister pour choisir `/usr/sys/inst.images` comme répertoire d'installation.
*   Utiliser la fonction lister pour sélectionner les packages ODM à installer :

```
[...]
EMC Symmetrix AIX Support Software;  Standard utility support.
EMC Symmetrix FCP Support Software; IBM Fibre Channel driver support.(Supports PowerPath)
EMC CLARiiON AIX Support Software; Standard utility support.
EMC CLARiiON FCP Support Software; IBM Fibre Channel driver support.(Supports PowerPath)
EMC XtremIO AIX Support Software; Standard utility support.
EMC XtremIO FCP Support Software; IBM Fibre Channel driver support. (Supports PowerPath)
[...]
```

Une fois toutes les étapes terminées, ce message devrait apparaitre sur le terminal :

```
+-----------------------------------------------------------------------------+
                    Pre-installation Verification...
+-----------------------------------------------------------------------------+
Verifying selections...done
Verifying requisites...done
Results...

SUCCESSES
---------
  Filesets listed in this section passed pre-installation verification
  and will be installed.

  Selected Filesets
  -----------------
  EMC.CLARiiON.aix.rte 5.3.1.2                # EMC CLARiiON AIX Support Sof...
  EMC.CLARiiON.fcp.rte 5.3.1.2                # EMC CLARiiON FCP Support Sof...
  EMC.Symmetrix.aix.rte 5.3.1.2               # EMC Symmetrix AIX Support So...
  EMC.Symmetrix.fcp.rte 5.3.1.2               # EMC Symmetrix FCP Support So...
  EMC.XtremIO.aix.rte 5.3.1.2                 # EMC XtremIO AIX Support Soft...
  EMC.XtremIO.fcp.rte 5.3.1.2                 # EMC XtremIO FCP Support Soft...

  << End of Success Section >>

+-----------------------------------------------------------------------------+
                   BUILDDATE Verification ...
+-----------------------------------------------------------------------------+
Verifying build dates...done
FILESET STATISTICS
------------------
    6  Selected to be installed, of which:
        6  Passed pre-installation verification
  ----
    6  Total to be installed

installp:  bosboot verification starting...
installp:  bosboot verification completed.
+-----------------------------------------------------------------------------+
                         Installing Software...
[MORE...125]
```

L'installation des nouveaux packages ODM est maintenant terminée.

## Attributs Cartes FC

### Vérification

Il est nécessaire de vérifier la présence de plusieurs attributs sur les adaptateurs FC (`fcsn`) et SCSI (`fscsin`) connectés :


|**Attribut** |**Type** |**Valeur**
|--- |--- |---
|fc_err_recov |fscsi |fast_fail
|max_xfer_size |fcs |0x100000
|num_cmd_elems |fcs |2048
|dyntrk |fscsi |yes

Pour vérifier que ces attributs sont bien aux bonnes valeurs, il faut tout d'abord récupérer les ids des adaptateurs FC/SCSI:

```
# lsdev | grep -E '(fcs|fscsi)[0-9]+'
fcs0           Available C7-T1       Virtual Fibre Channel Client Adapter
fcs1           Available C8-T1       Virtual Fibre Channel Client Adapter
fscsi0         Available C7-T1-01    FC SCSI I/O Controller Protocol Device
fscsi1         Available C8-T1-01    FC SCSI I/O Controller Protocol Device
```

Puis lancer la commande suivante sur chaque adaptateur FC:

```
# lsattr -El fcs0
intr_priority 3        Interrupt priority                 False
lg_term_dma   0x800000 Long term DMA                      True
max_xfer_size 0x100000 Maximum Transfer Size              True
num_cmd_elems 2048     Maximum Number of COMMAND Elements True
sw_fc_class   2        FC Class for Fabric                True
```

Et SCSI:

```
# lsattr -El fscsi0
attach       switch    How this adapter is CONNECTED         False
dyntrk       yes       Dynamic Tracking of FC Devices        True
fc_err_recov fast_fail FC Fabric Event Error RECOVERY Policy True
scsi_id      0x1bee0a  Adapter SCSI ID                       False
sw_fc_class  3         FC Class for Fabric                   True
```

### Modification

Dans le cas ou l'attribut n'aurait pas la bonne valeur, la commande suivante permet de modifier l'attribut:

```
# chdev -l fscsi0 -a fc_err_recov=fast_fail –P
```

Apr&egrave;s toutes modifications, redémarrer le host:

```
# shutdown -Fr
```

# Installation

## Lancement

Une fois toutes les vérifications effectuées, nous pouvons lancer l'installation des packages PowerPath. Tout d'abord décompresser le fichier tar.gz:

```
# gzip -d EMCPower.AIX.6.2.b071.tar.gz
# tar xvf EMCPower.AIX.6.2.b071.tar
x EMCpower_install, 42342400 bytes, 82700 tape blocks
#
# ls -l
-rw-r--r--    1 29080    43093      42342400 Sep  8 00:53 EMCpower_install
```

Lancer l'installation du Package:

```
# installp -agXd . EMCpower
+-----------------------------------------------------------------------------+
                    Pre-installation Verification...
+-----------------------------------------------------------------------------+
Verifying selections...done
Verifying requisites...done
Results...

SUCCESSES
---------
  Filesets listed in this section passed pre-installation verification
  and will be installed.

  Selected Filesets
  -----------------
  EMCpower.MgmtComponent 6.2.0.0              # PowerPath Management Compone...
  EMCpower.base 6.2.0.0                       # PowerPath Base Driver and Ut...
  EMCpower.migration_enabler 6.2.0.0          # PowerPath Migration Enabler ...
  EMCpower.mpx 6.2.0.0                        # PowerPath Multi_Pathing Exte...

  << End of Success Section >>

+-----------------------------------------------------------------------------+
                   BUILDDATE Verification ...
+-----------------------------------------------------------------------------+
Verifying build dates...done
FILESET STATISTICS
------------------
    4  Selected to be installed, of which:
        4  Passed pre-installation verification
  ----
    4  Total to be installed

+-----------------------------------------------------------------------------+
                         Installing Software...
+-----------------------------------------------------------------------------+

installp: APPLYING software for:
        EMCpower.base 6.2.0.0
        EMCpower.mpx 6.2.0.0
        EMCpower.migration_enabler 6.2.0.0
        EMCpower.MgmtComponent 6.2.0.0

. . . . . << Copyright notice for EMCpower >> . . . . . . .
Copyright (c) 2017 EMC Corporation.  All rights reserved.
All trademarks used herein are the property of their respective owners.

License registration is NOT required to manage the CLARiiON AX series array.

Build 071
. . . . . << End of copyright notice for EMCpower >>. . . .

0518-307 odmdelete: 0 objects deleted.
Finished processing all filesets.  (Total time:  8 secs).

+-----------------------------------------------------------------------------+
                                Summaries:
+-----------------------------------------------------------------------------+

Installation Summary
--------------------
Name                        Level           Part        Event       Result
-------------------------------------------------------------------------------
EMCpower.base               6.2.0.0         USR         APPLY       SUCCESS
EMCpower.mpx                6.2.0.0         USR         APPLY       SUCCESS
EMCpower.migration_enabler  6.2.0.0         USR         APPLY       SUCCESS
EMCpower.MgmtComponent      6.2.0.0         USR         APPLY       SUCCESS
```

# Post Installation

## Vérification

### Packages PP

Une fois l'installation terminée, nous pouvons vérifier que les nouveaux packages PowerPath sont bien prises en compte:

```
# lslpp -l | grep EMCpower
  EMCpower.MgmtComponent     6.2.0.0  COMMITTED  PowerPath Management
  EMCpower.base              6.2.0.0  COMMITTED  PowerPath Base Driver and
  EMCpower.migration_enabler
  EMCpower.mpx               6.2.0.0  COMMITTED  PowerPath Multi_Pathing
```


### Découverte des Volumes

Nous pouvons maintenant lancer un scan du hardware puis vérifier que nos devices remontent correctements sur le serveur:

```
# cfgmgr
# powermt display
Symmetrix logical device count=9
==============================================================================
----- Host Bus Adapters ---------  ------ I/O Paths -----  ------ Stats ------
###  HW Path                       Summary   Total   Dead  IO/Sec Q-IOs Errors
==============================================================================
   0 fscsi0                        optimal      18      0       -     0      0
   1 fscsi1                        optimal      18      0       -     0      0

# powermt display paths
Symmetrix logical device count=9
==============================================================================
----- Host Bus Adapters --------- ------ Storage System -----    - I/O Paths -
###  HW Path                         ID           Interface     Total    Dead
==============================================================================
   0 fscsi0                       000295700055     FA 12h:01          9       0
   0 fscsi0                       000295700055     FA  5h:01          9       0
   1 fscsi1                       000295700055     FA 11h:00          9       0
   1 fscsi1                       000295700055     FA  6h:00          9       0

# lspv | grep power
hdiskpower0     00c8c3fea7ae6103                    None
hdiskpower1     00c8c3fe57c23c77                    None
hdiskpower2     00c8c3fe57cac86b                    None
hdiskpower3     00c8c3fe57c3d556                    None
...
```

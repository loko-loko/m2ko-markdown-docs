---
tags: [System, AIX, Management]
title: HMC
---

# AIX - HMC

## Commands

### Interaction

Start console on a partition:

```bash
mkvterm -m {frame} -p {vioc|s}
```

Kill console terminal:

```bash
rmvterm -m {frame} -p {vioc|s}
```

Boot partition to SMS menu:

```bash
chsysstate -m {frame} -r lpar -o on -f default_profile -b sms -n {vioc|s}
```

Stop partition:

```bash
chsysstate -m {frame} -r lpar -o shutdown -n {vioc|s}
```

Save profile:

```bash
mksyscfg -m {frame} -r prof -o save -p {vioc|s} -n default_profile --force
```

### Information

Display HMC managed frame:

```bash
lssyscfg -r sys -F name
```

Display partition list:

```bash
lssyscfg -m {frame} -r lpar -F name,lpar_id,state
```

Display info of a partition:

```bash
lssyscfg -m {frame} -r lpar --filter "lpar_names={vioc|s}"
```

List virtual adapters of a partition:

```bash
lshwres -m {frame} -r virtualio --rsubtype slot --level slot --filter "lpar_names={vioc|s}"
```

List detail of VSCSI adapters of a partition:

```bash
lshwres -m {frame} -r virtualio --rsubtype scsi --level lpar --filter "lpar_names={vioc|s}"
```

List detail of FC adapters of a partition:

```bash
lshwres -m {frame} -r virtualio --rsubtype fc --level lpar --filter "lpar_names={vioc|s}"
```

Display state of a partition:

```bash
lssyscfg -m {frame} -r lpar -F name:state | grep {vioc|s}
```

Display profile name of one or all partitions:

```bash
lssyscfg -m {frame} -r prof --filter lpar_names={vioc|s} | cut -d, -f1
lssyscfg -m {frame} -r prof | cut -d, -f1,2
```

List NPIV WWN of a partition: 

```bash
lshwres -m {frame} -r virtualio --rsubtype fc --filter lpar_names={vioc} --level lpar -F lpar_name,slot_num,wwpns --header
```

### Creation

Create virtual SCSI adapter:

```bash
chhwres -m {frame} -r virtualio --rsubtype scsi -a "adapter_type=server,remote_lpar_name={vioc},remote_slot_num=3" -o a -p {vios} -s 149
```

Add virtual SCSI adapter in profile:

```bash
chsyscfg -m {frame} -r prof -i lpar_name={vioc},name=default_profile,"virtual_scsi_adapters+={adapter id}/client//{vios}/{remote adapter id}/0"
```

Create profile:

```bash
mksyscfg -m {frame} -r lpar -i name=vioc_name,profile_name=default_profile,lpar_env=aixlinux,min_mem=1024,desired_mem=8192 ,max_mem=24576,proc_mode=shared,desired_procs=1,min_procs=1,max_procs=64,min_proc_units=0.1,desired_proc_units=0.3,max_proc_units=64,sharing_mode=uncap,uncap_weight=128,boot_mode=norm
```

### Deletion

Delete partition:

```bash
rmsyscfg -m {frame} -r lpar --id 2
```

Delete Virtual SCSI adapter:

```bash
chhwres -m {frame} -r virtualio -o r -p "{vios}" --rsubtype scsi -s {scsi id}
```

Delete Virtual FC adapter:

```bash
chhwres -m {frame} -r virtualio -o r -p "{vios}" --rsubtype fc -s {fc id}
```

## API

### Login to HMC

Create file with HMC credentials:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<LogonRequest xmlns="http://www.ibm.com/xmlns/systems/power/firmware/web/mc/2012_10/" schemaVersion="V1_0">
  <UserID>hscroot</UserID>
  <Password>@password</Password>
</LogonRequest>
```

Run this command to login:

```bash
curl -i -X PUT -k \
  -c cookies.txt \
  -H "Content-Type: application/vnd.ibm.powervm.web+xml; type=LogonRequest" \
  -H "Accept: application/vnd.ibm.powervm.web+xml; type=LogonResponse" \
  -H "X-Audit-Memento: hmc_test" \
  -d @login.xml https://hmc.local.net:12443/rest/api/web/Logon
```

### Requests

Get managed system:

```bash
curl -X GET -k \
  -c cookies.txt \
  -b cookies.txt \
  -H "Accept: application/atom+xml; charset=UTF-8" \
  https://hmc.local.net:12443/rest/api/uom/ManagedSystem
```

## Procedures

### NPIV Card

Test NPIV card:

```bash
chsysstate -m {frame} -r lpar -o on -b of -n {vioc} -f default_profile
0 } ioinfo
# -} 6
# -} 1
```

Activate NPIV card:

```bash
chsysstate -m {frame} -r lpar -o shutdown -n {vioc}
chsysstate -m {frame} -r lpar -o on -b of -n {vioc} -f default_profile
chnportlogin -m {frame} -o login -p {vioc}
lsnportlogin -m {frame} --filter lpar_names={vioc}
```
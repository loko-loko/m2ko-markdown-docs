---
tags: [System, AIX, Management]
title: Vios
---

# AIX - Vios

## Commands

### Globals

Get OS Version:

```bash
ioslevel
```

### Vhost/VTD

Display detail of all vhosts:

```bash
lsmap -all
```

Display vhost detail:

```bash
lsmap -vadapter {vhost}
# Ex: lsmap -vadapter vhost1
```

Display detail of all vhosts with formatting of output:

```bash
lsmap -all -field vtd svsa physloc -fmt :
```

Retrieve vhost with VTD alias:

```bash
lsdev -dev *alias* -parent
```

Display correspondence between vhost/physical address:

```bash
lsdev -slots
```

Create VTD:

```bash
mkvdev -vdev {hdisk} -vadapter {vhost} -dev {dev}
# Ex: mkvdev -vdev hdisk1 -vadapter vhost1 -dev bs_001_server1
```

Delete VTD:

```bash
# Padmin:
rmvdev -vtd {vhost}

# Root:
rmdev -dev {vhost}
```

### NPIV

Delete NPIV device:

```bash
lsmap -all -npiv
rmdev -dev {vfchost}
# Ex: rmdev -dev vfchost1
```

Map NPIV adapter:

```bash
vvfcmap –vadpater {vfchost} –fcp {fc-adapter}
# Ex: vvfcmap –vadpater vfchost0 –fcp fcs0
```

## Scripts​

Display last VTD of vhost:

```bash
lsmap -vadapter {vhost} | grep VTD | sed -n '$p'
```

Generate commands to map volumes:

```bash
vol_lst="hdiskpower1 hdiskpower2 hdiskpower3"
server_name='server1'
vhost_name='vhost1'

vtd_cnt=0
for vol in $vol_lst; do
    printf "mkvdev -vdev %s -vadapter %s -dev vol_%03d_%s\n" $vol $vhost_name $vtd_cnt $server_name
    vtd_cnt=$(expr $vtd_cnt + 1)
done

# mkvdev -vdev hdiskpower1 -vadapter vhost1 -dev vol_000_server1
# mkvdev -vdev hdiskpower2 -vadapter vhost1 -dev vol_001_server1
# mkvdev -vdev hdiskpower3 -vadapter vhost1 -dev vol_002_server1
```

---
tags: [System, AIX]
title: Memory/Swap
---

# AIX - Memory/Swap

## Commands

### Memory

Display total memory:

```bash
bootinfo -r
```

Display system memory:

```bash
lsattr -El mem0
```

### Paging Space (Swap)

Display current amount of paging space available:

```bash
lsps -a
```

Display total paging space:

```bash
lsps -s
```

Delete paging space:

```bash
rmps {ps}
```

Increase PS size:

```bash
chps -s {pp-count} {ps}
# Ex: chps -s 10 hd6 -> Increase of 10 pp
```

Decrease PS size:

```bash
chps -d {pp-count} {ps}
# Ex: chps -d 10 hd6 -> Decrease of 10 pp
```

## Procedures

Increase/Decrease paging space:

```bash
# Display paging space logical partition info
lsps -a

# Determine the size of a logical partition
lslv {ps}
# Ex: lslv hd6 -> pp size: 128 mb

# Increase PS:
chps -s {pp-count} {ps}
# Ex: chps -s 10 hd6 -> Increase of 10 pp of 128 mb

# Increase PS:
chps -d {pp-count} {ps}
# Ex: chps -s 10 hd6 -> Decrease of 10 pp of 128 mb
```
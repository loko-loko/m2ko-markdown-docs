---
tags: [System, AIX]
title: Performance
---

# AIX - Performance

## Commands

### Common

Analyse dynamique des ressources systèmes:

```bash
nmon
```

Affichage de la consommation des ressources (mem,cpu,io, ...):

```bash
vmstat -w 1
```

Visualisation de l’activité du système:

```bash
topas
```

Liste des processus triée par consommation CPU et mémoire:

```bash
ps ux
```

### CPU

Liste la charge CPU et mémoire toutes les `d` secondes, `t` fois:

```bash
vmstat [opt] d [t]
# Example:
#   vmstat -Iwt 2
#     -I: Display I/O oriented view
#     -w: Wide mode
#     -t: Display timestamp
```

Informations sur les processus en cours:

```bash
pstat -a
```

Gestion du traitement Multi-Thread.Activation,/Désactivation:

```bash
# Display status
smtctl

# Enable SMT
smtctl -m on -w now

# Disable SMT
smtctl -m off
```

Gestion des processeurs physiques et virtuels:

```bash
bindprocessor
```

Analyse de performance:

```bash
tprof
```

Listage de la consommation CPU:

```bash
# Global
sar -u 2 10

# Per CPU
sar -u -P ALL 5 2
```

Descendance d'un processus donné en argument:

```bash
proctree
```

Suivi de l’activité de la partition courante:

```bash
lparstat
```

Suivi de l’activité des processeurs logiques:

```bash
mpstat
```

Lister les Locks en cours:

```bash
splat
```

### Disk

Listage dynamique toutes les 2 secondes de l’activité disque du système courant:

```bash
iostat -d 2
```

Listage de l’activité disque 5 fois, toutes les 2 secondes:

```bash
sar -d 2 5
```

Suivi de l’activité des disques, volumes logiques ou segments de mémoire virtuelle:

```bash
# Start trace
filemon

# Stop trace
trcstop
```

Suivi de l’emplacement d’un fichier sur un disque:

```bash
fileplace <file_path>
```

Process d'un fichier ouvert:

```bash
fuser -u <file_path>
```

Suivi de l’activité dans un groupe de volumes:

```bash
# Enable stats
lvmstat (-v <vg>|-l <lv>) -e

# Disable stats
lvmstat  (-v <vg>|-l <lv>) -d

# Start stats
lvmstat  (-v <vg>|-l <lv>) <interval> [<count>]

# Clear stats
lvmstat  (-v <vg>|-l <lv>) -C

# Display space reclaim stats
lvmstat  (-v <vg>|-l <lv>) -r

# Examples:
#   lvmstat -v rootvg -e  > Enable stats on rootvg
#   lvmstat -v rootvg 4 3 > Stats toutes les 4 secondes du rootvg (3 fois).
#   lvmstat -v rootvg -d  > Disable stats on rootvg
```

### Memory

Suivi de l’activité mémoire 5 fois toutes les 2 secondes:

```bash
sar -r 2 5
```

Liste des espaces de pagination:

```bash
# List all paging space
lsps

# List all active paging space
lsps -a

# Sum of all paging space
lsps -s
```

Suivi global de la partition, toutes les 2 secondes.

```bash
lparstat -h 2
```

Activité globale de la partition avec 2 colonnes `fre` et `avm`
pour la taille mémoire libre et en moyenne, donnée en bloc de 4k.

```bash
vmstat 2 10
```

Affichage de la consommation mémoire en différentes tailles de bloc.

```bash
svmon
```

---

- [Petit Manuel des Performances AIX](http://ahix.fr/index.php/tous-les-articles/51-produits/systemes-d-exploitation/aix/performances/486-petit-manuel-des-performances-aix)
- [AIX Debugging Commands](https://www.ibm.com/docs/fr/sdk-java-technology/7.1?topic=techniques-aix-debugging-commands)
- [Espace de pagination (Swap)](http://www.linux-france.org/~mdecore/aix/memo-aix/html/sec13.html)



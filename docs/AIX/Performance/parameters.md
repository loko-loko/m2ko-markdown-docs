---
tags: [System, AIX, Parameter]
title: System Parameters
---

# AIX - System Parameters

## Base

Display system parameters:

```bash
lsattr -E -l sys0
```

Change system parameters:

```bash
chdev -l sys0 -a {parameter}={value}
# Ex: chdev -l sys0 -a maxuproc=150
```

Enabling I/O Completion Ports:

```bash
# Get status
lsdev | grep iocp

# Enable:
smitty iocp

# Restart the system to make the changes permanent.
shutdown -Fr
```

## Tunable

Available tunable:

```bash
# Path: /etc/tunables
schedo
vmo
ioo
raso
no
nfso
asoo
```

Display value options:

```bash
{tunable} -a
# Ex: vmo -a
```

Display options details:

```bash
{tunable} -L
# Ex: no -L
```

Display hidden options:

```bash
{tunable} -F
# Ex: no -F
```

Set new value:

```bash
{tunable} -o {option}={value}
# Ex: no -o tcp_keepidle=600
```

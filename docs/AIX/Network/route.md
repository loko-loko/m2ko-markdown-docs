---
tags: [System, AIX, Network]
title: Route
---

# AIX - Route

## Commands

Add route:

```bash
route add -net <ip> <gateway>
# Ex: route add -net 10.10.1.1 192.168.1.1
```

List route of inet0:

```bash
lsattr -El inet0
```

List route with netstat:

```bash
netstat -rn
```

Add default gateway (route):

```bash
chdev -l inet0 -a hostname={hostname} -a route=net,-hopcount,0,,0,{gateway}
# Ex: chdev -l inet0 -a hostname=srv-1 -a route=net,-hopcount,0,,0,10.2.5.1
```

Add route for an given interface:

```bash
# With CIDR
chdev -l inet0 -a route=net,-hopcount,0,-netmask,{mask},-if,{interface},,,,-static,{cidr},{interface-address}

# With specific interface
chdev -l inet0 -a route=host,-hopcount,0,,-if,{interface},,,,-static,{ip-address},{interface-address}

# Examples:
#  With CIDR:
#    chdev -l inet0 -a route=host,-hopcount,0,,-if,en1,,,,-static,10.5.1.0,10.5.2.8
#  With specific interface
#    chdev -l inet0 -a route=net,-hopcount,0,-netmask,255.255.255.192,-if,en1,,,,-static,10.5.1.0,10.5.2.8
```

Delete route:

```bash
chdev -l inet0 -a delroute=net,{route-attribute}

# Examples:
#   Get route from inet0
#     lsattr -El inet0
#   Delete route:
#     chdev -l inet0 -a delroute="net,-hopcount,0,-netmask,255.255.255.192,-if,en1,,,,-static,10.5.1.0,10.5.2.8"
```
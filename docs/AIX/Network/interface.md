---
tags: [System, AIX, Network]
title: Interface
---

# AIX - Interface

## Commands

Config interface with smit:

```bash
smitty tcpip
```

Config network interface:

```bash
chdev -l en0 -a netaddr={ip-address} -a netmask={mask} -a state=up
# Ex: chdev -l en0 -a netaddr=10.2.5.6 -a netmask=255.255.248.0 -a state=up
```

Add default gateway:

```bash
chdev -l inet0 -a hostname={hostname} -a route=net,-hopcount,0,,0,{gateway}
# Ex: chdev -l inet0 -a hostname=srv-1 -a route=net,-hopcount,0,,0,10.2.5.1
```

List route of inet0:

```bash
lsattr -El inet0
```
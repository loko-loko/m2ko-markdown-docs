---
tags: [System, AIX, Network]
title: NFS
---

# AIX - NFS

## Commands

### exportfs

Display exported FS:

```bash
exportfs
```

Export all FS:

```bash
exportfs -a
```

Remove (Unexport) FS:

```bash
exportfs -u {fs}
# Ex: exportfs -u /data/nfs
```

### Services

List NFS services:

```bash
lssrc -g nfs
```

Restart NFS services:

```bash
stopsrc -g nfs; startsrc -g nfs
```

### Mount

Create NFS mount (write in `/etc/filesystems`):

```bash
mknfsmnt \
  -f {local-path} \  # Local path
  -d {nfs-path} \    # NFS path
  -h {nfs-server} \  # NFS server
  -w {bg|fg} \       # Background (bg) or Forground (fg)
  -M {sec-method} \  # Security Method (sys,krb5,...)
  -k {any|tcp|udp}   # Transport Protocol
  -K {2|3|4} \       # NFS Version
  -b {rsize} \       # Buffer Read Size
  -c {wsize} \       # Buffer Write Size
  -o {timeout} \     # Timeout
  -S|-H \            # Softmount|Hardmount
  -A                 # Automount

# Ex: mknfsmnt \
#       -f /data/nfs \
#       -d /nfs_data \
#       -h nfs-srv \
#       -w bg -M sys -K 3 -k tcp \
#       -b 32768 -c 32768 \
#       -o 600 \
#       -S -A
```

Mount NFS share:

```bash
mount {nfs-server}:{nfs-path} {local-path}
# Ex: mount nfs-srv:/data /data
```

## Examples

Export file:

```bash
/data/nfs -anon=0,sec=sys:krb5p:krb5i:krb5:dh,rw
```

NFS share from `/etc/filesystems`:

```bash
/data:
        dev             = "/data"
        vfs             = nfs
        nodename        = nfs-srv
        mount           = true
        options         = bg,soft,intr,sec=sys
        account         = false
```

## Links

- https://www.ibm.com/docs/en/aix/7.2?topic=m-mknfsmnt-command

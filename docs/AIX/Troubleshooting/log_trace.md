---
tags: [System, AIX]
title: Logs/Trace
---

# AIX - Logs/Trace

## Commands

### Logs

Display logs entities:

```bash
alog -L
```

Display system logs

```bash
alog -o -t {entity}
# Examples:
#   alog -o -t cfg (System logs)
#   alog -o -t console (Console logs)
#   alog -o -t boot (Boot logs)
#   alog -o -t lvmt (LVM logs)
```

Read system logs:

```bash
cat /var/adm/messages
# NOTE: Needs to be configured in syslog.conf
```

Clear logs:

```bash
echo "Log Cleared  - $(date)" | alog -t {entity}
# Ex: echo "Log Cleared  - $(date)" | alog -t cfg
```

Change system log verbosity:

```bash
export  CFGLOG=timestamp,detail,verbosity:9
```

### Syslog

Config file:

```bash
/etc/syslog.conf
```

Add redirection (`/etc/syslog.conf`):

```bash
{facility}.{level} {path}
# Ex: auth.debug /tmp/sshd.log
```

### Support

Create a snapshot for support:

```bash
snap -ac
# Output: /tmp/ibmsupt/snap.pax.Z
# NOTE: Rename snap with IBM case (Ex: TS00XXXX.snap.pax.Z)
```

!!! note

    Rename snapshot with IBM case before send it.
    
    Example:
    
    ```bash
    mv snap.pax.Z TS00XXXX.snap.pax.Z
    ```

## Procedures

### Configure syslog for sshd

Configure syslog from sshd config:

```bash
# /etc/ssh/sshd_config
SyslogFacility AUTH
LogLevel INFO
```

Add redirection:

```bash
# /etc/syslog.conf
auth.debug /tmp/sshd.log
```

Refresh syslog daemon:

```bash
refresh -s syslogd
```

Restart ssh daemon:

```bash
stopsrc -s sshd; startsrc -s sshd
```

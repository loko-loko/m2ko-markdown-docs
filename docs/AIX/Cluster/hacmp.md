---
tags: [System, AIX, Cluster]
title: HACMP
---

# AIX - HACMP

## Commands

Display state of cluster:

```bash
clstatus
```

!!! note

    All the following commands are in the following path:
    
    ```bash
    /usr/es/sbin/cluster/sbin
    ```

Create LV:

```bash
cl_mklv -cspoc -n '<servers>' -R '<server>' -y '<lv>' -t '<fs-type>' -d 'ps' -e 'x' <vg> <size> <hdisk [hdisk]>
# Ex: cl_mklv -cspoc -n 'aix1,aix2' -R 'aix1' -y 'lv_test' -t 'jfs2' -d 'ps' -e 'x' vg_test 10G hdisk1
```

Create FS:

```bash
cl_crfs -cspoc -n '<servers>' -v '<fs-type>' -d '<lv>' -m '<mount-point>' -p rw -a <attr>='<value>'
# Ex: cl_crfs -cspoc -n 'aix1,aix2' -v jfs2 -d lv_test -m /data -p rw -a logname='INLINE'
```

Extend VG:

```bash
cl_extendvg -cspoc -n '<servers>' <vg> <hdisk [hdisk]>
# Ex: cl_extendvg -cspoc -n 'aix1,aix2' vg_test hdisk1 hdisk2
```

Change FS attribute:

```bash
cl_chfs -cspoc -n '<servers>' -a <attr>='<value>' <mount-point>
# Ex: cl_chfs -cspoc -n  'aix1,aix2' -a size=+5G /data
```

## Link

- http://www.unixwerk.eu/aix/cluster-cmd.html

---
tags: [System, AIX]
title: Library
---

# AIX - Library

## Commands

List of all loaded libraries:

```bash
genkld
```

Get library of a package (or library):

```bash
dump -X32_64 -Hv {binary}
# Ex: dump -X32_64 -Hv /opt/freeware/bin/curl
#     dump -X32_64 -Hv /opt/freeware/lib64/libcurl.a
```
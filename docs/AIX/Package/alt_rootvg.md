---
tags: [System, AIX]
title: Alt RootVG
---

# Alt RootVG

## Commands

Create alternative rootvg:

```bash
alt_disk_copy -d {alt-hdisk}
# Ex: alt_disk_copy -d hdisk2
```

Clean old alt rootvg:

```bash
alt_rootvg_op -X old_rootvg
```

Mount alternative rootvg:

```bash
alt_rootvg_op -W -d {alt-hdisk}

# See FS:
lsvg -l altinst_rootvg
```

Umount alternative rootvg FS:

```bash
alt_rootvg_op -S -d {alt-hdisk}
```

Restore mksysb on alternate rootvg:

```bash
alt_disk_mksysb -m {mksysb} -d {alt-hdisk}
# Ex: alt_disk_mksysb -m /mnt/mksysb_srv1 -d hdisk2
```

---
tags: [System, AIX]
title: DVD Install
---

# AIX - DVD Install

## From VIOS

Create VMLibrary directory:

```bash
mkdir -p /var/vio/VMLibrary
```

Copy DVD master:

```bash
master_image="master_AIX72"

scp /path/to/$master_image /var/vio/VMLibrary/$master_image
```

Display repository:


```bash
lsrep
```

Unload/Load DVD:

```bash
# Get if Mksysb is used
lsmap -all

# NOTE: If not present create vtd
mkvdev -fbo -vadapter vhost0

# If used, unload old master
unloadopt -vtd dvd -release

# Load new master
loadopt -disk $master_image -vtd dvd -release
```

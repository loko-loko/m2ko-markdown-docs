---
tags: [System, AIX]
title: Nim
---

# AIX - Nim

## Commands

### Globals

Get list of nim attributes:

```bash
lsnim -t {attr}
# Ex:
#   lsnim -t spot
#   lsnim -t mksysb
#   lsnim -t lpp_source
```

Get ressource detail:

```bash
lsnim -l {resource}
# Ex: lsnim -t server-1
```

Upgrade AIX:

```bash
nimadm -j nimadmvg \
  -c {server} \
  -s {spot} \
  -l {lpp} \
  -d {hdisk_to_install} -Y

# nimadm -j nimadmvg \
#   -c aix-srv-1 \
#   -s spot_aix72 \
#   -l lpp_aix72 \
#   -d hdisk1 -Y
```

### Spot

Get list of spot:

```bash
lsnim -t spot
```

Create Spot:

```bash
nim -o define -t spot \
  -a server=master \
  -a source={mksysb-name} \
  -a location={spot-location} \
  {spot-name}

# Ex: nim -o define -t spot \
#       -a server=master \
#       -a source=srv1_mksysb \
#       -a location=/export/spot/spot_srv1 \
#       spot_srv1
```

Check spot:

```bash
nim -o lppchk {spot-name}
```

Check integrity of spot:

```bash
nim -o fix_query {spot-name} | grep ML
```

Map spot to a server:

```bash
nim -o bos_inst \
  -a source=mksysb \
  -a mksysb={mksysb-name} \
  -a boot_client=no \
  -a spot={spot-name} \
  {server}
```

Delete spot:

```bash
nim -o remove {spot-name}
```

## Link

- https://www.ibm.com/docs/en/aix/7.2?topic=csrfmr-creating-spot-resource-from-mksysb-resource-smit-from-command-line

---
tags: [System, AIX]
title: Package/Fix
---

# AIX - Package/Fix

## Commands

### Packages

Packages list:

```bash
# With state:
lslpp -l

# Generic:
lslpp -L all

# Historical:
lslpp -h
```

Get which package provide a bin:

```bash
lslpp -w {bin}
# Ex: lslpp -w /usr/dt/bin/dtterm
```

List package from a directory:

```bash
installp -l -MR -d /path/to/packages
```

Install package:

```bash
installp -aYX -d /path/to/packages lsof 
```

Commit package:

```bash
installp -c {package}
```

Delete package:

```bash
installp -u (-g) {package}
# Ex: installp -u -g bos.cifs_fs.rte
```

Create `.toc` (Package index):

```bash
bffcreate -ld .
```

List package greather/lower than version:

```bash
oslevel -rg 7200-04
oslevel -sg 7200-04-02-2028
```

### Update

Update package:

```bash
smitty update_all
```

### Fix

Check that installed levels are completed:

```bash
instfix -i | grep -i ML
# Output:
#   All filesets for 7.1.0.0_AIX_ML were found.
#   All filesets for 7100-00_AIX_ML were found.
#   All filesets for 7100-01_AIX_ML were found.
#   All filesets for 7100-02_AIX_ML were found.
```

List installed fix:

```bash
instfix -ivq
```

Check missing fix or package:

```bash
instfix -icqk {ML} | grep :-:
# Ex: instfix -icqk 7200-01_AIX_ML | grep :-:
```

Install interim fix (*.Z):

```bash
# Preview
emgr -e {ipkg-name} -p

# Install
emgr -e {ipkg-name} -X
```

Install a fix:

```bash
# Preview
installp -a -d {fix-name} -p all

# Install
installp -a -d {fix-name} -X all
```

Check quality of package installation:

```bash
lppchk -vm3
```

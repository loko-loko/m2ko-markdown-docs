---
tags: [System, AIX, Package]
title: Yum
---

# AIX - Yum

## Prerequisite

Resize some FS:

```bash
chfs -a size=+1G /opt
chfs -a size=+2G /var
```

## RPM Setup

Install last rpm package manager from https://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/INSTALLP/ppc/: 

```bash
installp -qacXYd rpm.rte all
```

## Yum Setup

### RPM Install

Install all RPM's from `yum_bundle.tar` (https://public.dhe.ibm.com/aix/freeSoftware/aixtoolbox/ezinstall/ppc/):

```bash
# Unarchive
tar -xvf yum_bundle.tar

# Install with rpm
rpm -Uvh *
```

### Config

Modify the `/opt/freeware/etc/yum/yum.conf` file: 

```ini
[main]
cachedir=/var/cache/yum
keepcache=1
debuglevel=2
logfile=/var/log/yum.log
exactarch=1
obsoletes=1
plugins=1

[AIX_Toolbox]
name=AIX generic repository
baseurl=https://public.dhe.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/
enabled=1
gpgcheck=0

[AIX_Toolbox_noarch]
name=AIX noarch repository
baseurl=https://public.dhe.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/noarch/
enabled=1
gpgcheck=0

[AIX_Toolbox_72]
name=AIX 7.2 specific repository
baseurl=https://public.dhe.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc-7.2/
enabled=1
gpgcheck=0
```

### Update

Start yum update:

```bash
yum update
```

Update DB:

```bash
updtvpkg
```

### Optional (for iperf3)

Install `bos.net.sctp` AIX package.

### Usage

Install your favorite rpm:

```bash
yum install iperf3
```

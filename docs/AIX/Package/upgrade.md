---
tags: [System, AIX, Upgrade]
title: Upgrade
---

# AIX - Upgrade

!!! warning

    Before Update or Upgrade don't forgot the alternate rootvg

## Upgrade AIX with installp (TL Only)

Mount package for AIX 7.2:

```bash
mount nim-server:/export/AIX72/UPD_TLSP_AIX72TL4SP2_2028 /mnt/upgrade
```

Upgrade:

```bash
# Display only (-p):
installp -a -Y -p -d /mnt/upgrade all

# Upgrade package:
installp -a -Y -d /mnt/upgrade all
```

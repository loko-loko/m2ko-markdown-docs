---
tags: [System, AIX]
title: Service
---

# AIX - Service

## Commands

List all services:

```bash
lssrc -a
```

Stop a service:

```bash
stopsrc -s {service}
stopsrc -g {group}
# Ex:
#   stopsrc -c sshd
#   stopsrc -g nfs
```

Start a service:

```bash
startsrc -s {service}
startsrc -g {group}
# Ex:
#   startsrc -c sshd
#   startsrc -g nfs
```

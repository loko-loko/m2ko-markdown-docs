---
tags: [Storage, Ceph]
title: Object Storage
---

# Ceph - Object Storage (Rados GW)

## Commands

### User/Bucket

List all users:

```bash
radosgw-admin metadata user list
```

List all buckets:

```bash
radosgw-admin metadata bucket list
```

Detail of a bucket:

```bash
radosgw-admin metadata get bucket:{bucket}
# Ex:
#  Get owner (user) of a bucket:
#   radosgw-admin metadata get bucket:my-bucket | awk -F'"' '/owner/ {print $4}'
```

Detail of a user:

```bash
radosgw-admin metadata get user:{user}
# Ex:
#  Get access/secret key of a user:
#   radosgw-admin metadata get user:my-user | awk '/(access|secret)_key/' | sed 's/"\|,//g'
```

Create user:

```bash
radosgw-admin user create \
  --uid={username} \
  --display-name="{display-name}"
```

Bucket stats:

```bash
radosgw-admin bucket stats (--bucket {bucket-name})
```

Change bucket owner:

```bash
radosgw-admin bucket link --bucket={bucket-name} --uid={user}
```

### Pool/Placement

Create Pool:

```bash
ceph osd pool create {pool-name} {number-of-objects}
# Ex: ceph osd pool create rgw.buckets.loko.data 32
```

Enable application on a pool:

```bash
ceph osd pool application enable {pool} {app-name (cephfs|rbd|rgw)}
# Ex: ceph osd pool application enable pool.data rgw
```

Get current zone/zonegroup:

```bash
radosgw-admin zone get
radosgw-admin zonegroup get
```

Create Zonegroup Placement:

```bash
radosgw-admin zonegroup placement add \
  --rgw-zonegroup="{zonegroup-name}" \
  --placement-id="{placement-name}"
# Ex: radosgw-admin zonegroup placement add \
#       --rgw-zonegroup="prod" \
#       --placement-id="loko"
```

Create Zone Placement:

```bash
radosgw-admin zone placement add \
  --rgw-zone stime \
  --placement-id="{placement-name}" \
  --data-pool="{data-pool}" \
  --index-pool="{index-pool}" \
  --data-extra-pool="{data-extra-pool}"
# Ex: radosgw-admin zone placement add \
#       --rgw-zone paris \
#       --placement-id="loko" \
#       --data-pool="rgw.buckets.loko.data" \
#       --index-pool="rgw.buckets.index" \
#       --data-extra-pool="rgw.buckets.non-ec"
```

### Others

List zone (region):

```bash
radosgw-admin zone list
```

Commit modification:

```bash
radosgw-admin period update --commit
```


List queue of Garbage Collector:

```bash
radosgw-admin gc list --include-all
```

## Procedure

### Debug Mode

Add the following under the rgw section of the `/etc/ceph/ceph.conf` file of Rados gateway node:

```ini
debug ms = 1
debug rgw = 20
```

Restart service:

```bash
systemctl restart ceph-radosgw@{service_name}.service
```

## Links

- https://docs.ceph.com/en/latest/radosgw/admin/
- https://docs.ceph.com/en/latest/man/8/radosgw-admin/

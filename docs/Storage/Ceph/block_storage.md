---
tags: [Storage, Ceph]
title: Block Storage
---

# Ceph - Block Storage (Rados BD)

## Globals

Display block device info (Global/Pools):

```bash
ceph df
```

## Client

### Configs

Example of keyring config:

```ini
# /etc/ceph/ceph.client.server1.keyring
[client.server1]
        key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXxx=="
        caps mon = "allow r"
        caps osd = "allow rwx pool=my-pool"
```

!!! note

    With this config you must specify the hostname of server with `--id` option.
    You can also specify the keyring file path with `--keyring` option.

### Pool/Volume

Display pool's volumes:

```bash
rbd ls {pool}
```

Display volume detail:

```bash
rbd info {pool}/{volume}
```

List host's mapped devices:

```bash
rbd showmapped
```

Map/Unmap a volume to client:

```bash
rbd [map|unmap] {pool}/{volume}
# Ex:
#   Map Vol:   rbd map pool-1/vol-1 
#   Unmap Vol: rbd unmap pool-1/vol-1
```

### Snapshot

List volume's snapshots:

```bash
rbd snap ls {pool}/{volume}
```

Create snapshot:

```bash
rbd snap create --snap {snap-name} --pool {pool} {volume}
# Ex: rbd snap create --snap my-vol.snap --pool pool-1 my-vol
```

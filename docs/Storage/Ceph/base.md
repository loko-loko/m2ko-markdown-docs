---
tags: [Storage, Ceph]
title: Base
---

# Ceph - Base

## Commands

### Globals

Ceph version:

```bash
ceph --version
# RH Ceph version: https://access.redhat.com/solutions/2045583
```

Display global cluster status:

```bash
ceph (status|-s)
```

Display cluster health:

```bash
ceph health (detail)
# Result: HEALTH_OK|HEALTH_WARN|HEALTH_ERR
```

Display block device info (Global/Pools):

```bash
ceph df
```

Exec bash on a mononitor container:

```bash
docker exec -it ceph-mon-{node} /bin/bash
# Ex: docker exec -it ceph-mon-node001 /bin/bash
```

Get status of a monitor:

```bash
ceph tell mon.{node} mon_status
ceph daemon mon.{node} mon_status
# Ex: ceph tell mon.node001 mon_status
```

### Configs

Display all configuration:

```bash
ceph config dump
```

Set a parameter:

```bash
ceph config set mon {parameter} {value}
# Ex: ceph config set mon paxos_service_trim_max 10000
```

### Auths

Create client keyring:

```bash
ceph auth get-or-create {client-key} {attributes}
# Ex: ceph auth get-or-create client.server1 \
#       mon 'allow r' \
#       osd 'allow rwx pool=pool1, allow rwx pool=pool2'
```

Get client keyring:

```bash
ceph auth get {client-key}
# Ex: ceph auth get client.server1
```

### Mon

Display size of database:

```bash
du -hs /var/lib/ceph/mon/ceph-$(hostname -s)/store.db
```

## Procedures

### Mon

Get how many `osdmaps` are kept in mon.db:

```bash
ceph report
...
    "osdmap_first_committed": 620715,
    "osdmap_last_committed": 2122666,
...

# Calculate with: osdmap_last_committed - osdmap_first_committed
# In example: 2122666 - 620715 = 1501951
```

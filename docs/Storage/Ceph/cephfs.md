---
tags: [Storage, Ceph]
title: CephFS
---

# Ceph - CephFS (MDS)

## Commands

### Globals

Ceph FS status:

```bash
ceph fs status
```

### Configs

Show mds configs:

```bash
ceph daemon mds.{mds-node} config show
# Ex: ceph daemon mds.mds-node-1 config show
```

Get a specific mds config value:

```bash
ceph daemon mds.{mds-node} config get {parameter}
# Ex: ceph daemon mds.mds-node-1 config get mds_cache_memory_limit
```

Set a mds config value:

```bash
ceph daemon mds.{mds-node} config set {parameter} {value}
# Ex: ceph daemon mds.mds-node-1 config set mds_cache_memory_limit 68719476736
```

Persist parameter in `ceph.conf`:

```ini
[mds.{mds-node}]
mds_cache_memory_limit = 68719476736

# Ex:
#  [mds.mds-node-1]
#  mds_cache_memory_limit = 68719476736
```

## Scripts

Get some values of all mds nodes:

```bash
mds_servers=($(ceph node ls | jq -r '.mds|keys[]'))
for mds in ${mds_servers[@]}; do
  echo "mds.$mds:"
  echo "mds_bal_fragment_size_max: $(ceph config get mds.$mds mds_bal_fragment_size_max)"
  echo "mds_bal_split_size: $(ceph config get mds.$mds mds_bal_split_size)"
  echo "-------------------------------------------"
done
```

## Links

- https://documentation.suse.com/fr-fr/ses/5.5/html/ses-all/cha-ceph-as-cephfs.html
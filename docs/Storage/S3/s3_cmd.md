---
tags: [Storage, Object, Client]
title: S3Cmd
---

# S3 - S3Cmd

## Configs

Example of config:

```ini
# s3cmd.conf
[default]
access_key = $ACCESS_KEY
secret_key = $SECRET_KEY
host_base = s3.m2ko.net
host_bucket = %(bucket)s.s3.m2ko.net
```

Usage example:

```bash
export ACCESS_KEY=XXXXXX
export SECRET_KEY=xxXXXXxxxXXXxxxxx s3cmd -c s3cmd.conf ls s3://my-bucket
```

## Commands

List object:

```bash
s3cmd ls s3://{bucket}/{file|path}
s3cmd ls -H s3://{bucket}/{file|path} # Human readable size
```

Sum of objects:

```bash
s3cmd du s3://{bucket}/{file|path}
s3cmd du -H s3://{bucket}/{file|path} # Human readable size
```

Put object:

```bash
s3cmd put {local-file} s3://{bucket}/{file}
```

Put object:

```bash
s3cmd put {file} s3://{bucket}/{file}
```

Modify ACL (Grant access to a user):

```bash
s3cmd setacl --acl-grant=all:{user} s3://{bucket}
```

Delete object:

```bash
s3cmd del|rm {file} s3://{bucket}/{file}
s3cmd del|rm -r {file} s3://{bucket}/{file} # Recursive
```
---
tags: [Storage, Object, AWS, Client]
title: AWS Cli
---

# S3 - AWS Cli

## Install

Install with Python `pip`:

```bash
pip install aws
```

Install with AWS script:

```bash
# Download installer
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

# Unzip + Start install script
unzip awscliv2.zip
sudo ./aws/install

# Remove files
rm -rf awscliv2.zip aws
```

## Configuration

Start configuration with CLI:

```
aws configure
```

Example of configuration:

```bash
# ~/.aws/config
[ceph4]
output = json
region = paris

# ~/.aws/credentials
[ceph4]
aws_access_key_id = XXXXXXXXXXXXX
aws_secret_access_key = XXXXXXXXXXXXXXXXXXXXXX
```

## Commands

### S3 CLI

List bucket:

```bash
aws s3 \
  (--profile {profile}) \
  (--endpoint-url {s3-url}) \
  ls s3://{bucket}
# Ex:
#  aws s3 --endpoint-url https://s3.plop.com ls s3://plopy-bucks
```

Create bucket:

```bash
aws s3 \
  (--profile {profile}) \
  (--endpoint-url {s3-url}) \
  mb s3://{bucket}
# Ex:
#  aws s3 --endpoint-url https://s3.plop.com mb s3://new-plopy-bucks
```

Copy file:

```bash
aws s3 \
  (--profile {profile}) \
  (--endpoint-url {s3-url}) \
  cp {file} s3://{bucket}/{file}
# Ex:
#  aws s3 --endpoint-url https://s3.plop.com cp file.txt s3://plopy-bucks/file.txt
```

Delete file:

```bash
aws s3 \
  (--profile {profile}) \
  (--endpoint-url {s3-url}) \
  rm (--recursive) s3://{bucket}/{file}
# Ex:
#  aws s3 --endpoint-url https://s3.plop.com rm s3://plopy-bucks/file.txt
```

Set html index/error file to use bucket as website:

```bash
aws s3 website \
  (--profile {profile}) \
  (--endpoint-url {s3-url}) \
  --index-document {html-index-file} \
  --error-document {html-error-file} s3://{bucket}
# Ex:
#  aws s3 website s3://my-bucket/ --index-document index.html --error-document error.html
```

### S3 API

Create bucket:

```bash
aws s3api create-bucket \
  (--profile {profile}) \
  (--endpoint-url {s3-url}) \
  --bucket {bucket} \
  (--create-bucket-configuration LocationConstraint={zone})
```

Set object to Public:

```bash
aws s3api put-object-acl \
  (--profile {profile}) \
  (--endpoint-url {s3-url}) \
  --bucket {bucket} \
  --key {object} \
  --acl public-read
```

Set bucket to Public:

```bash
aws s3api put-bucket-acl \
  (--profile {profile}) \
  (--endpoint-url {s3-url}) \
  --bucket {bucket} \
  --acl public-read
```

Get bucket ACL:

```bash
aws s3api get-bucket-acl \
  (--profile {profile}) \
  (--endpoint-url {s3-url}) \
  --bucket {bucket}
```

## Link

- https://docs.aws.amazon.com/cli/latest/reference/

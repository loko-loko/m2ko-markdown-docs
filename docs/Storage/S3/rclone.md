---
tags: [Storage, Object, Client]
title: RCLone
---

# RCLone

## Config

Example of config:

```ini
# rclone.conf
[S3]
type = s3
provider = Ceph
env_auth = false
access_key_id = <ACCESS_KEY>
secret_access_key = <SECRET_KEY>
endpoint = s3.m2ko.net
acl = private
```

Usage example:

```bash
rclone --config rclone.conf -v copy file.txt S3:/{bucket}/file.txt
```

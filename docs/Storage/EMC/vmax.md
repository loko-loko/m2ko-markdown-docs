---
tags: [Storage, EMC, Block]
title: VMax
---

# EMC - Symmetrix VMax

## Commands

### Device

Detail of a device:

```bash
symdev -sid {sid} show {dev}
```

List all device:

```bash
symdev -sid {sid} list
```

Display more detail of a dev (Pool/Written/Allocate):

```bash
symcfg -sid {sid} list -tdev -gb -dev {dev}
```

Display dev attachment:

```bash
symaccess -sid {sid} list assignment -v -devs {dev}
```

Display SG of a dev:

```bash
symaccess -sid {sid} list -type storage dev {dev}
```

Display more detail of SG's dev:

```bash
symcfg -sid {sid} list -tdev -gb -sg {sg} (-detail)
```

Create device:

```bash
# Symconfigure
symconfigure -sid {sid} -cmd "create dev count={count}, size={size cyl}, emulation=fba, dynamic_capability=DYN_RDF, config=TDEV, binding to pool={pool};" prepare -nop

# Symdev (Only: VMAX 3 / PMAX)
symdev -sid {sid} create -tdev -emulation fba -cap {} -captype {cyl|mb|gb|tb} (-sg {sgroup}) (-N {count}) -v
```

Create meta dev:

```bash
symconfigure -sid {sid} -cmd "create dev count={count}, size={size cyl}, meta_member_size={member size cyl}, meta_config=striped, emulation=FBA, dynamic_capability=DYN_RDF, config=TDEV, binding to pool={pool};" prepare -nop
```

Create GateKeeper:

```bash
symconfigure -sid {sid} -cmd "create gatekeeper count={count}, emulation=FBA, type=thin, binding to pool={pool};"  prepare -nop
```

Add dev to a SG:

```bash
symaccess -sid {sid} add -name {sg} -type storage dev {dev}
```

Unbind dev:

```bash
symdev -sid {sid} unbind -dev {dev}
```

Delete dev:

```bash
symconfigure -sid {sid} -cmd "delete dev {dev};" prepare -nop
```

Disolve META:

```bash
symconfigure -sid {sid} -cmd "dissolve meta dev {dev};" prepare -nop
```

Unbind LUN:

```bash
symconfigure -sid {sid} -cmd "unbind tdev {dev}from pool {pool};" prepare -nop
```

Change dev state:

```bash
symdev -sid {sid} not_ready -dev {dev} -nop
```

Bind of a dev:

```bash
symdev -sid {sid} bind -pool {pool} -dev {dev}
```

Change dev flag:

```bash
symconfigure -sid {sid} -cmd "convert dev {dev} to BCV+TDEV;" prepare -nop
```

Change SRDF flag:

```bash
symconfigure -sid {sid} -cmd "set dev {dev} attribute=dyn_rdf;" prepare -nop
```

List all devs with identifier:

```bash
symdev -sid {sid} list -identifier device_name
```

Reset lun identity (External UID):

```bash
symdev -sid {sid} reset -identity {dev} -nop
```

### S.Group

Display all sg:

```bash
symsg -sid {sid} list
symaccess -sid {sid} list -type storage
```

SG detail:

```bash
symsg -sid {sid} show {sg}
symaccess -sid {sid} show {sg} -type storage
```

Create SG:

```bash
symaccess -sid {sid} create -name {sg} -type storage
```

Delete SG:

```bash
symaccess -sid {sid} delete -name {sg} -type storage
```

Remove dev from a SG:

```bash
symaccess -sid {sid} remove -name {sg} -type storage dev {dev} (-unmap)
```

Rename of a SG:

```bash
symaccess -sid {sid} rename -name {sg} -type storage -new_name {new sg}
```

### Initiator/M.View

Detail of a IG:

```bash
symaccess -sid {sid} show {ig} -type initiator
```

Detail of a MV:

```bash
symaccess -sid {sid} show {mv} view
```

Create IG:

```bash
symaccess -sid {sid} create -type initiator -name {ig} -wwn {login}
```

Add login to IG:

```bash
symaccess -sid {sid} add -type initiator -name {ig} -wwn {login}
```

Create MV:

```bash
symaccess -sid {sid} create view -name {mv} -sg {sg} -ig {ig} -pg {pg}
```

Delete of a IG:

```bash
symaccess -sid {sid} delete -name {ig} -type initiator
```

Delete MV:

```bash
symaccess -sid {sid} delete -name {mv} view -unmap
```

### Login/P.Group

Logins list:

```bash
symaccess -sid {sid} list logins
```

Login detail:

```bash
symaccess -sid {sid} list logins -v -wwn {login}
```

Get IG from a WWN:

```bash
symaccess -sid {sid} list -type initiator -wwn {login}
```

PG list:

```bash
symaccess -sid {sid} list -type port
```

PG detail:

```bash
symaccess -sid {sid} show {pg} -type port
```

Give alias to a Login:

```bash
symaccess -sid {sid} rename -wwn {login} -alias {server name}/{server port}
```

Create PG:

```bash
symaccess -sid {sid} create -name {pg} -type port -dirport {dir}:{port}
```

Add port to PG:

```bash
symaccess -sid {sid} add -name {pg} -type port -dirport {dir}:{port}
```

Delete Login:

```bash
symaccess -sid {sid} remove -login -wwn {login}
```

Delete Port from a Login:

```bash
symaccess -sid {sid} remove -login -wwn {login} -dirport {dir:port}
```

Delete PG:

```bash
symaccess -sid {sid} delete -name {pg} -type port
```

### Pool

Display pool:

```bash
symcfg -sid {sid} list -pool -thin -detail -gb
```

### SRDF

List SRDF device:

```bash
symrdf -sid {sid} list
```

Display info of SRDF pair(s):

```bash
symrdf -sid {sid} -f {file.txt} -rdfg {ra grp} query
```

List mapping of SRDF ports:

```bash
symsan -sid {sid} list -sanrdf -dir all
```

Check before start replication:

```bash
symrdf -sid {sid} -f {file.txt} -rdfg {ra grp} -type rdf1 createpair -invalidate R2
```

Create replication with Synchrone mode:

```bash
symrdf -sid {sid} -f {file.txt} -rdfg {ra grp} createpair -type rdf1 -establish -rdf_mode sync -nop
```

Create replication with Adaptive mode (Write depends on workload of array):

```bash
symrdf -sid {sid} -f {file.txt} -rdfg {ra grp} createpair -type rdf1 -establish -rdf_mode acp_disk -nop
```

Start SRDF sync:

```bash
symrdf -sid {sid} -f {file.txt} -rdfg {ra grp} establish -nop
```

Passage de la synchronisation en mode Synchrone ou Adaptive Copy:

```bash
symrdf -sid {sid} -f {file.txt} -rdfg {ra grp} set mode sync -nop
symrdf -sid {sid} -f {file.txt} -rdfg {ra grp} set mode acp_disk -nop
```

Split SRDF pair(s):

```bash
symrdf -sid {sid} -f {file.txt} -rdfg {ra grp} split (-force) -nop
```

Delete pair(s):

```bash
symrdf -sid {sid} -f {file.txt} -rdfg {ra grp} deletepair (-force) -nop
```

List RA group:

```bash
symcfg -sid {sid} list -rdfg all
```

List RA group ports:

```bash
symcfg -sid {sid} list -ra all -switch
```

Create RA group:

```bash
symrdf -sid {sid} addgrp -label {grp label} -dir {port} -rdfg {ra group id} -remote_sid {remote bay} -remote_dir {remote port} -remote_rdfg {remote ra group id}
```

Delete RA Group:

```bash
symrdf -sid {sid} removegrp -rdfg {ra group id} -nop
```

### Clone/BCV

Display state:

```bash
symclone -sid {sid} -f {file.txt} query
```

Start sync (Precopy):

```bash
symclone -sid {sid} -f {file.txt} create -precopy -differential (-nop)
```

Activate sync:

```bash
symclone -sid {sid} -f {file.txt} activate (-nop)
```

Restart sync (Precopy)

```bash
symclone -sid {sid} -f {file.txt} recreate (-nop)
```

Split sync:

```bash
symclone -sid {sid} -f {file.txt} split (-nop)
```

Delete pair(s):

```bash
symclone -sid {sid} -f {file.txt} terminate (-nop)
```

### Device Group (DG)

DG List (Disk Group):

```bash
symdg list
```

Display DG info:

```bash
symdg show {dg}
```

Add dev in DG:

```bash
symdg -sid {sid} -g {dg} addall -devs {dev}
```

Add Target to DG:

```bash
symdg -sid {sid} -g {dg} add dev {dev} -tgt
```

Add BCV Target in DG:

```bash
symbcv -g {dg} add dev {dev}
```

Delete Target in DG:

```bash
symld -g {dg} remove {dev}
```

### VLUN

List devs/session with migration in progress:

```bash
symmigrate -sid {sid} list
```

Validate a new session:

```bash
symmigrate -sid {sid} [-f {file}|-sg {sg}] -tgt_pool -pool {target_pool} -name {session_name} validate -nop
```

Start new session:

```bash
symmigrate -sid {sid} [-f {file}|-sg {sg}] -tgt_pool -pool {target_pool} -name {session_name} establish -nop
```

Terminate session:

```bash
symmigrate -sid {sid} -name {session_name} terminate -nop
```

### Fast Policy

List Fast Policies:

```bash
symfast -sid {sid} list -fp
```

Display Fast Policy detail:

```bash
symfast -sid {sid} show -fp_name {fp}
```

Display Fast Policy of a SG:

```bash
symfast -sid {sid} list -association -demand -sg {sg}
```

Add Fast Policy of a SG:

```bash
symfast -sid {sid} associate -fp_name {fp} -sg {sg}
```

Remove Fast Policy of a SG:

```bash
symfast -sid {sid} disassociate -fp_name {fp} -sg {sg}
```

### Hardware

List failed disk(s):

```bash
symdisk -sid {sid} list -failed
```

List components:

```bash
# All
symcfg -sid {sid} list -env_data

# Failed or Degraded
symcfg -sid {sid} list -env_data -service_state failed
symcfg -sid {sid} list -env_data -service_state degraded
```

Events list (Logs):

```bash
symevent -sid {sid} list -fatal (-start mm/dd/yy)
symevent -sid {sid} list -error (-start mm/dd/yy)
symevent -sid {sid} list -warning (-start mm/dd/yy)
```

### Processus Lock

Display lock of array:

```bash
symcfg -sid {sid} list -lockn all
```

Stop a lock:

```bash
symcfg -sid {sid} release -lockn {lock id} -force -nop
```

## Procedures

### Debug Mode

To activate this mode, you must export the following env vars:

```bash
export SYMAPI_DEBUG=1
export SYMAPI_DEBUG_FILENAME=/tmp/debug.out
```

### Symforce

To activate `symforce` option, edit `/var/symapi/config/options`
file and set `SYMAPI_ALLOW_RDF_SYMFORCE` to `TRUE`:

```makefile
...
#------------------------------------------------------------------------------
#
# Parameter:            SYMAPI_ALLOW_RDF_SYMFORCE
#
# Allowed values:       TRUE | FALSE
#
# Default value:        FALSE
#
# Description:          Indicates whether users can specify -symforce when
#                       performing RDF control operations.
#
SYMAPI_ALLOW_RDF_SYMFORCE = TRUE
...
```

After that, you can add `-symforce` option to `symrdf` command:

```bash
symrdf -sid 0001 -f file.txt -rdfg 9 split -symforce -force -nop
```
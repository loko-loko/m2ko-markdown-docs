---
tags: [Storage, EMC]
title: Unipshere
---

# EMC - Unipshere

## API

List MV:
```bash
curl -k -u "$user" https://$server:8443/univmax/restapi/92/sloprovisioning/symmetrix/$array/maskingview
```

MV connection:
```bash
curl -k -u "$user" https://$server:8443/univmax/restapi/92/sloprovisioning/symmetrix/$array/maskingview/$mv/connections
```

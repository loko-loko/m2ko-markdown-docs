---
tags: [Tools, Excel, Windows]
title: Excel
---

# Windows - Excel

## Functions

It is a sum of product of 2 array elements. The function supports matrices (ranges):

```bash
=SUMPRODUCT(UNARY_PERCENT($D$3:$D$23)*E3:E23)
```

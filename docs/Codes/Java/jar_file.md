---
tags: [Java, Code]
title: Jar File
---

# Java - Jar File

## Tips

Get jar version

```bash
unzip -p {jar-file} META-INF/MANIFEST.MF
# Ex: unzip -p graylog.jar META-INF/MANIFEST.MF
```
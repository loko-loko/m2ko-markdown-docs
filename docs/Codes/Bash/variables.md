---
tags: [Code, Shell, Bash]
title: Variables
---

# Bash - Variables

## Manipulation

### D&eacute;claration

La commande `declare` permet de d&eacute;clarer (et d&#39;afficher) des variables avec un type. Voici les diff&eacute;rentes options de la commande :

|**Option** |**Fonction**
|--- |---
|`-r` |Lecture seule
|`-i` |Entier
|`-a` |Tableau (array)
|`-f` |Fonction(s)
|`-x` |Export

Examples:

D&eacute;claration d&#39;une variable de type entier en lecture seule:

```bash
declare -ir my_int=40
```

### Valeur par d&eacute;faut

Il est possible de donner une valeur &agrave; une variable si celle ci n&#39;est pas d&eacute;fini ou est &eacute;gal &agrave; `NULL`. Pour la prise en compte des variables d&eacute;clar&eacute;es mais `NULL` il sera n&eacute;cessaire de rajouter un caract&egrave;re (`:`).

La syntaxe `${var-value}` affichera la valeur sans l&#39;attribuer &agrave; la variable&nbsp;:

```bash
my_var=''

echo ${my_var-Default}
# {NULL}

echo ${my_var:-Default}
# Default

echo ${my_var}
# {NULL}

unset my_var

echo ${my_var-Default}
# Default

my_var='Value'

echo ${my_var:-Default}
# Value
```

La syntaxe `${var=value}` affichera la valeur et l&#39;assignera &agrave; la variable&nbsp;:

```bash
my_var=''

echo ${my_var=Default}
# {NULL}

echo ${my_var:=Default}
# Default

echo ${my_var}
# Default

unset my_var

my_var='Value'

echo ${my_var:=Default}
# Value
```

Cela fonctionne aussi avec une commande, par exemple :

```bash
username=''

echo ${username:-$(whoami)}
# current_user
```

### Expansion Indirecte

Si le premier caract&egrave;re du param&egrave;tre est un point d&#39;exclamation (`!`), Un niveau d&#39;indirection de variable est introduit.

Bash utilise la valeur de la variable form&eacute;e &agrave; partir du reste du param&egrave;tre comme nom de la variable :

```bash
var_1="var_2"
var_2="var_3"

echo ${var_1}
# var_2

echo ${!var_1}
# var_3
```

### Interpr&eacute;tation

Il est possible d&#39;interpr&eacute;ter des variables dans un fichier texte avec la commande&nbsp;`envsubst`&nbsp;:

```bash
MY_USER=Loko

echo 'Hello $MY_USER' > user.txt

cat user.txt
# Hello $MY_USER

envsubst < user.txt
# Hello Loko
```

### Majuscule/Minuscule

Les 2 syntaxes `${var^^}` et `${var,,}`&nbsp;permettent respectivement de passer une variable en majuscule ou en minuscule :

```bash
my_var="HeLLo"

echo ${my_var^^}
# HELLO

echo ${my_var,,}
# hello
```

Il est aussi possible de passer uniquement la premi&egrave;re lettre en majuscule ou en minuscule avec la syntaxe&nbsp;`${var^}` et `${var,}` :

```bash
my_var="hello"

echo ${my_var^}
# Hello

my_var="HELLO"

echo ${my_var,}
# hELLO
```

Ou encore de choisir les lettres &agrave; transformer avec une pattern :

```bash
my_var="hello"

echo ${my_var^^[ho]}
# HellO
```

## Arithmetique

### Taille d&#39;une cha&icirc;ne

Pour d&eacute;terminer la longueur d&#39;une cha&icirc;ne&nbsp;de caract&egrave;re il suffit de rajouter # avant le nom de la variable :

```bash
my_var="one"

echo ${#my_var}
# 3
```

### Op&eacute;ration

Il y a plusieurs m&eacute;thodes permettant de faire des calculs en bash.

#### Syntaxe sp&eacute;ciale

```bash
a=8
b=2

echo $((a + b))     # Addition
echo $((a - b))     # Substraction
echo $((a * b))     # Multiplication
echo $((a / b))     # Division
echo $((a % b))     # Modulo
echo $((a ** b))    # Puissance
```

#### Fonction expr

```bash
expr $a + $b     # Addition
expr $a - $b     # Substraction
expr $a \* $b    # Multiplication
expr $a / $b     # Division
expr $a % $b     # Modulo
```

#### Fonction bc

```bash
echo "$a + $b" | bc     # Addition
echo "$a - $b" | bc     # Substraction
echo "$a * $b" | bc     # Multiplication
echo "$a / $b" | bc     # Division
echo "$a % $b" | bc     # Modulo
```

La fonction `bc` permet aussi d&#39;afficher les d&eacute;cimales&nbsp;avec l&#39;option `-l` :

```bash
echo "$a / $b" | bc       # 2
echo "$a / $b" | bc -l    # 2.66666666666666666666
```

#### Fonction let

```bash
a=8
b=4

let "c = a + b"
let "d = a ** b"

echo "\$c : $c"   # $c : 12
echo "\$d : $d"   # $d : 4096
```

## Sous cha&icirc;ne

### Fractionner

Pour fractionner une cha&icirc;ne de caract&egrave;re il faut utiliser la syntaxe suivante :&nbsp;`${var:<position>:<length>}`&nbsp;

```bash
my_var='abcdefghifklmnop'

echo "'${my_var:6}'"
# 'ghifklmnop'

echo "'${my_var:1:4}'"
# 'bcde'

echo "'${my_var:6:10}'"
# 'ghifklmnop'

echo "'${my_var::3}'"
# 'abc'

echo "'${my_var:(-3)}'"
# 'nop'
```

### Supprimer

**Depuis le d&eacute;but**

La syntaxe `${var#<pattern>}` permet de supprimer une sous cha&icirc;ne de caract&egrave;re&nbsp;en commen&ccedil;ant par le d&eacute;but :

```bash
my_var='Hello Manola !'

echo "'${my_var#H*[ol]}'"
# 'lo Manola !'

echo "'${my_var#H* }'"
# 'Manola !'
```

L&#39;on peut ajouter un `#` pour supprimer la sous cha&icirc;ne&nbsp;la plus longue :

```bash
my_var='Hello Manola !'

echo "'${my_var##H*o}'"
# 'la !'
```

**Depuis la fin**

La syntaxe `${var%<pattern>}` permet de supprimer une sous cha&icirc;ne de caract&egrave;re&nbsp;en commen&ccedil;ant par la fin.&nbsp;L&#39;on peut aussi ajouter un `%` pour supprimer la sous cha&icirc;ne&nbsp;la plus longue.

```bash
my_var='Hello Manola !'

echo "'${my_var%o*\!}'"
# 'Hello Man'

echo "'${my_var%%o*\!}'"
# 'Hell'
```

Voici un exemple d&#39;utilisation permettant de s&eacute;parer le r&eacute;pertoire&nbsp;et le nom d&#39;un fichier :

```bash
my_file="/to/path/file.txt"

echo ${my_file%/*}
# /to/path

echo ${my_file##*/}
# file.txt
```

### Remplacer&nbsp;

La syntaxe `${var/<pattern>/<new pattern>}` permet de remplacer une&nbsp;sous cha&icirc;ne de caract&egrave;re.&nbsp;

```bash
my_var='Hello Manola !'

echo "'${my_var/o/X}'"
# 'HellX Manola !'

echo "'${my_var/Hello/GoodBye}'"
# 'GoodBye Manola !'
```

La syntaxe pr&eacute;c&eacute;dente remplace uniquement la premi&egrave;re occurrence. Pour que toutes les occurrences soit remplac&eacute;es il suffit de doubler le `/` :

```bash
my_var='Hello Manola !'

echo "'${my_var//o/X}'"
# 'HellX ManXla !'
```

Il est aussi possible de remplacer la sous cha&icirc;ne&nbsp;par une valeur NULL :

```bash
my_var='Hello Manola !'

echo "'${my_var/H*M/}'"
# 'anola !'

echo "'${my_var//l/}'"
# 'Heo Manoa !'
```

## Variables Sp&eacute;ciales

### Arguments

Voici la liste de variables permettant de g&eacute;rer les arguments :

|**Variable** |**Fonction**
|--- |---
|`$0` |Nom du script
|`$1`, `$2`, `$3`, `$*n*` |Argument(s)
|`$*` |Liste des arguments sur une ligne (ex: "a b c")
|`$@` |Liste des arguments dans un tableau (ex : "a" "b" "c")
|`$#` |Nombre d'arguments

**Examples**

Voici un petit script affichant les informations sur les arguments:

```bash
#!/bin/bash

echo '[$0] Script Name     : '$0
echo '[$1] First Argument  : '$1
echo '[$2] Second Argument : '$2
echo '[$*] Arguments List  : '$*
echo '[$@] Arguments List  : '$@
echo '[$#] Arguments Count : '$#
```

```
# ./script.sh a b
[$0] Script Name     : ./script.sh
[$1] First Argument  : a
[$2] Second Argument : b
[$*] Arguments List  : a b
[$@] Arguments List  : a b
[$#] Arguments Count : 2
```

### Autres

|**Variable** |**Fonction**
|--- |---
|`$$` |Pid du shell courant
|`$?` |Code retour de la dernière commande
|`$!` |Id du processus de la dernière tâche exécutée en BG
|`$_` |Retour de la dernière commande

**Exemples**

```bash
#!/bin/bash

printf "%-24s" 'Script Pid'
echo '$$ : '$$

printf "%-24s" "sleep .1"
sleep .1; cmd_return=$?
echo '$? : '$cmd_return

printf "%-24s" "sleep -bad_argument"
sleep -bad_argument 2>&-; cmd_return=$?
echo '$? : '$cmd_return

printf "%-24s" "sleep .1 &"
sleep .1 &
echo '$! : '$!

printf "%-24s" "echo Hello > /dev/null"
echo Hello > /dev/null
echo '$_ : '$_
```

```
# ./script.sh
Script Pid              $$ : 10962
sleep .1                $? : 0
sleep -bad_argument     $? : 1
sleep .1 &              $! : 11025
echo Hello > /dev/null  $_ : Hello
```
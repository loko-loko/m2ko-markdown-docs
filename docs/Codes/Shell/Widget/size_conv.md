---
tags: [Code, Shell, Widget]
title: Size Convert
---

# Shell - Widget: Size Convert

## Description

Voici une petite fonction permettant de formater une taille donn&eacute;e (Bytes|Octet) afin de la rendre lisible.

Elle prend en argument :

*   La valeur (`Défaut:0`)
*   L&#39;unit&eacute; de mesure (`B, KB, MB, GB, TB`) (`Défaut:B`)
*   D&eacute;cimal apr&egrave;s la virgule (`Défaut:1`)

## Exemples

Voici quelques exemples d&#39;utilisation

```
size_conv 6000 KB
> 5.9 MB
size_conv 8000 GB
> 7.8 TB
size_conv 4099 MB 4
> 4.0029 GB
./size.sh 4099 MB 0
> 4 GB
```

## Code

```bash
size_conv() { #Arg $size $val $dec_nb
    size=${1:-0}
    val=${2:-'B'}
    dec_nb=${3:-1}

    val_lst=('B' 'KB' 'MB' 'GB' 'TB' 'PB')
    nb_re='^[0-9]+$'
    nb_max=22

    (( ${#size} >= nb_max )) && { echo "<!> Number is too Big [Max:${nb_max}]" >&2; exit 1; }
    [[ ! $val =~ $nb_re ]] && [[ ! $dec_nb =~ $nb_re ]] && { echo "<!> Bad Value" >&2; exit 1; }

    cnt=0

    for v in ${val_lst[@]}; do
        (( cnt++ ))
        if [ $val == $v ] && [ $v != ${val_lst[${#val_lst[@]}-1]} ]; then
            (( ${size%.*} >= 1024 )) && { size=$(echo "$size / 1024" | bc -l); val=${val_lst[$cnt]}; }
        fi
    done

    printf "%.${dec_nb}f %s\n" ${size} ${val}
}
```
---
tags: [System, Shell, Commands]
title: Awk
---

# Shell - Awk Command

## Description

### Syntax

Syntax of `awk` command:

```bash
awk [-Fs] [-v variable] [-f fichier] 'program' fichier
```

### Arguments

See the detail of arguments:

|**Argument** |**Définition** |**Exemple**
|--- |--- |---
|**-v** |Injecter variable dans AWK |`awk -v VAR=$VAR '{print VAR}' file.txt`
|**-F** |Séparateur de champs [Défaut ' '] |`awk -F';' '{print $1}' file.csv`
|**-f** |Les commandes sont lus à partir d'un fichier |`awk -f script.awk`

### Variables

The `awk` command have several define variables:

|**Variable** |**Signification** |**Défaut**
|--- |--- |---
|**ARGC** |Nombre d'arguments de la ligne de commande |-
|**ARGV** |Tableau des arguments de la ligne de commnde |-
|**FILENAME** |Nom du fichier sur lequel on applique les commandes |-
|**FNR** |Nombre d'enregistrements du fichier |-
|**FS** |Separateur de champs en entrée |" "
|**NF**	|Nombre de champs de l'enregistrement courant |-
|**NR**	|Nombre d'enregistrements deja lu |-
|**OFMT** |Format de sortie des nombres |"%.6g"
|**OFS** |Separateur de champs pour la sortie |" "
|**ORS** |Separateur d'enregistrement pour la sortie |"\n"
|**RLENGTH** |Longueur de la chaine trouvée |-
|**RS** |Separateur d'enregistrement en entrée |"\n"
|**RSTART** |Debut de la chaine trouvée |-
|**SUBSEP** |Separateur de subscript |"\034"
|**IGNORECASE**	|Ignorer la casse |0

### Functions

Voici la liste des différentes fonctions pouvant être utilisées sur les chaînes de caractère avec `awk`.

*   <kbd>s</kbd> et <kbd>t</kbd> représentent des chaînes de caractères
*   <kbd>r</kbd> une expression régulière
*   <kbd>i</kbd> et <kbd>n</kbd> des entiers

|**Fonction** |**Description**
|--- |---
|**gsub(r,s,t)** |Sur la chaîne <kbd>t</kbd>, remplace toutes les occurrences de <kbd>r</kbd> par <kbd>s</kbd>
|**index(s,t)** |Retourne la position la plus à gauche de la chaîne <kbd>t</kbd> dans la chaîne <kbd>s</kbd>
|**length(s)** |Retourne la longueur de la chaîne <kbd>s</kbd>
|**match(s,r)** |Retourne l'index ou <kbd>s</kbd> correspond à <kbd>r</kbd> et positionne `RSTART` et `RLENTH`
|**split(s,a,fs)** |Split <kbd>s</kbd> dans le tableau <kbd>a</kbd> sur `fs`, retourne le nombre de champs
|**sprintf(fmt,liste expressions)** |Retourne la liste des expressions formatées suivant `fmt`
|**sub(r,s,t)** |Comme `gsub`, mais remplace uniquement la première occurrence
|**substr(s,i,n)** |Retourne la sous chaîne de s commençant en <kbd>i</kbd> et de taille <kbd>n</kbd>
|**tolower(s)** |Retourne la sous chaîne <kbd>s</kbd> en minuscule
|**toupper(s)** |Retourne la sous chaîne <kbd>s</kbd> en majuscule

## Examples

### Basic

Display text from `word1` to `word2`:

```bash
awk '/word1/,/word2/' file.txt
```

Calculate column's sum:

```bash
awk '{TOTAL+=$1}END{print TOTAL}' file.txt
```

Calculate items number of a column:

```bash
awk 'END{print NR}' file.txt
```

Insert external vars:

```bash
# Méthode 1
var='word'
awk -v var=$var '$1 == var {print $2}' file.txt

# Méthode 2
awk '/'"$var"'/ {print $2}' file.txt
awk '$1 == "'"$var"'" {print $2}' file.txt
```

Select a Paragraph :

```bash
awk -v RS='' '/text/' file.txt
```

Do not take into account case:

```bash
# Method 1
echo 'LoKo' | awk -v IGNORECASE=1 '/^loko/'

# Method 2
echo 'LoKo' | awk 'tolower($0) ~ /^loko/'
echo 'LoKo' | awk 'toupper($0) ~ /^LOKO/'
```

Count number of row:

```bash
grep -v "^#" file.txt | awk -F"#" '{print $1," ",NF}'
```

### Functions

Do not keep the first raw:

```bash
awk -F',' '{sub($1 FS,"")}1' file.txt
```

Add several values with convertion of value to TB:

```bash
values="326.777G
1.064T
217.852G"

echo "$values" | awk '{
    if($1 ~ /T/){
        gsub("T", "", $1); T += $1*1024
    }
    if($1 ~ /G/){
        gsub("G", "", $1); T += $1
    }
} END{print T}'
```

---
tags: [System, Shell, Commands]
title: Sed
---

# Shell - Sed Command

## Arguments

Voici la liste de quelques arguments utiles :

*   `-r` : Permet d'insérer des expressions régulières dans la commande sed
*   `-e` : Permet de passer plusieurs commande à la suite (Par défaut dans la plupart des versions de sed)
*   `-i` : Permet de modifier directement le fichier
*   `-n` : Mode silencieux qui permet de n'afficher que les lignes selectionnées avec l'option `p` (print)

## Mode

### Silencieux

Le mode `silencieux` permet de n'afficher que les lignes sélectionnées avec l'option `p` (print). Pour l'activer il faut entrer l'argument `-n` à la commande `sed`:

```bash
sed -n '<numéro de ligne à afficher>p'
sed -n '/<motif à afficher>/p'
```

Voici des exemples d'utilisation. Par exemple pour afficher la première (`1`) ou la dernière (`$`) :

```bash
echo -e "A\nB\nC\nD" | sed -n '1p'
echo -e "A\nB\nC\nD" | sed -n '$p'
```

Récupérer les lignes contenants uniquement le motif recherché (Similaire au `grep`) :

```bash
echo -e "A\nB\nC\nD" | sed -n '/B/p'
```

Compter les lignes (Similaire à `wc -l`):

```bash
echo -e "A\nB\nC\nD" | sed -n '$='
```

Il est possible d'inverser la recherche pour afficher uniquement les lignes n'ayant pas le motif avec `!` :

```bash
echo -e "A\nB\nC\nD" | sed -n '/B/!p'
```

### Substitution

Le mode `substitution` (`s`) permet de remplacer une expression par une autre. La syntaxe est la suivante:

```bash
sed 's/<motif à remplacer>/<motif de remplacement>/'
```

Voici un exemple d'utilisation:

```bash
echo "Il fait beau" | sed 's/beau/moche/'
```

Les séparateurs peuvent être différent que le `/`, les exemples du dessous feront la même chose que celui du dessus:

```bash
echo "Il fait beau" | sed 's#beau#moche#'
echo "Il fait beau" | sed 's_beau_moche_'
echo "Il fait beau" | sed 'sXbeauXmocheX'
```

Cela est pratique quand le motif comprend le caractère `/` par exemple.

Il est possible de coupler les subsititutions pour par exemple capturer un mot :

```bash
echo 'Bonjour Loko !, il est 15h' | sed 's/Bonjour //;s/ !.*//'
```

!!! info

    Dans cette exemple `sed` va remplacer le `'Bonjour '` par `''` (vide) puis remplacer `' !.*'` (soit tout ce qu'il y a après le point d'exclamation) par `''` (vide)

Pour selectionner l'emplacement du caractère à remplacer, ajouter une valeur à la fin de la commande:

```bash
echo 'A A A A A' | sed 's/A/B/3'
```

L'option `g` (global) permet de remplacer toutes les occurences par le caractère:

```bash
echo 'A A A A A' | sed 's/A/B/g'
```

L'option `I` permet de ne pas prendre en compte la casse:

```bash
echo "SaLUt" | sed 's/salut/Bonjour/I'
```

Pour remplacer une ligne par une autre directement, il existe aussi cette méthode:

```bash
sed '<numéro de ligne>c<nouvelle ligne à mettre en place>'
```

Cette méthode nécessite donc de connaître au préalable le numéro de la ligne à modifier, exemple:​​​

```bash
echo -e "E E E\nA A A\nB B B" | sed '2cX X X'
```

**Exemples**

Mettre les espacements au propre:

```bash
echo 'A  B    A     B  A' | sed -r 's/\ +/ /g'
```

Eliminer tout espace blanc (espaces, tabulations) à la fin de chaque ligne:

```bash
echo 'A B C D   ' | sed -r 's/[ \t]*$//'
```

Remplacer le 3ème `;` par un `_`:

```bash
echo 'A;B;C;D' | sed 's/\;/_/3'
```

Suppression des 2 premiers caractères:

```bash
echo 'ABCDEFG' | sed -r 's/^.{2}(.*)$/\1/'
```

Suppression des 3 derniers caractères:

```bash
echo 'ABCDEFG' | sed -r 's/^(.*).{3}$/\1/'
```

Suppression des 2 premiers et 2 derniers caractères:

```bash
echo 'ABCDEFG' | sed -r 's/^.{2}(.*).{2}$/\1/'
```

Remplacer une directory pas une autre dans un fichier:

```bash
sed 's#/home/tiger:/usr/bin/ksh#/home/tiger:/home/tiger/Menu.ksh#g' /etc/passwd
```

### Translittération

La translittération (`y`) permet d'échanger certains caractères avec d'autres caractères.

```bash
sed 'y/<liste1>/<liste2>/'
```

Voici un exemple d'utilisation :

```bash
echo "Yééèèh" | sed 'y/éè/ea/'
```

### Suppression

Le mode `suppression` (`d`) permet de supprimer une ou plusieurs lignes selon son numéro ou motif:

```bash
sed '<numéro de ligne>d'
sed '/<motif à supprimer>/d'
```

Ou encore une plage de motif (qui supprimera les lignes du `motif1` au `motif2`):

```bash
​sed '/<motif 1>/,/<motif 2>/d'​
```

!!! info

    La négation (`!d`) produit l'inverse et supprimera les lignes différentes du motif ou du numéro de ligne. 

Il est possible de coupler les commandes pour supprimer plusieurs lignes:

```bash
echo -e "A\nB\nC\nD" | sed '1d;4d'
```

Ou encore par le biais d'un motif:

```bash
echo -e "A\nB\nC\nD" | sed '/[AD]/d'
```

Nous pouvons indiquer une plage de ligne à supprimer (Les 3 premières lignes):

```bash
echo -e "A\nB\nC\nD" | sed '1,3d'
```

Supprimera toutes les lignes qui ne comprennent pas `A` ou `D`:

```bash
echo -e "A\nB\nC\nD" | sed '/[AD]/!d'
```

**Exemples**

Supprimes toutes les lignes vides:

```bash
echo -e "E E E\nA A A\n\nB B B\n\nZ Z Z" | sed '/^$/d'
```

Supprimes toutes les lignes comprises entre `B` et le `C`:

```bash
echo -e "A\nB\nC\nD\nE" | sed '/^B/,/^D/d'
```

## Autres

### Numérotation

Il est possible de numéroter les lignes d'un fichier. Voici un exemple:

```bash
echo -e "A\nB\nC\nD" > file.txt

sed = file.txt | sed 'N;s/\n/\t/'
sed = file.txt | sed -r 'N; s/^/   /; s/ *(.{4,})\n/\1  /'
```

L'on peut aussi numéroter chaque ligne d'un fichier, mais afficher le numéro de ligne seulement si la ligne n'est pas vide.

```bash
echo -e "A\nB\nC\n\nD\nE" | sed '/./=' | sed '/./N; s/\n/ /'
```

### Insertion

Espacer en double un fichier:

```
sed G
```

Espacer en double un fichier qui a déjà des lignes vides. Le fichier de sortie n'aura jamais plus qu'une ligne vide entre les lignes de texte:

```
sed 'G;G'
```

Défaire le double-espacement (assumons les lignes paires comme étant toujours vides):

```
sed 'n;d'
```

Espacer en triple un fichier:

```
sed 'G;G'
```

Insérer une ligne vide au-dessus de chaque ligne qui contient `regex`:

```
sed '/regex/{x;p;x;}'
```

Insérer une ligne vide sous chaque ligne qui contient l'expression régulière `regex`:

```
sed '/regex/G'
```

Insérer une ligne vide au-dessus et au-dessous de chaque ligne qui contient `regex`:

```
sed '/regex/{x;p;x;G;}'
```

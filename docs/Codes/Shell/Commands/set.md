---
tags: [System, Shell, Commands]
title: Set
---

# Shell - Set Command

## Options

Voici la liste de quelques&nbsp;options utiles de `set`&nbsp;:

|**Option** |**Description**
|--- |---
|`-x` / `-o xtrace` |Mode debug
|`-u` / `-o nounset` |Quitte le script si une variable n'est pas déclarée
|`-e` / `-o errexit` |Quitte le script si une commande retourne une valeur != 0
|`-v` / `-o verbose` |Affiche les lignes du script pendant l'exécution
|`-C` / `-o noclobber` |Bloque le remplacement de fichier existant par la redirection (`>`)
|`-o vi` |Utilise le mode interactif de `vi`

Pour ajouter une option ajouter `-` pour la retirer `+`.

Pour afficher les options activ&eacute;es (ou non) afficher la variable sp&eacute;ciale `$-`&nbsp;:

```bash
echo $-
```

### Exemples

Ajouter le mode debug :

```bash
set -x / set -o xtrace
```

Retirer le mode debug :

```bash
set +x / set +o xtrace
```

&nbsp;

Il est aussi possible de setter des arguments &agrave; un script comme suit :

```bash
#!/bin/bash

echo "\$1 : $1"
echo "\$2 : $2"
echo "\$@ : $@"

echo

set "new_a" "new_b" "new_c"

echo "\$1 : $1"
echo "\$2 : $2"
echo "\$@ : $@"
```

Example:

```
# ./script.sh a b c
$1 : a
$2 : b
$@ : a b c

$1 : new_a
$2 : new_b
$@ : new_a new_b new_c
```
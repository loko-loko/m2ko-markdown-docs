---
tags: [System, Shell, Commands]
title: Find
---

# Shell - Find Command

## Examples

Delete files with mtime > 60 days:

```bash
find /data -type f -mtime +60 -delete
```

List empty directories with mtime > 30 days:

```bash
find /data -type d -empty -mtime +30
```

List files with size > 1000 on / filesystem:

```bash
find / -xdev -size +1000
```
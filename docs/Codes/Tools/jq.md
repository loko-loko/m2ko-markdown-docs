---
tags: [System, JSON, Commands]
title: JQ
---

# JQ

## Syntax

Get only keys:

```bash
jq -r '.mydict|keys[]' file.json
```

## Examples

Get values with regex condition:

```bash
oc get pv -o json | jq -r '
  .items[]
    | select(.spec.csi.driver | test("csi-powermax"))
    | .metadata.name
      + ";" + .spec.storageClassName
      + ";" + .spec.claimRef.namespace
      + ";" + .spec.claimRef.name
      + ";" + .spec.csi.volumeHandle
' | column -t -s';'
```

Jq request containing filtering and selecting and csv formatting . This one permits to extract a list of container image for a specific repository

```bash
oc get po -A -o json |jq -r '.items[]|select(contains(.spec.containers[].image="ocp-311"))|[.metadata.name,.metadata.namespace,.metadata.ownerReferences[0].kind,.status.phase,.spec.containers[].image]|@csv'
```

Jq request with sorting and csv formatting

```bash
kubectl get nodes -o json |   jq -r '.items | sort_by(.status.capacity.memory)[]|[.metadata.name,.status.capacity.memory,.status.allocatable.memory] | @csv'
```

Jq with bash variable injection And object transformation to array because variable on filter pattern is not possible
```
cat TokenFile.json |jq -r --arg MYVAR "$MYVAR" '.tokens[]|to_entries[]|select(.key == $MYVAR )|.value'
```

Jq extract CPU requests of all containers / all pods / one line per container 

```jq
# Jq is fine with line breaks , no need to add backslash
# As there can be several containers for only one .metadata.whatever value , we need to save it to be able to reuse several times
# 'map' command takes an array as input and gives an array as output with selected values / no keys

oc get po -A -o json \ 
|jq --raw-output '.items[]
| .metadata as $meta
| .spec.nodeName as $noden
| .spec.containers
| map ([$meta.name,$meta.namespace,$noden,.name,.resources.requests.cpu])
| sort_by(.[:4][])
| .[]
|@csv '
```

Jq cheatsheet

https://medium.com/geekculture/my-jq-cheatsheet-34054df5b650

Jq Tutorial

```bash
npm install jq-Tutorial
```

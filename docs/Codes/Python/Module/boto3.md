---
tags: [Python, Code, Module]
title: Boto3
---

# Python - Boto3

S3 Storage python client.

## Install

Install with pip:

```bash
pip install boto3
```

## Samples

List buckets:

```python
import boto
import boto.s3.connection

access_key = "XXXXXXXXXXX"
secret_key = "XXXXXXXXXXXXXXXXXXXXXX"
s3_url = "s3.com"

conn = boto.connect_s3(
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
    host=s3_url,
    calling_format=boto.s3.connection.OrdinaryCallingFormat(),
)

# List bucket
for bucket in conn.get_all_buckets():
    print("%(name)s: %(created)s" % dict(
       name=bucket.name,
       created=bucket.creation_date
    ))
```

---
tags: [Python, Code, Module]
title: Regex (re)
---

# Python - Module Regex (re)

## Description

Le module `re` permet de faire des op&eacute;rations avec des expression r&eacute;guli&egrave;res. Le module est natif sur Python, il suffit de l&#39;importer en d&eacute;but de code comme ceci:

```python
import re
```

Voici un tableau comprennant les syntaxe d&#39;expressions r&eacute;guli&egrave;res les plus courantes:

|Expression |Description
|--- |---
|`.` |Valide tout caractère à l’exception du saut de ligne
|`^` |Détermine le début d'une chaine de caractère
|`$` |Détermine la fin d'une chaine de caractère
|`*` |Valide 0 répétition ou plus de l’expression qui précède
|`+` |Valide 1 répétition ou plus de l’expression qui précède
|`?` |Valide 0 ou 1 répétition de l’expression qui précède
|`{n}` |Valide n copies de l’expression qui précède
|`{n,m}` |Valide l’expression qui précède entre n et m copies
|`\` |Permet d'échapper un caractère spécial
|`[]` |Valide un ensemble de caractère
|`A\|B` |Valide soit A, soit B (OU)
|`\A` |Correspond uniquement au début d’une chaîne de caractères
|`\b` |Correspond à une chaine vide
|`\B` |Correspond à une chaine vide, mais uniquement au début ou à la fin d'un mot
|`\d` |Correspond à un chiffre `[0-9]`
|`\D` |Correspond à tout caractère qui n'est pas un chiffre `[^0-9]`
|`\s` |Correspond à un caractère d'espacement `[ \t\n\r\f\v]`
|`\S` |Correspond à tout caractère qui n'est pas un caractère d'espacement `[^ \t\n\r\f\v]`
|`\w` |Correspond à tout caractère de mot `[a-zA-Z0-9_]`
|`\W` |Correspond à tout caractère qui n'est pas un caractère de mot `[^a-zA-Z0-9_]`
|`\Z` |Correspond uniquement à la fin d’une chaîne de caractères

Voici la pr&eacute;sentation de quelques m&eacute;thodes pratiques du module `re`.

## Méthodes

### re.compile

```
re.compile(pattern)
```

La m&eacute;thode compile permet de compiler une expression r&eacute;guli&egrave;re afin d&#39;accroitre la rapidit&eacute; d&#39;execution des m&eacute;thodes match() et search(). Cela est tr&egrave;s utile quand on doit utiliser une m&ecirc;me expression plusieurs fois dans un script afin de gagner en temps d&#39;execution:

```python
lst = ['string1', 'string2', 'string3']
expr_compile = re.compile(r'^s.*\d')

for l in lst:
   if expr_compile.match(l):
       print 'Yes'
   else:
       print 'No'

# Yes
# Yes
# Yes
```

### re.search

```
re.search(pattern, string, flags=0)
```

La m&eacute;thode `re.search` analyse la chaine de caract&egrave;re &agrave; la recherche du premier emplacement o&ugrave; l&rsquo;expression rationnelle trouve une correspondance, et renvoie l&rdquo;objet de correspondance trouv&eacute;. Renvoie `None `si rien n&#39;est trouv&eacute;:

```python
var = 'Test92'

if re.search(r'^T[a-z]+\d+', var):
   print 'Find'
else:
   print 'Not Find !'

# Find
```

### re.match

```
re.match(pattern, string, flags=0)
```

La m&eacute;thode `re.match` analyse le d&eacute;but de la chaine de caract&egrave;re &agrave; la recherche du premier emplacement o&ugrave; l&rsquo;expression rationnelle trouve une correspondance, et renvoie l&rdquo;objet de correspondance trouv&eacute;. Renvoie `None `si rien n&#39;est trouv&eacute;.

!!! warning

    A la difference de la m&eacute;thode `search`, `match` ne renvoie que les chaines de caract&egrave;res commen&ccedil;ant par l&#39;expression r&eacute;guli&egrave;re

### re.split

```
re.split(pattern, string, maxsplit=0, flags=0)
```

Le module `re.split` permet de d&eacute;couper une chaine de caract&egrave;re en fonction de l&#39;expression r&eacute;guli&egrave;re en argument:

```python
re.split(r'\W+', 'Words, words, words.')
# ['Words', 'words', 'words', '']
re.split(r'\W+', 'Words, words, words.', 1)
# ['Words', 'words, words.']
```

## Utilisation

### Expression Capturante

Il est possible de capturer une ou plusieurs chaine de caract&egrave;re et les r&eacute;cup&eacute;rer avec un id ou une chaine de caract&egrave;re.

```python
string='lsan_em7062_tiths514_6a59d4_cm6032_00923_FA03D28_FA04D28'
```

Voici comment les r&eacute;cup&eacute;rer avec un id:

```python
m = re.search(r'_([eck]m\d+)_.*_([eck]m\d+)_', string)

m.group(0)
> '_em7062_tiths514_6a59d4_cm6032_'
m.group(1)
> 'em7062'
m.group(2)
> 'cm6032'
```

Voici comment les r&eacute;cup&eacute;rer avec une chaine de caract&egrave;re:

```python
m = re.search(r'_(?P<first_sw>[eck]m\d+)_.*_(?P<sec_sw>[eck]m\d+)_', string)

m.group('first_sw')
> 'em7062'
m.group('sec_sw')
> 'cm6032'
```

---

[https://docs.python.org/fr/3.6/library/re.html](https://docs.python.org/fr/3.6/library/re.html)

---
tags: [Python, Code, Module]
title: SSH
---

# Python - SSH Modules

## Fabric

### Install

```
pip install fabric
```

### Usage

```python
#!/usr/bin/env python
#-*- coding: utf-8 -*-

from fabric.api import *

env.host_string = 'adresse_ip'
env.user = 'user'
env.password = 'password'
```

L&#39;&eacute;quivalent de la commande scp:

```python
get(remote_path="/backup/2014.sql.gz", local_path="/home/olivier/2014.sql.gz")
```

Ou en &eacute;criture courte:

```python
get("/backup/2014.sql.gz", "/home/olivier/2014.sql.gz")
```

Vous pouvez envoyer un fichier via SSH en utilisant la m&eacute;thode `put`:

```python
put('/home/olivier/file1.txt', '/backup/file2.txt')
```

Vous pouvez lancer une commande sur un servant distant comme ceci:

```python
run('python /scripts/script.py')
```

Executez des scripts avec les droits super utilisateur:

```python
sudo("mkdir /backup/2014")
```

## Paramiko​​​​​​​

### Install

```
pip install paramiko
```

### Usage

Exemple de fonction permettant l&#39;initialisation d&#39;une connexion SSH sur un switch (Avec diffusion de cl&eacute; pr&eacute;alable):

```python
def connect(user, srv):

    print 'SSH Connect on {0}'.format(srv)

    try:
        conn = paramiko.SSHClient()
        conn.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        conn.connect(srv, username=user)

    except:
        print 'SSH Connect on {0} Problem'.format(srv)
        return 1

    return conn
```

Ensuite on peut l&#39;initialiser et lancer une commande:

```python
connection = connect('admin', 'server1')
stdin, stdout, stderr = connection.exec_command('ls -l')
```

### Timeout

Il est possible de renseigner une valeur pour le&nbsp;timeout&nbsp;en argument de la fonction `connect()` (vu plus haut)&nbsp;:

```python
conn.connect('server1', username='admin', timeout=60)
```

La connection sera donc interrompu si elle n&#39;a pas pu se faire au bout du temps indiqu&eacute; (en seconde).

Il est aussi possible de mettre une limite sur le temps d&#39;&eacute;x&eacute;cution de la commande ssh. Il y a dans ce cas la 2 possibilit&eacute;s, la premi&egrave;re consiste &agrave; indiquer un timeout sur la `channel` de la sortie standard (`stdout`):

```python
stdout.channel.settimeout(120)
```

La deuxi&egrave;me m&eacute;thode consiste &agrave; attendre que le signal de fin soit envoy&eacute; par la `channel` de la sortie standard (`stdout`):

```python
cnt = 1

while not stdout.channel.eof_received:
    print 'command in progress'
    time.sleep(30)

    if cnt is 4:
       print 'time limit exceed (120)'
       break

    cnt += 1
```
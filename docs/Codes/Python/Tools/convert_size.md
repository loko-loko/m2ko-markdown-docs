---
tags: [Codes, Python, Tools]
title: Convert Size
---

# Python - Convert Size

A little function to convert size (B, KB, ..., PB)

## Codes

```python
from __future__ import division

def convert_size(size, unit = "B"):
    units = ["B", "KB", "MB", "GB", "TB", "PB"]
    unit_index = units.index(unit)
    if size < 1024:
        return "%(size)s %(unit)s" % dict(
            size=round(size, 1),
            unit=units[unit_index]
        )
    return convert_size(
        size=float(size / 1024),
        unit=units[unit_index + 1]
    )
```

## Usage

```python
convert_size(size=5000000)
# 4.8 MB
convert_size(size=1500, unit="KB")
# 1.5 MB
```
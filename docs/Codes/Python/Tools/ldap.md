---
tags: [Codes, Python, Tools]
title: LDAP Extract
---

# Python - LDAP Extract

## Description

Ce script permet de récuperer la liste de tous les groupes/users LDAP.

## How to use it

Créer un venv et installer les dépendances:

```bash
# Creer un venv
python3 -m venv venv

# Source venv
source venv/bin/activate

# Upgrade pip
pip install -U pip

# Install requirements.txt
pip install -r requirements.txt
```

!!! warning

    Le module python ldap à besoin des dépendances systèmes suivantes: `yum install python3-devel openldap-devel gcc`

Exporter le mot de passe du serveur LDAP:

```
export LDAP_PASSWORD="p@ssword"
```

Executer le script:

```bash
python ldap-search.py
```

Une fois terminée les 2 fichiers suivants sont générés:
  - `ldap-groups-by-member.csv`: Contient la liste de tous les users avec leurs groupes LDAP
  - `ldap-members-by-group.csv`: Contient la liste de tous les groupes LDAP avec leurs users

## Codes

**requirements.txt**

```
loguru==0.5.3
python-ldap==3.4.0
```

**ldap-search.py**

```python
import os
import sys
import csv
from pprint import pprint

import ldap
from loguru import logger

LDAP_URL = "myldap.com"
LDAP_BIND_DN = "cn=ReadLdap,ou=PERSONNES,o=MYORG"
LDAP_BASE_DN = "ou=GROUPES,o=MYORG"

GROUP_BY_MEMBERS_CSV_FILE = "ldap-groups-by-member.csv"
MEMBER_BY_GROUPS_CSV_FILE = "ldap-members-by-group.csv"

# Get LDAP Password from ENV
try:
    LDAP_PASSWORD = os.environ["LDAP_PASSWORD"]
except Exception as ex:
    logger.error("LDAP: Problem with en vars: {ex}")
    exit(1)

# Init LDAP client
search_scope = ldap.SCOPE_SUBTREE
try:
    logger.info(f"LDAP: Initialize connection -> {LDAP_URL}")

    ldap_client = ldap.initialize(f'ldap://{LDAP_URL}:389')
    ldap_client.protocol_version = ldap.VERSION3
    ldap_client.simple_bind_s(LDAP_BIND_DN, LDAP_PASSWORD) 

except ldap.INVALID_CREDENTIALS:
  logger.error("LDAP Error: Your username or password is incorrect.")
  exit(1)

except ldap.LDAPError as ex:

  if type(e.message) == dict and e.message.has_key('desc'):
      logger.error(f"LDAP Error: {ex.message['desc']}")
  else: 
      logger.error(f"LDAP Error: {ex}")

  exit(1)

# Search in LDAP
try:
    logger.info(f"LDAP: Search -> {LDAP_BASE_DN}")
    ldap_result_id = ldap_client.search(
        LDAP_BASE_DN,
        search_scope
    )
    data = []
    while True:
        result_type, result_data = ldap_client.result(ldap_result_id, 0)
        if result_data == []:
            break
        elif result_type != ldap.RES_SEARCH_ENTRY:
            continue
        data += result_data

except ldap.LDAPError as ex:
    logger.error(f"LDAP Error: {ex}")

# Disconnect from LDAP
ldap_client.unbind_s()


# Get Members by Group
logger.info("LDAP: Get Members by Group")
group_members = {}
for headers, ldap_data in data:
    name = ldap_data.get("cn")

    if not name:
        continue
    name = name[0].decode("utf-8")
    members = ldap_data.get("uniqueMember")

    if not members:
        continue
    members = [
        m.decode("utf-8").split(",")[0].split("=")[1]
        for m in members
    ]
    group_members[name] = members

# Get Group by Members
logger.info("LDAP: Get Group by Members")
member_groups = {}
for group, members in group_members.items():
    for member in members:
        if not member_groups.get(member):
            member_groups[member] = []
        member_groups[member].append(group)

# Write CSV Output for Members by Group
logger.info(f"LDAP: Write CSV Output for Members by Group: {MEMBER_BY_GROUPS_CSV_FILE}")
headers = ["Group", "Members"]
with open(MEMBER_BY_GROUPS_CSV_FILE, "w", encoding="UTF8", newline="") as f:
    writer = csv.writer(f, delimiter=";")
    writer.writerow(headers)

    for group, members in group_members.items():
        data = [group, ",".join(members)]
        writer.writerow(data)

# Write CSV Output for Group by Members
logger.info(f"LDAP: Write CSV Output for Groups by Member: {GROUP_BY_MEMBERS_CSV_FILE}")
headers = ["Member", "Groups"]
with open(GROUP_BY_MEMBERS_CSV_FILE, "w", encoding="UTF8", newline="") as f:
    writer = csv.writer(f, delimiter=";")
    writer.writerow(headers)

    for member, groups in member_groups.items():
        data = [member, ",".join(groups)]
        writer.writerow(data)
```

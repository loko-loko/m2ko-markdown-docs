---
tags: [Codes, Python, Tools]
title: Week's Day
---

# Python - Week's Day

## Description

Here is a function allowing to recover the day of the week according to a date

## Argument

Function&#39;s arguments are:

*   `year`, `month` and&nbsp;`day`
*   `short`: return&nbsp;only the 3 first letters of day (default:`False`)

## Code

```python
import datetime

def week_day(year, month, day, short=False):
    days=["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    day_nb = datetime.date(year, month, day).weekday()
    day_name = days[day_nb]
    if short:
        day_name = day_name[0:3]
    return day_name
```

## Examples

```python
week_day(2018, 11, 5)
"Monday"

week_day(1989, 11, 10, short=True)
"Mon"

input_date = {"year": 1954, "month": 11, "day": 17}
week_day(**input_date)
"Wednesday"
```
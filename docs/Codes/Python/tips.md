---
tags: [Python, Code]
title: Tips
---

# Python - Tips

## Useful

Execute http server:

```bash
# Python 2.x
python -m SimpleHTTPServer {port}

# Python 3.x
python -m http.server {port}
```

Format JSON:

```bash
cat data.json | python -m json.tool
```

Download file from http (like `curl`):

```bash
python -c 'import urllib;urllib.urlretrieve("url", "dest")'
# Ex:
#   python -c 'import urllib;urllib.urlretrieve("https://m2ki.com/file.txt", "/data/file.txt")'
```

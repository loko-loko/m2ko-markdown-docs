---
tags: [System, Hardware, ILO]
title: Dell Idrac
---

# Hardware - Dell Idrac

## Tips

Lancer la console en CLI:
```bash
# Se connecter en SSH sur l'idrac
ssh root@server-idrac

# Lancer la commande suivante:
console com2
```

---
tags: [System, Windows]
title: Network
---

# Windows - Network

## Tips

Test connection with curl:

```bash
curl telnet://{server}:{port}
```

---
tags: [Data, Logs, Monitoring]
title: Graylog
---

# Monitoring - Graylog

## Tips

Send GELF TCP input message with `nc`:

```
echo -e '{"version": "1.1","host":"example.org","short_message":"Short message","full_message":"Backtrace here\n\nmore stuff","level":1,"_user_id":9001,"_some_info":"foo","_some_env_var":"bar"}\0' | nc -w 1 graylog-server 12201
```

## Examples

### Pipeline rules

Drop message with a specific message:

```ruby
rule "Drop ovn hexa messages for OCP4"
when
contains(to_string($message.message), "....")
and contains(to_string($message.namespace_name), "openshift-ovn-kubernetes")
then
drop_message();
end
```

Format message:

```ruby
rule "Format OCP4 Ingress logs"
when
contains(to_string($message.ocp_cluster), "ocp4")
and to_string($message.namespace_name) == "openshift-ingress"
and contains(to_string($message.message), "router-log")
then
let fmt_message = regex_replace(".*: (.*)", to_string($message.message), "$1");
set_fields(to_map(parse_json(fmt_message)));
end
```

### Dashboard

Example of bart chart:

![](../../_images/Others/Monitoring/graylog_bart_chart.png)

Example of pie chart:

![](../../_images/Others/Monitoring/graylog_pie_chart.png)

Example of table filter:

![](../../_images/Others/Monitoring/graylog_table_filter.png)

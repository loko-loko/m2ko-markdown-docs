---
tags: [Cloud, Google, GCP]
title: Client
---

# GCP - Client

## Commands

### Basics

Login to gcloud:

```bash
gcloud auth login
```

### Networks

Reserve static private ip address:

```bash
gcloud compute addresses create ingress-private \
    --project=stime-dotsti-hprd-sbx \
    --region=europe-west9 \
    --subnet=projects/stime-dothub-hprd/regions/europe-west9/subnetworks/stime-dot-sti-sbx-subnetwork \
    --addresses=192.168.137.98
```

Delete static private ip address:

```bash
gcloud compute addresses delete ingress-private \
    --project=stime-dothub-hprd \
    --region=europe-west9
```

List addresses:

```bash
gcloud compute addresses list --project=stime-dotsti-hprd-sbx
```

### Iam

```bash
gcloud auth activate-service-account gitlab-ci-sa@stime-dotdigital-hprd.iam.gserviceaccount.com \
  --key-file=./sa-keys/gitlab-ci-sa.json
```

```bash
gcloud iam service-accounts list \
  --project=stime-dotdigital-hprd
```

```bash
gcloud container clusters get-credentials stime-dotdigital-hprd-gke \
  --region=europe-west9-a \
  --project=stime-dotdigital-hprd
```

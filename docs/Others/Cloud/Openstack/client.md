---
tags: [Cloud, Openstack]
title: Client
---

# Openstack - Client

## Instance

Lister les flavors:

```
openstack flavor list
```

Lister les instances:

```
openstack server list (--project=<project>)
```

D&eacute;tail d&#39;une instance:

```
openstack server show <instance>
```

Cr&eacute;er une VM:

```
openstack server create \
    --network <network> \
    --availability-zone <az> \
    --flavor <flavor> \
    --image <image> \
    --security-group <security_group> \
    --config-drive True \
    (--key-name <key> \)
    <name>
```

## Project

Lister les projets:

```
openstack project list
```

## Security Group

Cr&eacute;&eacute;r un security group:

```
openstack security group create <security_group_name> --project <project_name>
```

Lister les r&egrave;gles d&#39;un security group:

```
openstack security group rule list <security_group>
```

D&eacute;tail d&#39;un security group:

```
openstack security group show <security_group>
```

Cr&eacute;ation de r&eacute;gles dans un security group:

```
openstack security group rule create --protocol <tcp|udp|icmp> --dst-port <port_range> --remote-ip <ip/mask>
```

## Quota

D&eacute;tail du Quota d&#39;un projet:

```
openstack quota show <project>
```

Changer le Quota de volume sur un projet:

```
openstack quota set --volumes <value>
```

## Volume

Cr&eacute;er un volume:

```
openstack volume create --type <volume_type> --size <size> --availability-zone <az> <name>
```

Changer l&#39;&eacute;tat d&#39;un volume (`error`):

```
openstack volume set <volume> --state error
```

Supprimer un volume :

```
openstack volume delete <volume>
```

Attacher/d&eacute;tacher un volume :

```
openstack server add|remove volume <instance> <volume>
```

## Host/Service

Lister les services Cinder :

```
openstack volume service list
```

Activer/D&eacute;sactiver un service:

```
openstack volume service set --enable/--disable <host> <service>
```

Lister les hosts:

```
openstack host list
```

Lister les ressources disponible par host (CPU/Mem):

```
openstack host show <host>
```

## QoS

Cr&eacute;er une&nbsp;QoS:

```
openstack volume qos create <qos> --consumer front-end --property <key=value>
```

Supprimer une&nbsp;QoS:

```
openstack volume qos delete <qos>
```

Lister les volume-type&nbsp;:

```
openstack volume type list
```

Associer une&nbsp;QoS &agrave; un volume-type:

```
openstack volume qos associate <qos> <volume_type>
```

D&eacute;sassocier&nbsp;une QoS&nbsp;d&#39;un volume-type:

```
openstack volume qos disassociate --volume-type <volume_type> <qos>
```

## Scripts

Activer (`--enable`) ou d&eacute;sactiver (`--disable`) un service:

```bash
openstack volume service list | awk '/<host>/ \
{printf "openstack volume service set --enable %s %s\n", $4, $2}'
```

Lister toutes les ressources des hosts compute:

```bash
openstack host list | awk '/compute/ {print "openstack host show " $2}' | sh
```
---
tags: [DevOps, Git, Infra]
title: GitLab
---

# Tools - GitLab

## Commands

### Rake

Before use rake, load the following var:

```bash
export RAILS_ENV=production
```

Gitlab info:

```bash
bundle exec rake gitlab:env:info
```

See all bundle commands:

```bash
bundle exec rake --tasks
```

---
tags: [DevOps, Git, Client]
title: Git
---

# Tools - Git

## Install

Install with yum:

```bash
yum install git
```

Install git-2.* on CentOS7:

```bash
# Add endpoint repo
yum install https://packages.endpointdev.com/rhel/7/os/x86_64/endpoint-repo-1.10-1.x86_64.rpm

# Verify version
yum list --show-duplicates | grep git

# Update git
yum update git
```

## Commands

### Configs

Set color to auto:

```bash
git config --global color.ui auto
```

Add automatically `crlf` (`^M`) at the end of each line:

```bash
git config --global core.autocrlf true
```

Change default editor (Default: `$VISUAL` or `$EDITOR`):

```bash
git config --global core.editor {editor}
# Ex: git config --global core.editor vi
```

### Logs

Get formatted log commit:

```bash
git log --pretty=oneline --graph --decorate --all
```

Get reflog actions:

```bash
git reflog
```

Display detail of last commit:

```bash
git log -1 HEAD --stat
```

### Troubleshooting

Set trace:

```bash
export GIT_TRACE=1
export GIT_TRACE_PERFORMANCE=1

# Or
GIT_TRACE=1 git pull
```

## Tips

### Alias

|Alias |Description |Command
|--- |--- |---
|**`git last`** |Display last commit detail |`git config --global alias.last 'log -1 HEAD --stat'`
|**`git cm`** |Commit with message |`git config --global alias.cm 'commit -m'`
|**`git pdiff`** |Pretty diff |`git config --global alias.pdiff 'difftool -t vimdiff -y'`
|**`git search`** |Search string in commits |`git config --global alias.search '!git rev-list --all \| xargs git grep -F'`
|**`git ll`** |Display oneline logs |`git config --global alias.ll 'log --oneline'`
|**`git st`** |Display short status |`git config --global alias.st 'status -sb'`
|**`git gl`** |Display git configs |`git config --global alias.gl 'config --global -l'`

### Autocompletion

Add autocompletion for git commands:

```bash
# Download completion script
curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash >> ~/.git-completion.bash

# Add line on bashrc file to source it
echo "source ~/.git-completion.bash" >> ~/.bashrc
```
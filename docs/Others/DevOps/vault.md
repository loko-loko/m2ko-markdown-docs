---
tags: [DevOps, HashiCorp, Vault, jq]
title: Vault
---

# Tools - Vault

## Client

### Install

Install from official site:

```bash
CLIENT_VERSION=1.8.7

# Download archive
curl -LO https://releases.hashicorp.com/vault/${CLIENT_VERSION}/vault_${CLIENT_VERSION}_linux_amd64.zip

# Extract
unzip vault_${CLIENT_VERSION}_linux_amd64.zip

# Copy to bin path (admin)
mv vault /usr/local/bin

# Clean zip
rm -f vault_${CLIENT_VERSION}_linux_amd64.zip
```

### Setup

Before use vault client, export vault url:

```bash
export VAULT_ADDR=https://my-vault.m2ko.com
```

To use a specific namespace export the following var:

```bash
export VAULT_NAMESPACE=my_vault_ns
```

### Commands

Login with LDAP:

```bash
vault login \
  -method=ldap \
  username={username} \
  (password={password})
# Ex: vault login -method=ldap username=lkone
```

List kv:

```bash
vault kv list {kv}
# Ex: vault kv list secret
```

List secret:

```bash
vault kv get -format json -field=data unixlinux/secret/cicd/gitlab/preprod/gitlab-runner 
```

List secret and filter data with JQ ( with bash variable injection)

```bash
export mykey="key_to_retrieve"
vault kv get -format json -field=data unixlinux/secret/cicd/gitlab/preprod/gitlab-runner |jq -r --arg app "$mykey" '.|to_entries[]|select(.key==$mykey)'
````

Extract vault key/values inside a secret and export them to bash
```bash
vault kv get -format json -field=data unixlinux/secret/cicd/gitlab/preprod/s3/cache |jq -r '.|to_entries[]|"export \(.key)=\(@sh"\(.value)")"'
export accesskey='6ZCEO-NOT-REAL-3R9O'
export secretkey='C6omB4J2TA-NOT-REAL-N8QTYFOIgpkDJ'

# pour importer directement dans l env courant
source <(vault kv get -format json -field=data unixlinux/secret/cicd/gitlab/preprod/s3/cache |jq -r '.|to_entries[]|"export \(.key)=\(@sh"\(.value)")"')
```

> @sh ci dessus permet de proteger les values jq avant de les rendre visible au shell

---
tags: [DevOps, Tools, Python]
title: Ad-Hoc
---

# Ansible - Ad-Hoc

## Commands

Connectivity test:

```bash
ansible -m ping all
```

Run local script on remote server:

```bash
ansible -m script -a {script} all
# Ex: ansible -m script -a script.sh all
```

Run shell on remote server:

```bash
ansible -m shell -a "{command}" all
# Ex: ansible -m shell -a "ls -l" all
```

Get config from a host (Gathering facts):

```bash
ansible -m setup all
```

## Link

- https://www.middlewareinventory.com/blog/ansible-ad-hoc-commands/

---
tags: [DevOps, Tools, Python]
title: Callback
---

# Ansible - Callback

## Configs

Example of config from `ansible.cfg` file:

```ini
[defaults]
stdout_callback = unixy
```

```ini
[defaults]
callback_whitelist = profile_tasks
```

## DIY Module

Example of config:

```ini
[defaults]
stdout_callback = community.general.diy
bin_ansible_callbacks = True

[callback_diy]
on_any_msg = "{{ ansible_callback_diy.result.output.stdout }}"
```

!!! note

    Before use this plugin you must install
    the ansible `community.general` collection:

    ```bash
    ansible-galaxy collection install community.general
    ```

## Link

- https://www.middlewareinventory.com/blog/ansible-ad-hoc-commands/

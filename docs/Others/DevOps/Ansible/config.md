---
tags: [DevOps, Tools, Python]
title: Config
---

# Ansible - Config

## General

Global configs:

```ini
[defaults]
inventory = /path/to/hosts              # Ansible inventory
remote_user = user1                     # Remote user used by ansible
private_key_file = /path/to/priv.key    # Private key
remote_tmp = /tmp                       # Remote temporary dir use by Ansible (Default $HOME/.ansible/tmp)
forks = 5                               # Parallele forked process
```

---
tags: [DevOps, Tools, Python]
title: Modules
---

# Ansible - Modules

## Hashi Vault

Usage examples:

```yaml
# Defined values
- set_fact:
    vault_url: "https://vault.com"
    vault_namespace: myns
    vault_secrets:
      - name: "flop"
        path: "secret/plop/flop"

# Get secrets with lookup hashi_vault
- name: get secrets from vault
  tags: [build_iso, vmware]
  ansible.builtin.set_fact:
    vault: "{{ vault | default({}) | combine({item.name: lookup('hashi_vault', 'secret={{ vault_namespace }}/data/{{ item.path }} token={{ vault_token }} url={{ vault_url }} auth_method=token')}) }}"
  with_items: "{{ vault_secrets }}"
```

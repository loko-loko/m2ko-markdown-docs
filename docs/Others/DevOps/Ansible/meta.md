---
tags: [DevOps, Tools, Python]
title: Meta
---

# Ansible - Meta

## flush_handlers

Start all trigger handlers:

```yaml
- meta: flush_handlers
```

!!! warning

    `flush_handlers` meta Not work with condition

## end_play

End of playbook:

```yaml
- meta: end_play
```

---
tags: [SQL, Database]
title: Oracle
---

# Oracle

## Commands

### Globals

See declared instance:

```bash
cat /etc/oratab
```

Found instance alert logs file:

```bash
find {oracle-path} -name "alert_{instance}.log"
# Ex: find /softwares/oracle -name "alert_ORA8.log"
```

### Oracle RAC

Oracle RAC user: `grid`

List cluster's nodes:

```bash
olsnodes -i
```

Check cluster status:

```bash
crsctl check cluster -all
# Ex: crsctl check cluster -all
```

Check status of Oracle High Availability Services and the Oracle Clusterware stack on the local server:

```bash
crsctl check crs
```

Get configured node role of nodes in the cluster:

```bash
crsctl get node role {config|status} -all
# Ex: crsctl get node role status -all
```

## Procedure

Check status of an instance:

```bash
# Login with oracle user
su - oracle

# Load oracle envs, Select instance: ORACLE_SID
. oraenv

# Start oracle client
sqlplus / as sysdba

# Check status
select open_mode from v$database ;
```

## Links

- https://docs.oracle.com/database/121/CWADD/GUID-6A4E73C4-E1A0-4D90-B69A-7B2B4DAB66BA.htm#CWADD91595

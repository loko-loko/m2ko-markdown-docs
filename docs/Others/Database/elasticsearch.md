---
tags: [Database, NoSQL]
title: Elasticsearch
---

# Database - Elasticsearch

## Commands

Custom Elastic vars:

```bash
# Custom elastic vars
el_host=$(hostname --fqdn)
el_user="elastic"
el_password="passwd"
el_cacert="/etc/elasticsearch/certs/ca.crt"

alias ecurl="curl --user $el_user:$el_password --cacert $el_cacert"
```

### Globals

Cluster Health:

```bash
ecurl -X GET "https://$el_host:9200/_cluster/health?pretty"
```

Available storage:

```bash
ecurl -X GET "https://$el_host:9200/_cat/allocation?v&pretty"
```

Cluster Version:

```bash
ecurl -X GET "https://$el_host:9200"
```

Display master node:

```bash
ecurl -X GET "https://$el_host:9200/_cat/master"
```

Display shards:

```bash
# All shards
ecurl -X GET "https://$el_host:9200/_cat/shards"

# Shards with reloc in Progress
ecurl -X GET -s "https://$el_host:9200/_cat/shards" | grep RELOC

# Count par state
ecurl -X GET -s "https://$el_host:9200/_cat/shards?h=state" | sort | uniq -c

# Check reloc in Progress
ecurl -X GET -s "https://$el_host:9200/_cat/shards" | awk '/RELOC/ && $NF == "'"$(hostname)"'"'
```

### Snapshot

Create snapshot:

```bash
ecurl -X PUT "https://$el_host:9200/_snapshot/es_snapshot/{snapshot-name}" \
  -H 'Content-Type: application/json' \
  -d '{ "accepted": true }'
```

Display available snapshot:

```bash
ecurl -X GET "https://$el_host:9200/_snapshot/es_snapshot?v&s=id&pretty"
```

Delete snapshot:

```bash
ecurl -X DELETE "https://$el_host:9200/_snapshot/es_snapshot/{snapshot-name}"
```

## Procedures

### Elasticsearch HQ

Start Elasticsearch HQ in container:

```bash
host="myhost"
user="elastic"
password="passwd"
cacert="/home/lkone/elastic-ca.crt"

docker run -d \
  -p 5000:5000 \
  --name elasticsearch-hq \
  -v $cacert:/etc/ssl/certs/elastic-ca.crt \
  -e HQ_DEFAULT_URL="https://$user:$password@$host:9200" \
  -e HQ_CA_CERTS=/etc/ssl/certs/elastic-ca.crt \
  -e HQ_VERIFY_CERTS=True \
  -e HQ_ENABLE_SSL=True \
  elastichq/elasticsearch-hq
```

### Sortir un noeud

Vérifier l'état du cluster (green):

```bash
ecurl -X GET "https://$el_host:9200/_cluster/health?pretty"
```

Vérifier ou se situe le master:

```bash
ecurl -X GET "https://$el_host:9200/_cluster/health?pretty"
```

!!! warning

    Si le node est le master s'assurer que la bascule se fait bien après l'arrêt du service elasticsearch 

Mettre les règles d'allocation uniquement sur les shards primaires pour éviter la re-balance (relocating) des shards durant l'arrêt du node:

```bash
ecurl -X PUT "https://$el_host:9200/_cluster/settings" \
  -H "Content-Type: application/json" \
  -d '{"persistent": {"cluster.routing.allocation.enable": "primaries"}}'
```

Flusher les index du cluster:

```bash
ecurl -X GET "https://$el_host:9200/_flush"
```

Arréter le noeud:
```bash
systemctl stop elasticsearch
```

!!! note

    Faire les actions sur le noeud stoppé

Démarrer le noeud:
```bash
systemctl start elasticsearch
```

Vérifier l'état du cluster (yellow):
```bash
ecurl -X GET "https://$el_host:9200/_cluster/health?pretty"
```

!!! note

    Le cluster doit voir tous ses noeuds mais reste en yellow

Remettre les règles d'allocation routing:
```bash
ecurl -X PUT "https://$el_host:9200/_cluster/settings" \
  -H "Content-Type: application/json" \
  -d '{"persistent": {"cluster.routing.allocation.enable": null}}'
```

Vérifier l'état du cluster (green):
```bash
ecurl -X GET "https://$el_host:9200/_cluster/health?pretty"
```

!!! note

    Après que les unassigned shards passent actives le statut passe en green

## Problem

### Blocked index to read-only

By default, Elasticsearch installed goes into read-only mode when you have
less than 5% of free disk space. If you see errors similar to this:

```json
{
    "error": {
        "reason": "blocked by: [FORBIDDEN/12/index read-only / allow delete (api)];",
        "root_cause": [
            {
                "reason": "blocked by: [FORBIDDEN/12/index read-only / allow delete (api)];",
                "type": "cluster_block_exception"
            }
        ],
        "type": "cluster_block_exception"
    },
    "status": 403
}
```

Resolution:

```bash
ecurl -XPUT -H "Content-Type: application/json" \
   "https://$(hostname):9200/_all/_settings" \
   -d '{"index.blocks.read_only_allow_delete": false}'
```

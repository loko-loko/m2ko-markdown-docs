---
tags: [Database, NoSQL]
title: Etcd
---

# Database - Etcd

## Commands

Connection to influxdb:

```bash
# With env 
export INFLUX_USERNAME=influxadmin
export INFLUX_PASSWORD=XxxxxxXXXXX

influx

# With argument
influx -username=influxadmin -password=XxxxxxXXXXX
```

Display databases

```bash
show databases
```

## Query

### Flux

```bash
from(bucket: v.defaultBucket)
    |> range(start: -1d)
    |> filter(fn: (r) => r["_measurement"] == "anomalies_count")
    |> group(columns: ["app_functional_group"])


from(bucket: v.defaultBucket)
    |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
    |> filter(fn: (r) => r["_measurement"] == "anomalies_count")
    |> group(columns: ["app_functional_group"])
    |> drop(columns: ["_start", "_stop"])


from(bucket: v.defaultBucket)
    |> range(start: -2d)
    |> filter(fn: (r) => r["_measurement"] == "anomalies_count")
    |> group(columns: ["app_functional_group"])
    |> sum()
    |> last()
    |> drop(columns: ["_start", "_stop"])


from(bucket: v.defaultBucket)
    |> range(start: -2d)
    |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
    |> group()
    |> distinct(column: "server_os_type")
    
```

---
tags: [SQL, Database]
title: Patroni
---

# Patroni

## Commands

Get cluster status:

```bash
patronictl list
```

Get cluster config:

```bash
patronictl show-config
```

Edit cluster config:

```bash
patronictl edit-config
```

Failover to a replica:

```bash
patronictl failover \
  --master={current-master}
  --candidate={candidate}
```

Switchover to a replica:

```bash
patronictl switchover \
  --master={current-master}
  --candidate={candidate}
```

Restart a member:

```bash
patronictl restart {cluster} {member}
```

Reinit a member:

```bash
patronictl reinit {cluster} {member}
```

## API

Get endpoint status:

```bash
curl -s http://localhost:8008/patroni | jq .
```

Get cluster status:

```bash
curl -s http://localhost:8008/cluster | jq .
```

Get event history:

```bash
curl -s http://localhost:8008/history | jq .
```

Get config:

```bash
curl -s localhost:8008/config | jq .
```

Failover to a replica:

```bash
curl -XPOST -s http://localhost:8009/failover -d '{"candidate":"{candidate}"}'
```

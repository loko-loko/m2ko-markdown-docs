---
tags: [SQL, Database]
title: MariaDB
---

# Database - MariaDB

## Commands

Start mysql client:

```bash
mysql
```

Display databases:

```sql
show databases;
```

Use database:

```sql
use {database};
```

Display field of table:

```sql
describe {table};
```

---
tags: [SQL, Database]
title: SQL
---

# Database - SQL

## Request Examples

Select with count:

```sql
SELECT file,count(*)
FROM ci_job_artifacts
WHERE file_store=2 GROUP BY file;
```

Select with count and join:

```sql
SELECT ci_job_artifacts.file,projects.archived,count(ci_job_artifacts.*) AS counter
FROM ci_job_artifacts
INNER JOIN projects
ON ci_job_artifacts.project_id=projects.id
WHERE file_store=1
GROUP BY ci_job_artifacts.file,projects.archived;
```

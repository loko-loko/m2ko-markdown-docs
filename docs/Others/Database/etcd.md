---
tags: [Database, NoSQL]
title: Etcd
---

# Database - Etcd

## Install

Download bin from github repository:

```bash
etcd_version=3.5.1

# Download package
curl -LO https://github.com/etcd-io/etcd/releases/download/v${etcd_version}/etcd-v${etcd_version}-linux-amd64.tar.gz

# Decompress files
tar -xzvf etcd-v${etcd_version}-linux-amd64.tar.gz

# Copy bin to /usr/local/bin path
sudo mv etcd-v${etcd_version}-linux-amd64/etcd* /usr/local/bin
```

Create systemd service:

```ini
# /etc/systemd/system/etcd.service
[Unit]
Description="Etcd database"

[Service]
User=root
ExecStart=/usr/local/bin/etcd \
  --name=db \
  --data-dir=/var/lib/etcd
Restart=always

[Install]
WantedBy=multi-user.target
```

Enable service:

```bash
systemctl enable --now etcd
```

## Commands

Export etcd API version:

```bash
export ETCDCTL_API=3
```

### Global

Get etcd version:

```bash
etcdctl version
```

Health of endpoints:

```bash
etcdctl endpoint health
```

Status of endpoints:

```bash
etcdctl endpoint status
```

### Actions

Create key:

```bash
etcdctl put {key} {value}
# Ex: etcdctl put plop ploppy
```

Display key:

```bash
etcdctl get [ {key} | --prefix {key-prefix} (--limit={limit}) ] (--print-value-only)
# Examples:
#   Return plop key:
#     etcdctl get plop
#
#   Return all keys start with plop:
#     etcdctl get --prefix plop
#
#   Return 2 first keys start with plop:
#     etcdctl get --prefix plop --limit=2
```

Delete key:

```bash
etcdctl del [ {key} ({other-key}) | --prefix {key-prefix} ]
# Ex: etcdctl del key1 key2
```
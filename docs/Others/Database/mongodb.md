---
tags: [Database, NoSQL]
title: MongoDB
---

# Database - MongoDB

## Commands

Start mongodb client:

```bash
mongo \
  --host=localhost \
  --port=27017 \
  --username=$MONGODB_USERNAME \
  --password="$MONGODB_PASSWORD" \
  --authenticationDatabase \
  $MONGODB_DATABASE
```

Switch to a database:

```bash
use {database}
# Ex: use graylog-db
```

Show databases:

```bash
show dbs
```

Show tables/collections:

```bash
show tables
show collections
```

Drop a collection:

```bash
db.{collection}.drop()
# Ex: db.ldap_settings.drop()
```

## Backup/Restore

### Openshift

Backup Database:

```bash
# Logging to cluster
oc login

# Switch project
oc project mongo-db

# Connection on mongodb pod
oc rsh mongodb-0

# Backup
mkdir /bitnami/mongodb/backup.d
mongodump \
  --host=localhost \
  --port=27017 \
  --db=$MONGODB_DATABASE \
  --username=$MONGODB_USERNAME \
  --password="$MONGODB_PASSWORD" \
  --gzip \
  --out=/bitnami/backup.d

# Exit du pod
exit

# Backup copy to local
oc cp mongodb-0:/bitnami/mongodb/backup.d .
```

Restore database:

```bash
# Copy backup on target pod
oc cp backup.d mongodb-0:/bitnami/

# Connection on mongodb pod
oc rsh mongodb-0

# Restore
mongorestore \
  --host=localhost \
  --port=27017 \
  --db=$MONGODB_DATABASE \
  --username=$MONGODB_USERNAME \
  --password="$MONGODB_PASSWORD" \
  --gzip \
  /bitnami/backup.d/$MONGODB_DATABASE
```

## Link

- https://www.mongodb.com/docs/manual/reference/mongo-shell/
---
tags: [Openshift, Kubernetes, Tools]
title: Helm
---

# Tools - Helm

## Commands

### Repo

List repos:

```bash
helm repo list
```

Add repo:

```bash
helm repo add {name} {path}
# Ex: helm repo add bitnami https://charts.bitnami.com/bitnami
```

List chart of a repo:

```bash
helm search repo {repo}
# Ex: helm search repo stable
```

List available version of a repo:

```bash
helm search repo {repo} --versions
# Ex: helm search repo litmuschaos/litmus --versions
```

### Chart

List all Chart (namespace):

```bash
helm list
```

Pull Chart:

```bash
helm pull {repo}/{chart} (--version {chart-version})
# Ex: helm pull stable/graylog
```

Delete Chart:

```bash
helm delete {chart}
```

Install release:

```bash
helm install {name} {repo} \
  -n|--namespace {namespace} \
  (-f|--values {value-file}) \
  (--version {chart-version})s
# Ex: helm install graylog-app stable/graylog \
#       --version 1.6.12 \
#       --namespace graylog \
#       --values graylog-values.yaml
```

Upgrade release:

```bash
helm upgrade {name} {repo} \
  -n|--namespace {namespace} \
  (-f|--values {value-file}) \
  (--version {chart-version})s
```

Get current values of a installed chart:

```bash
helm get values {name} (--all)
```

Get current manifests of a installed chart:

```bash
helm get manifest {chart}
```

Get values of a chart:

```bash
helm show values {repo}/{app}
# Ex: helm show values longhorn/longhorn
```

Download chart locally (fetch):

```bash
helm fetch {repo}/{app}
# Ex: helm fetch stable/graylog
```

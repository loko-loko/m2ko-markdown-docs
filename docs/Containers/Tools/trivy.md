---
tags: [Openshift, Kubernetes, Tools, Security]
title: Trivy
---

# Tools - Trivy

## What is Trivy

Trivy (tri pronounced like trigger, vy pronounced like envy) is a simple and comprehensive scanner
for vulnerabilities in container images, file systems, and Git repositories, as well as for configuration
issues. Trivy detects vulnerabilities of OS packages (Alpine, RHEL, CentOS, etc.) and language-specific
packages (Bundler, Composer, npm, yarn, etc.). In addition, Trivy scans Infrastructure as Code (IaC)
files such as Terraform, Dockerfile and Kubernetes, to detect potential configuration issues that expose
your deployments to the risk of attack. Trivy is easy to use. Just install the binary and you're ready
to scan.

- Github: https://github.com/aquasecurity/trivy

## Commands

Scan image's vulnerabilities:

```bash
trivy image (--severity={severity}) {image}
# Examples:
#  Only CRITICAL vulnerabilities:
#    trivy image --severity=CRITICAL docker.io/graylog:3.3
#
#  All vulnerabilities:
#    trivy image docker.io/graylog:3.3
```
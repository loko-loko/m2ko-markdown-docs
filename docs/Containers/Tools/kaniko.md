
---
tags: [Openshift, Kubernetes, Tools]
title: Kaniko
---

# Tools - Kaniko

## Description

**Kaniko** is a tool to build container images from a Dockerfile, inside a container or Kubernetes cluster.

## Run It

Run image:

```bash
docker run -it \
  --entrypoint sh \
  -v /path/to/project:/path/to/project \
  docker.io/gcr/kaniko-project/executor:v1.6.0-debug
```

Create docker credentials file:

```bash
echo '{"auths":{"docker.io":{"username":"lkone","password":"XXxXxXXX"}}}' > /kaniko/.docker/config.json
```

Build image:

```bash
context_path="/path/to/project"

/kaniko/executor \
  --context $context_path \
  --dockerfile $context_path/Dockerfile \
  --destination docker.io/my-image:v0
```

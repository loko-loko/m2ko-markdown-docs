---
tags: [Openshift, Kubernetes, Tools, Backup]
title: Kasten
---

# Tools - Kasten

## Commands

### Client

List all Restore Point Contents:

```bash
oc get restorepointcontents

# With state:
oc get restorepointcontent -o json | jq -rj '
  .items[]|
    .metadata.name," ",
    .status.state," ",
    .metadata.creationTimestamp,"\n"
' | column -s ' ' -t
```

Delete all restore point of 2021 year:

```bash
oc get restorepointcontent -o json | jq -rj '
  .items[]|
    .metadata.name," ",
    .status.state," ",
    .metadata.creationTimestamp,"\n"
' | awk '$3 ~ /2021-/ {printf "oc delete restorepointcontent %s # Date: %s\n", $1, $3}'
```

Get all restorepointcontent of a specific policy name:

```bash
oc get restorepointcontent  -l k10.kasten.io/policyName={policy-name}
# Ex: oc get restorepointcontent  -l k10.kasten.io/policyName=export-standard
```

### API

Get detail of an Kasten object:

```bash
oc get --raw /apis/{resource}.kio.kasten.io/v1alpha1/{object-type}/{object}/details
# Ex: oc get --raw /apis/actions.kio.kasten.io/v1alpha1/retireactions/retire-kjbm6/details
# Ex: oc get --raw /apis/apps.kio.kasten.io/v1alpha1/restorepointcontents/app-scheduled-96m7j/details
```

## Tools

### K10 Tools

Display application's backups status:

```bash
k10tools debug applications
```

Test DEX connection: 

```bash
k10tools debug auth
```

Display Catalog size:

```bash
k10tools debug catalog size
```

List backup actions:

```bash
k10tools debug backupactions
```

Get all components status (Pre checks):

```bash
k10tools primer
```
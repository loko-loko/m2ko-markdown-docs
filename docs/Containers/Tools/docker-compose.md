---
tags: [Docker, Tools, Deploy]
title: Docker Compose
---

# Tools - Docker Compose

## Install

Install with curl:

```bash
# Download bin
curl \
  -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \
  -o /usr/local/bin/docker-compose

# Change right
chmod +x /usr/local/bin/docker-compose
```

## Commands

Start compose in backgroud:

```bash
docker-compose up -d (--force-recreate)
```

Stop compose:

```bash
docker-compose rm -sf
```

---
tags: [System, Linux, Container]
title: Docker
---

# Container - Docker

## Install

Install with yum:

```bash
yum install docker
```

## Networks

Change default networks:

```bash
# /etc/docker/daemon.json
{
  "bip": "10.30.0.1/16",
  "fixed-cidr": "10.30.0.0/17",
  "default-address-pools": [
      {"base":"10.31.0.0/16", "size": 24} 
  ]
}
```

After modification of file:

```bash
systemctl restart docker
```

## Commands

### Base

List objects:

```bash
docker {object-type} ls
# With object-type:
#   - container
#   - image
#   - network
#   - volume
#   ...
```

Delete object:

```bash
docker {object-type} rm {object-name}
# Ex:
#   docker volume rm my-vol
#   docker image rm my-image:0.0
```

Detail of a object (`json`):

```bash
docker {object-type} inspect {object-name}
# Ex:
#   docker volume inspect my-vol
#   docker image inspect my-image:0.0
```

### Container

List containers:

```bash
docker ps (-a) (-q)
# With:
#   -a: All
#   -q: Only ID
```

Start/Stop/Restart container:

```bash
docker {action} {container}
# Ex:
#   docker stop my-container
#   docker restart my-container
```

Remove all containers:

```bash
docker rm $(docker ps -a -q)
```

### Advanced

Clean all unused docker objects:

```bash
docker system prune -af
```

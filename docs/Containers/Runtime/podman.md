---
tags: [System, Linux, Container]
title: Podman
---

# Container - Podman

## Commands

!!! note

    Lot of `podman` command are the same as `docker` commands.
    
    You can easily replace docker with `podman` in many cases.
    
    Examples:
    - `podman image ls`
    - `podman volume create {volume}`

List all pods:

```bash
podman pods
```

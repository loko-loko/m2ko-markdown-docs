---
tags: [Openshift, Object]
title: StateFulSet
---

# Openshift - StateFulSet

## Procedures

### Restart StateFulSet Pod(s)

```bash
# Get StateFulSet name
oc get sts

# Scale to 0 replica the StateFulSet
oc scale sts {sts-name} --replicas=0

# Check pod status (Terminating)
oc get pod | grep -i {sts-name}

# Scale to 1 replica the StateFulSet
oc scale sts {sts-name} --replicas=1
```


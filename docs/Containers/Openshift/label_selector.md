---
tags: [Openshift, Object]
title: Label/Selector
---

# Openshift - Label/Selector

## Commands

### Label

Display label:

```bash
oc get {resource} {resource-name} --show-labels
# Ex: oc get nodes worker1 --show-labels
```

Display only resource with specific label:

```bash
oc get {resource} -l {label}(={value})
# Ex: oc get nodes -l logging
#     oc get nodes -l logging=plomo
```

Display resource with a specific label:

```bash
oc get {resource} -L {label}
# Ex: oc get nodes -L logging
```

Add label:

```bash
oc label {resource} {resource-name} {key}={value} (--overwrite)
# Ex: oc label nodes worker1 logging=plomo --overwrite
```

Remove label:

```bash
oc label {resource} {resource-name} {key}-
# Ex: oc label nodes worker1 logging-
```

## Templates

### Selector

Select node with `nodeSelector`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-lako
spec:
  containers:
  - image: nginxinc/nginx-unprivileged
    name: nginx
  nodeSelector:
    kubernetes.io/hostname: kube-host-1
```
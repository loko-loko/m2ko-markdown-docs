---
tags: [Openshift]
title: Basics
---

# Openshift - Troubleshooting: Basics

## Commands

Get Cluster version:

```bash
oc get clusterversion
```

Get Cluster ID:

```bash
oc get clusterversion -o jsonpath='{.items[].spec.clusterID}{"\n"}'
```

Create dump file for RH Support:

```bash
oc adm must-gather
```

Start debug session on a node:

```bash
oc debug no/{node}
```

Display requests counts by API:

```bash
oc get apirequestcounts

# Only deprecated
oc get apirequestcounts -o jsonpath='{range .items[?(@.status.removedInRelease!="")]}{.status.requestCount}{"\t"}{.status.removedInRelease}{"\t"}{.metadata.name}{"\n"}{end}'
```

## Tips

Get pod count by node:

```bash
oc get pod -A -o wide | awk '!/^NAMESPACE/ && $8 != "<none>" {print $8}' | sort | uniq -c
```

Get all nodes performances (CPU/RAM):

```bash
for n in $(oc get node -o name | sed 's%node/%%g'); do oc adm top node $n; done
```

## Problems

- [SELinux relabel failed after upgrade to RHOCP 4.7](https://access.redhat.com/solutions/6015531)

## Link

- [How to get system:admin kubeconfigs on OCP 4.6+ ](https://access.redhat.com/solutions/6112601)
- [Trouble BudgetBurn Alert](https://runbooks.prometheus-operator.dev/runbooks/kubernetes/kubeapierrorbudgetburn/)
- [Rebuild Kubeconfig](https://rcarrata.com/openshift/regenerate-kubeconfig/)
---
tags: [Openshift]
title: Logs/Events
---

# Openshift - Logs and Events

## Commands

### Events

Display cluster events:

```bash
oc get events --sort-by='{.lastTimestamp}'
```

### Logs

Display pod logs:

```bash
oc logs {pod} (--tail {line}) (-f) (--timestamps)
# Examples:
#   Display pod logs with timestamp:
#     oc logs my-pod --timestamps
#   Display 100 last logs line of a pod:
#     oc logs my-pod --tail 100
```

### Support

Create must-gather:

```bash
oc adm must-gather
```

Create must-gather with network logs:

```bash
oc adm must-gather -- bash -c 'gather; gather_network_logs'
```

Create support file for a namespace:

```bash
oc adm inspect ns/{namespace}
# Ex: oc adm inspect ns/default
```

---
tags: [Openshift]
title: KubeApi
---

# Openshift - Troubleshooting: KubeApi

## Procedures

Extract `system:admin` kubeconfig:

```bash
oc -n openshift-kube-apiserver extract secret/node-kubeconfigs
```

## Scripts

Display kubeapi call by url:

```bash
masters=($(oc get nodes -l node-role.kubernetes.io/master= -o custom-columns=NAME:.metadata.name --no-headers))

for master in ${masters[@]}; do
  echo $master
  echo $(echo $master | tr '[a-z][A-Z][0-9]._' '-')
  oc logs kube-apiserver-${master} -n openshift-kube-apiserver | grep -oP 'https://\S+\.svc' | sort | uniq -c
  echo
done
```

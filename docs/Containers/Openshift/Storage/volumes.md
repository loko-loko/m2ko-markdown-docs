---
tags: [Openshift]
title: Volumes
---

# Openshift - Volumes

## Commands

### Storage Class

List storage class:

```bash
oc get sc
```

### Persistent Volume (PV)

List all PV:

```bash
oc get pv
```

PV Detail:

```bash
oc describe pv {pv_name}
oc get pvc {pv_name} -o yaml
```

Force PV deletion:

```bash
oc delete pv {pv-name} --grace-period=0 --force
oc patch pv {pv-name} -p '{"metadata": {"finalizers": null}}'
```

### Persistent Volume Claim (PVC)

List all PVC:

```bash
oc get pvc (--all-namespaces)
```

PVC Detail:

```bash
oc describe pvc {pvc_name}
oc get pvc {pvc_name} -o yaml
```

## Manifests

### Persistent Volume Claim (PVC)

See an example of PVC manifest:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: ceph-pvc-1
  namespace: my-ns
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
  storageClassName: sc-ceph-rbd
```

## Procedures

### Resize PVC

Edit PVC:

```bash
oc edit pvc data-myapp
```

Edit `spec/resources/requests/storage` with new value:

```yaml
spec:
  resources:
    requests:
      storage: 3Gi
```

Check new size from pvc attribute:

```bash
oc get pvc data-myapp -o yaml
```

!!! info

    For OCP 3.*, you must restart the pod to take into account new size.

Check FS size from pod:

```bash
oc exec myapp -- df -h
```

## Tips

List all CSI Ceph volume with `jq`:

```bash
oc get pv -o json | jq '
  .items[]
    | select(.spec.csi.driver == "openshift-storage.rbd.csi.ceph.com")
    | .spec.claimRef.namespace
        + ";" + .spec.claimRef.name
        + ";" + .spec.csi.volumeAttributes.imageName
' | tr -d '"' | column -t -s';'
```

## Links

- [k8s.io: Persistent Volumes Concept](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)

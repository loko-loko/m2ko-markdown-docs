---
tags: [Openshift, Network]
title: Ingress
---

# Openshift - Ingress

## Commands

List ingress controllers:

```bash
oc get ingresscontrollers
```

## Routes

### Description

List of Termination:

|**Name** |**Description**
|--- |---
|`edge` |Requete déchiffré par le routeur Ingress (possibilité de surchargement)
|`passthrough` |Requete envoyé en l'état au service/pod
|`reencrypt` |Requete déchiffré par le routeur Ingress puis ré-enchiffré jusqu'au service/pod

## Links

- https://docs.openshift.com/enterprise/3.0/architecture/core_concepts/routes.html#secured-routes
- https://www.venafi.com/fr/blog/what-ssltls-x509-certificate

---
tags: [Openshift, Network]
title: Egress
---

# Openshift - Egress

## Commands

Get egressips:

```bash
# SDN
oc get netnamespaces

# OVN
oc get egressips
```

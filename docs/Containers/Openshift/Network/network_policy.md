---
tags: [Openshift, Network]
title: Network Policy
---

# Openshift - Network Policy

## Commands

Get netpol:

```bash
oc get netpol
oc get networkpolicy
```

## Manifest

Examples of manifest, to only accept connections from Pods within a
project and for some port with project which have `prometheus: "yes"` label:

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-from-prometheus
  namespace: my-namespace
spec:
  podSelector: {}
  ingress:
  - from:
    - podSelector: {}
  - from:
    - namespaceSelector:
        matchLabels:
          prometheus: "yes"
    ports:
    - port: 8080
      protocol: TCP
    - port: 9114
      protocol: TCP
  policyTypes:
  - Ingress
  egress: []
```


## Links

- https://access.redhat.com/documentation/en-us/openshift_container_platform/4.1/html/networking/configuring-networkpolicy

---
tags: [Openshift, Network]
title: OVN Network
---

# Openshift - OVN Network

## Procedures

### Run OVN commands from ovn-northd pod

Execute bash to a OVN master:

```bash
OVN_NS=openshift-ovn-kubernetes
OVN_MASTER=$(oc get po -n $OVN_NS -o name | grep 'ovnkube-master' | tail -1)

oc exec -n $OVN_NS -it $OVN_MASTER -- /bin/bash
```

Get ovn-northd process for build ovn args:

```bash
OVN_NDB=$(ps -eafww | grep -oP 'ssl:[\w:\.,]+:9641')
OVN_ARGS=(
    "--db=${OVN_NDB}"
    "--private-key=/ovn-cert/tls.key"
    "--certificate=/ovn-cert/tls.crt"
    "--ca-cert=/ovn-ca/ca-bundle.crt"
)
```

Run commands:

```bash
ovn-nbctl ${OVN_ARGS[@]} {command}
# Ex: ovn-nbctl ${OVN_ARGS[@]} lr-list
```

### Check memory of OVN Controller pods

Script to check memory of OVN Controller pods:

```bash
mem_limit=2000
namespace=openshift-ovn-kubernetes

# Get pods metrics
pods=($(oc adm top pod -n $namespace --containers | grep -P "ovnkube-node-.*ovn-controller" | sed -r 's/\s+/;/g'))

for pod in ${pods[@]}; do
    pod_name=$(awk -F';' '{print $1}' <<< $pod)
    container=$(awk -F';' '{print $2}' <<< $pod)
    mem_usage=$(awk -F';' '{print $4}' <<< $pod | tr -d 'Mi')

    if (( mem_usage > mem_limit )); then
        echo "${pod_name}: ${container} -> Memory usage > ${mem_limit}Mi (${mem_usage}Mi). OVN-Controller: KO"
    else
        echo "${pod_name}: ${container} -> Memory usage < ${mem_limit}Mi (${mem_usage}Mi). OVN-Controller: OK"
    fi
done
```

### Get OVN version

```bash
oc exec -n openshift-ovn-kubernetes ovnkube-master-* -c northd -- ovn-controller --version
```

### Increase/Decrease verbosity of OVN logs ([redhat-solution](https://access.redhat.com/solutions/5726161)):

```bash
# Increase verbosity (debug)
oc -n openshift-ovn-kubernetes exec -t ovs-node-* -- ovs-appctl vlog/set dbg
oc -n openshift-ovn-kubernetes exec -t ovnkube-node-* -- ovn-appctl -t ovn-controller vlog/set dbg
oc -n openshift-ovn-kubernetes exec -t ovnkube-node-* -c northd -- ovn-appctl -t ovn-northd vlog/set dbg

# Decrease verbosity (info)
oc -n openshift-ovn-kubernetes exec -t ovs-node-* -- ovs-appctl vlog/set info
oc -n openshift-ovn-kubernetes exec -t ovnkube-node-* -- ovn-appctl -t ovn-controller vlog/set info
oc -n openshift-ovn-kubernetes exec -t ovnkube-node-* -c northd -- ovn-appctl -t ovn-northd vlog/set info
```

### Get OVN DB status

```bash
OVN_NS=openshift-ovn-kubernetes
OVN_MASTER=$(oc get po -n $OVN_NS -o name | grep 'ovnkube-master' | tail -1)

# Get OVN North Bound DB state:
oc exec -n $OVN_NS -it $OVN_MASTER -c nbdb -- ovs-appctl --target=/var/run/ovn/ovnnb_db.ctl cluster/status OVN_Northbound

# Get OVN South Bound DB state:
oc exec -n $OVN_NS -it $OVN_MASTER -c sbdb -- ovs-appctl --target=/var/run/ovn/ovnsb_db.ctl cluster/status OVN_Southbound
```

## Commands

### OVN

Show topology:

```bash
ovn-nbctl ${OVN_ARGS[@]} show
```

List router:

```bash
ovn-nbctl ${OVN_ARGS[@]} lr-list
```

List router's nat:

```bash
ovn-nbctl ${OVN_ARGS[@]} lr-nat-list {router}
# Ex: ovn-nbctl ${OVN_ARGS[@]} lr-nat-list GR_server1.net
```

List Load Balancer:

```bash
ovn-nbctl ${OVN_ARGS[@]} lb-list
```

### OVS

Show topology:

```bash
ovs-vsctl show
```

Get OVS infos:

```bash
ovs-vsctl list open_vswitch
```

Detail of an interface:

```bash
ovs-vsctl list interface {interface}
# Ex: ovs-vsctl list interface ovn-k8s-mp0
```

Troubleshoot OVSDB content:

```bash
POD=$(oc -n openshift-ovn-kubernetes get pod -o custom-columns=POD:.metadata.name --no-headers --selector='app==ovnkube-master' | head -n1)
oc -n openshift-ovn-kubernetes exec $POD -c ovnkube-master -it -- ovsdb-client --private-key=/ovn-cert/tls.key --certificate=/ovn-cert/tls.crt --ca-cert=/ovn-ca/ca-bundle.crt -f csv --no-headings dump ssl:localhost:9641 OVN_Northbound NAT | grep "name=$EGRESS_NAME" | tr -d '"' | cut -d ',' -f5,9 | sort -u
```

## Links

- https://man7.org/linux/man-pages/man7/ovn-architecture.7.html
- https://docs.openvswitch.org/_/downloads/en/latest/pdf/
- https://docs.ovn.org/_/downloads/en/latest/pdf/
- https://blog.scottlowe.org/2016/12/09/using-ovn-with-kvm-libvirt/

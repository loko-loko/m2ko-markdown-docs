---
tags: [Openshift]
title: Machine Configs
---

# Openshift - Machine Config (MC)

## Commands

Display Machine Config (MC):

```bash
oc get mc
```

Display Machine Config Pool (MCP):

```bash
oc get mcp
```

Display logs of MC controller:

```bash
oc logs deploy/machine-config-controller -n openshift-machine-config-operator -f
```

## Scripts

List apply and desired MC on all nodes:

```bash
oc get no -o json |jq -jr '.items[].metadata|
  .name,",",
  .annotations."machineconfiguration.openshift.io/currentConfig",",",
  .annotations."machineconfiguration.openshift.io/desiredConfig","\n"' | column -t -s ','
```

Compare 2 machine configs:

```bash
current_mc=rendered-physical-worker-b34ff59164845aa23f079a88c230f778
previous_mc=rendered-physical-worker-19e28b7b1bdfbebbd40c2af640e6ee6f

sdiff -s <(oc get mc $current_mc -o yaml) <(oc get mc $previous_mc -o yaml)
```
---
tags: [Openshift, Upgrade]
title: Upgrade
---

# Openshift - Upgrade

## Commands

Change upgrade channel:

```bash
oc patch clusterversion version --type json -p '
  [{"op": "add", "path": "/spec/channel", "value": "stable-4.8"}]
'
```

Display available upgrade:

```bash
oc adm upgrade
```

Upgrade cluster:

```bash
# To a specific version
oc adm upgrade --to={version}

# To lastest release
oc adm upgrade --to-latest=true 

# Ex: oc adm upgrade --to=4.8.34
```

Get current channel upgrade:

```bash
oc get clusterversion -o json|jq ".items[0].spec"
```

Get cluster version:

```bash
oc get clusterversion
```

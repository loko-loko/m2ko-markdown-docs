---
tags: [Openshift, Security, Certificate]
title: Certificate
---

# Openshift - Certificate

## Commands

List certificates:

```bash
oc get csr
```

Approve certificate:

```bash
oc adm certificate approve {cert}
```

Approve all Pending certs:

```bash
oc get csr | awk '/Pending/ {printf "oc adm certificate approve %s\n", $1}' | sh
```
## Check Non-Automated Certificates expiration

### Ingress Certificates

Check certificate
```bash
oc get ingresscontrollers.operator.openshift.io -n openshift-ingress-operator --no-headers -o json |jq -r '.items[].spec.defaultCertificate.name' |while read CERT ; do echo "----  $CERT  ------------" ; openssl x509 -enddate -noout -in <(oc get secret -n openshift-ingress $CERT -o json |jq -r '.data."tls.crt"' |base64 --decode) ;done

----  ingress-cert-default-2023  ------------
notAfter=Feb 19 23:59:59 2023 GMT
----  ingress-cert-ALPHA-2023  ------------
notAfter=Jul 18 23:59:59 2023 GMT
----  ingress-cert-BETA-2023  ------------
notAfter=Jul 18 23:59:59 2023 GMT
----  ingress-cert-GAMMA-2023  ------------
notAfter=Jul 18 23:59:59 2023 GMT
```

patch ingresscontrollers under openshift-ingress-operator to change certificates


### API Certificates

> Do Never change certificate on api-int ( internal) . It is automatically rotated by Openshift
> If you do this cluster will need a recovery procedure


Check expiration
```bash
openssl x509 -enddate -noout -in <(oc get secret -n openshift-config api-cert-2023 -o json |jq -r '.data."tls.crt"' |base64 --decode)
notAfter=Feb 20 23:59:59 2023 GMT
```

Change this certificate
```bash
oc create secret tls api-cert-NEW --key=tls.key --cert=tls.crt -n openshift-config
oc patch apiserver cluster \
     --type=merge -p \
     '{"spec":{"servingCerts": {"namedCertificates":
     [{"names": ["<FQDN>"], 
     "servingCertificate": {"name": "<secret>"}}]}}}'

     # replace FQDN and secret by accurate values
```

Verify rollout of kube-apiserver Pods

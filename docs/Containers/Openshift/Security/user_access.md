---
tags: [Openshift, Security]
title: Users/Groups and Access
---

# Openshift - Users/Groups and Access

## Commands

### Login

Connect with admin user:

```bash
oc login -u system:admin
```

Switch to `default` project:

```bash
oc project default
```

Connect with LDAP:

```bash
oc login https://api-ocp.m2ko.net:443 -u {user}
```

Display current user:

```bash
oc whoami
```

### Users

Add cluster role to user:

```bash
oc adm policy add-cluster-role-to-user {cluster-role} {user}
# Ex: oc adm policy add-cluster-role-to-user cluster-monitoring-view lkone
```

Add role to user:

```bash
oc adm policy add-role-to-user {role} {user} -n {namespace}
# Ex: oc adm policy add-role-to-user admin lkone -n prom
```

List cluster admin users:

```bash
oc get clusterroleBindings --output json | jq -r '.items[] | select(.roleRef.name=="cluster-admin")|.subjects[]|select(.kind=="User")|.name'
```

Create LDAP identity:

```bash
oc create identity ldap:CN=lako,OU=PERSONNES,O=MYGROUP
```

List user/identity:

```bash
user=laurent.kone

oc get user $user
oc get identity | grep -w $user
```

### Service Account 

Create service account:

```bash
oc create sa {service-account}
# Ex: oc create sa test
```

Retrieve token from a service account:

```bash
oc get secret {sa-secret} -o jsonpath='{.data.token}' | base64 -d
# Ex: oc get secret test-token-4l2xq -o jsonpath='{.data.token}' | base64 -d
```

Logging with service account:

```bash
oc login https://api-ocp.m2ko.net:6443 --token={token}
```

---
tags: [Openshift, Security, RBAC]
title: Roles
---

# Openshift - Roles

## Commands

### Roles

Add admin right to user for a namespace:

```bash
oc policy add-role-to-user admin {user} -n {namespace}
# Ex: oc policy add-role-to-user admin laurent.kone -n my-ns-1
```

Add admin right to group for a namespace:

```bash
oc policy add-role-to-group admin {group} -n {namespace}
# Ex: oc policy add-role-to-group admin USER_MDW -n my-ns-1
```

List role of a namespace:

```bash
oc describe rolebinding -n {namespace}
# Ex: oc describe rolebinding.rbac -n my-ns-1
```

Add SCC to Service Account:

```bash
oc adm policy add-scc-to-user {scc} -z {sa} (-n {namespace})
# Ex: oc adm policy add-scc-to-user anyuid -z my-sa -n my-ns
```

## Procedures

**Add cluster-admin right to a user**:

Once user is logged on cluster (LDAP):

```bash
user=laurent.kone

# Logging to cluster admin
oc login -u system:admin

# List users
oc get users | grep -w $user

# Add cluster-admin right to user:
oc adm policy add-cluster-role-to-user cluster-admin $user
```

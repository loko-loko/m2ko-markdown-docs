---
tags: [Openshift, Security, SCC]
title: SCC
---

# Openshift - SCC

## Commands

Get users who used `anyuid` scc:

```bash
oc get scc anyuid -o json | jq '.users'
```

Get user who can add `anyuid` scc to user:

```bash
oc adm policy who-can add-scc-to-user anyuid
```
---
tags: [Kubernetes, Plugins]
title: Plugins
---

# Kube - Plugins

## Krew

### Install

Installation of krew:

```bash
KREW_VERSION=0.4.3

# Download package
curl -sOL https://github.com/kubernetes-sigs/krew/releases/download/v${KREW_VERSION}/krew-linux_amd64.tar.gz

# Unarchive
tar xzf krew-linux_amd64.tar.gz

# Setup
./krew-linux_amd64 install krew

# Move in /usr/local/bin
mv ~/.krew/store/krew/v${KREW_VERSION}/krew /usr/local/bin/kubectl-krew

# Clean files
rm -rf krew* .krew

# Add krew local path on bashrc (or zshrc)
echo "export PATH=$HOME/.krew:$PATH" >> ~/.bashrc
```

### Usage

Update plugins list:

```bash
kubectl krew update
```

List available plugins:

```bash
kubectl krew search
```

Install plugins:

```bash
kubectl krew install {plugin}
# Ex: kubectl krew install sniff
```

List installed plugins:

```bash
kubectl krew list
```

## Links

- https://krew.sigs.k8s.io/plugins/
- https://github.com/eldadru/ksniff

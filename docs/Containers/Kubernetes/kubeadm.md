---
tags: [Kubernetes, Build]
title: KubeAdm
---

# Kubernetes - Kubeadm

## Configs

Get admin kube config file from a master:

```bash
kubectl --kubeconfig=/etc/kubernetes/admin.conf get node
```

## Certificates

Check certs expiration dates:

```bash
kubeadm alpha certs check-expiration
```

Renew a certificate:

```bash
kubeadm alpha certs renew {cert}
# Ex: kubeadm alpha certs renew apiserver-etcd-client
```

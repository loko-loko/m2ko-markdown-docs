---
tags: [Kubernetes, Tips]
title: Tips
---

# Kube - Tips

## Commands

Scale daemonset to zero:

```bash
# Unschedule all ds pods:
oc patch daemonset fluentd \
  -p='{"spec": {"template": {"spec": {"nodeSelector": {"non-existing": "true"}}}}}'

# Reschedule all ds pods:
oc patch daemonset fluentd \
  --type json \
  -p='[{"op": "remove", "path": "/spec/template/spec/nodeSelector/non-existing"}]'
```

## Utils

Copy image from a repository to an other: https://github.com/containers/skopeo

## Link

- https://github.com/kelseyhightower/kubernetes-the-hard-way


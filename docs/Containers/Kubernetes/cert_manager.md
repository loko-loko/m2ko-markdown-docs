---
tags: [Security, Kubernetes]
title: Cert-Manager
---

# Kube - Cert-Manager

## Install

```bash
# Create namespace
kubectl create namespace cert-manager

# Add/update Helm repo
helm repo add jetstack https://charts.jetstack.io
helm repo update

# Install chart
helm install cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version v1.1.1 \
  --set installCRDs=true

# Checks
kubectl get pods --namespace cert-manager
```

## Configuration

### Object's Templates

**Cluster Issuer**:

```yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-cluster-issuer
  namespace: cert-manager
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: laurent.alou.k@gmail.com
    privateKeySecretRef:
      name: letsencrypt-cluster-issuer-account-key
    solvers:
    - http01:
        ingress:
          class: nginx
```

**Ingress**:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt-cluster-issuer
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
  name: ingress-blog.m2ko.net
  namespace: default
spec:
  tls:
  - hosts:
    - blog.m2ko.net
    secretName: blog.m2ko.net-tls
  rules:
  - host: blog.m2ko.net
    http:
      paths:
      - backend:
          serviceName: hello-app
          servicePort: 8080
        path: /
        pathType: ImplementationSpecific
```

**Certificate**:

```yaml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: blog.m2ko.net
  namespace: default
spec:
  secretName: blog.m2ko.net-tls
  dnsNames:
  - blog.m2ko.net
  issuerRef:
    name: letsencrypt-cluster-issuer
    kind: ClusterIssuer
```

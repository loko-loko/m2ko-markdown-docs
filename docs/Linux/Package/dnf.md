---
tags: [System, Linux, RedHat, Package]
title: DNF
---

# Linux - DNF

## Commands

Install package:

```bash
dnf install {package}
```

## Tips

### Lock Version

Add following content from `/etc/dns/plugins/versionlock.conf`:

```ini
[main]
enabled = 1
locklist = /etc/dnf/plugins/versionlock.list
```
Add packages to lock from `/etc/dns/plugins/versionlock.list`:

```bash
chromium-0:105.0.5195.125-2.el8.*
chromium-common-0:105.0.5195.125-2.el8.*
```

---
tags: [System, Linux, RedHat, Package]
title: Yum
---

# Linux - Yum

## Commands

### Repos

Display available repo:

```bash
yum repolist
yum repolist enabled
```

Display installed package:

```bash
yum list installed
```

Clean cache:

```bash
yum clean all
rm -rf /var/cache/yum
```

Recreate cache:

```bash
yum makecache fast
```

Display duplicate packages:

```bash
yum --showduplicates list kernel
```

Find package with a binaries:

```bash
yum whatprovides {bin}
# Ex: yum whatprovides netstat (net-tools-*)
```

Yum history:

```bash
# Display history
yum history

# Detail of an history-id
yum history info {history-id}
# Ex: yum history info 22
```

### Security

Minimal update security and bugfix:

```bash
yum update-minimal --security --bugfix
```

## Procedures

Install PHP (https://www.howtoforge.com/tutorial/centos-lamp-server-apache-mysql-php/):

```bash
# With RPM:
rpm -Uvh http://rpms.remirepo.net/enterprise/remi-release-7.rpm

# With Yum:
yum-config-manager --enable remi-php71
yum install php php-opcache

# Check version of php:
php -v
````

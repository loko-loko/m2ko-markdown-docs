---
tags: [System, Linux, Package]
title: RPM
---

# Linux - RPM

## Commands

Package list:

```bash
rpm -qa
```

List package of a file:

```bash
rpm -qf {file}
# Ex: rpm -qf /etc/selinux/config
```

List package's files:

```bash
rpm -V {package}
```

Pretty display package:

```bash
rpm -qa --qf '%{INSTALLTIME} (%{INSTALLTIME:date}): %{NAME}-%{VERSION}-%{RELEASE}.%{ARCH}\n' | sort -n
```

Install package:

```bash
rpm -i {package}
```

Upgrade package:

```bash
rpm -U {package}
```

Install/Upgrade package:

```bash
rpm -Uvh {package}
# -U: Upgrade
# -v: Verbose
# -h: Add hash
```

Remove package:

```bash
rpm -ev {package}
```
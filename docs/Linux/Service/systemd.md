---
tags: [System, Linux, Service]
title: Systemd
---

# Linux - Systemd

## Commands

### Services

Services status:

```bash
systemctl list-unit-files --type=service
systemctl list-unit-files --type=service --state=enabled
```

Mask service (Lock):

```bash
systemctl mask {service} --now
```

Show environment vars:

```bash
systemctl show-environment
```

Set environment var:

```bash
systemctl set-environment {key}={value}
# Ex: systemctl set-environment no_proxy=localhost
```

### Targets


Get default target:

```bash
systemctl get-default
```

Set default target:

```bash
systemctl set-default {target}
# Ex: systemctl set-default graphical.target
#      -> Create symlink: /etc/systemd/system/default.target
```

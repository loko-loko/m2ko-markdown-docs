---
tags: [System, Linux]
title: Process/Service
---

# Linux - Process/Service

## Commands

### Processus

List process consumption (cpu/ram):

```bash
ps ux    # User courant
ps aux   # Tous les users
```

List number of process by user:

```bash
ps h -Led -o user | sort | uniq -c | sort -rn
```

List open file from a path:

```bash
lsof {path}
```

List open file from a process:

```bash
lsof -p {process}
```

Get open file of a device:

```bash
lsof | grep -w "{major},{minor}"
# Ex: lsof | grep -w "253,1"
```

Display process associate to a mount point:

```bash
fuser -um {mount_point}
```

Kill process associate to a mount point:

```bash
fuser -ku {mount_point}
```

!!! warning

    Don't execute command of umounted mount point,
    otherwise it may kill all the processes in `/`.

### Services

Services status:

```bash
# RHEL 6
service --status-all

# RHEL 7+
systemctl list-unit-files --type=service
systemctl list-unit-files --type=service --state=enabled
```

## Procedures

### Create new service from /etc/init.d

Example of service `test`:

```bash
#!/bin/bash
#
# Test
#
# chkconfig: 345 80 30
# description: test
# processname: test

RETVAL=0
prog="sleep"
LOCKFILE=/var/lock/subsys/$prog

start() {
        echo -n "Starting $prog: "
        echo start test $(date) >> /root/rc.local.out
        sleep 10000 &
        RETVAL=$?
        [ $RETVAL -eq 0 ] && touch $LOCKFILE
        echo
        return $RETVAL
}

stop() {
        echo -n "Shutting down $prog: "
        echo stop test $(date) >> /root/rc.local.out
        ps -eaf | awk '/sleep 10000/ && ! /grep/ {print $2}' | xargs kill -9
        RETVAL=$?
        [ $RETVAL -eq 0 ] && rm -f $LOCKFILE
        echo
        return $RETVAL
}

status() {
        echo -n "Checking $prog pid : "
        ps -eaf | awk '/sleep 10000/ && ! /grep/ {print $2}'
        RETVAL=$?
        return $RETVAL
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status
        ;;
    restart)
        stop
        start
        ;;
    *)
        echo "Usage: $prog {start|stop|status|restart}"
        exit 1
        ;;
esac
exit $RETVAL
```

Add and list service:

```bash
chkconfig --add test
chkconfig --list test
```

Check status with `systemctl`:

```
systemctl status test
```

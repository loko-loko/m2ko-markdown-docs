---
tags: [System, Linux, Security]
title: SELinux
---

# Linux - SELinux

## Commands

### Globals

Display SELinux status:

```bash
sestatus
```

To see the current status of SELinux, run the following command:

```bash
getenforce
# Ex: Enforcing
```

The current SELinux status can also be changed with the following command:

```bash
setenforce <status>
# Ex: setenforce enforcing
#     setenforce permissive
```

Configuration file:

```bash
/etc/selinux/config
```

At any point in time, you can generate a report from your SELinux audit logs. This report will contain all information regarding any potential event that has been blocked by SELinux and also how you can allow the blocked event(s) if needed:

```bash
sealert –a /var/log/audit/audit.log
```

### Permission

The permissions of a standard file/directory can be viewed by using the following command:

```bash
ls -Z <path>
```

To change the context, use the `chcon` command. To make the changes recursively use with the `-R` switch:

```bash
chcon (-R) -u <user> -t <context> <path>
# Ex: chcon -R -u user_u -t public_content_rw_t /ftp
```

Get SELinux boolean value(s):

```bash
getsebool –a
```

setsebool is used to toggle policy booleans on or off:

```bash
setsebool <value>=on|off
# Ex: setsebool httpd_can_network_connect=on
```

!!! note

    If you want the Boolean values to be persistant, use the `-P` option along with setsebool command. The `–P` option will make pending values be written to the policy file on disk. 

## Policy

The policy modules of SELinux can be viewed by running the following command:

```bash
semodule –l
```

## Link

- https://searchdatacenter.techtarget.com/tip/SELinux-tutorial-Commands-and-management

---
tags: [System, Linux, Security]
title: Password
---

# Linux - Password

## Commands

Display password policy for an user:

```bash
chage -l {user}
# Ex: chage -l lkone
```

## /etc/shadow

### How it work

Basically, the `/etc/shadow` file stores secure user account information.
All fields are separated by a colon (`:`) symbol. It contains one entry
per line for each user listed in `/etc/passwd` file. 

```bash
lkone:$1$fnnfcFgTiHGOfff#5:13064:0:99999:7:::
#---- -------------------- ----- - ----- -
# 1             2            3   4   5   6
```

1. **Username**
2. **Encrypted Password** (`${id}${salt}${hashed}`). With `id`:
   - `$1$`: md5
   - `$2a$`: Blowfish
   - `$2y$`: Blowfish
   - `$5$`: sha-256
   - `$6$`: sha-512
3. **Last password change**
4. **Minimum**: The minimum number of days required between password changes.
5. **Maximum**: The maximum number of days the password is valid.
6. **Warn**: The number of days before password is to expire that user is warned.
7. **Inactive**: The number of days after password expires that account is disabled.
8. **Expire**: days since Jan 1, 1970 that account is disabled.

### Verify Integrity

You can use `pwck` command verifies the integrity of the users and authentication information:

```bash
# Shadow file:
pwck -r /etc/shadow

# Passwd file:
pwck -r /etc/passwd

# Both
pwck -r /etc/passwd /etc/shadow
```

### Generate Password 

With `openssl` command:

```bash
openssl passwd -6 -salt xyz {password}
# With:
#  -1: MD5
#  -5: SHA256
#  -6: SHA512 (Recommanded)
```

With `mkpasswd` command:

```bash
mkpasswd --method={method} --stdin
# With method:
#  md5
#  sha-256
#  sha-512
```

With `chpasswd` command (Update existing password):

```bash
echo "{user}:{password}" | chpasswd
```

## Random Password

With `dd` command:

```bash
length=32
base64 /dev/urandom | tr -d "/+" | dd bs=$length count=1 status=none | xargs echo;
```

With `openssl` command:

```bash
length=32
openssl rand -base64 $length
```

## Links

- https://www.cyberciti.biz/faq/understanding-etcshadow-file/
---
tags: [System, Linux, Security]
title: OpenScap
---

# OpenScap

## Commands

List profile:

```bash
oscap info \
  --profiles /usr/share/xml/scap/ssg/content/ssg-rhel8-xccdf.xml
```

Start scan with CIS profile (Default: `lvl2`):

```bash
oscap xccdf eval \
  --profile cis \
  --report report.html \
  /usr/share/xml/scap/ssg/content/ssg-rhel8-xccdf.xml
```

List files of scap-security-guide:
```bash
rpm -ql scap-security-guide
```

---
tags: [System, Linux]
title: SSH
---

# Linux - SSH

## SSH Keygen

`SSH-Keygen` is a tool for creating new authentication key pairs for SSH. Such key pairs are used for automating logins, single sign-on, and for authenticating hosts.

SSH supports several public key algorithms for authentication keys. These include:

- `rsa`: an old algorithm based on the difficulty of factoring large numbers. A key size of at least 2048 bits is recommended for RSA; 4096 bits is better. RSA is getting old and significant advances are being made in factoring. Choosing a different algorithm may be advisable. It is quite possible the RSA algorithm will become practically breakable in the foreseeable future. All SSH clients support this algorithm.
- `dsa`: an old US government Digital Signature Algorithm. It is based on the difficulty of computing discrete logarithms. A key size of 1024 would normally be used with it. DSA in its original form is no longer recommended.
- `ecdsa`: a new Digital Signature Algorithm standarized by the US government, using elliptic curves. This is probably a good algorithm for current applications. Only three key sizes are supported: 256, 384, and 521 (sic!) bits. We would recommend always using it with 521 bits, since the keys are still small and probably more secure than the smaller keys (even though they should be safe as well). Most SSH clients now support this algorithm.
- `ed25519`: this is a new algorithm added in OpenSSH. Support for it in clients is not yet universal. Thus its use in general purpose applications may not yet be advisable.


### Commands

Create public/private key:

```bash
ssh-keygen -N "" -t rsa -f ~/.ssh/id_rsa
# -N: passphrase
# -t: type (rsa|dsa,...)
# -f: file
```

Display public key info:

```bash
ssh-keygen -l -f {public-key}
```

Display public key from the private:

```bash
ssh-keygen -y -f {private-key}
```

## Client (ssh)

### Options

You can use some options with SSH.

See some usefull options:
- `StrictHostKeyChecking`
- `BatchMode`
- `ConnectTimeout`

Usage:

```bash
ssh -n -q \
  -o "StrictHostKeyChecking=no" \
  -o "BatchMode=yes" \
  -o "ConnectTimeout=1"
  {host}
```

### Config file

You can configure a ssh config file with some options.

See an example of a config file with some options by host:

```bash
# ~/.ssh/config
Host bastion
User lkone
IdentityFile ~/.ssh/bastion.key
StrictHostKeyChecking no

Host gitssh.m2ko.com
User loko.loko
IdentityFile ~/.ssh/gitlab
```

## SSH Daemon (sshd)

Allow only some users:

```ini
# /etc/ssh/sshd_config
AllowUsers root
```

Authorized password authentification:

```bash
# /etc/ssh/sshd_config
PasswordAuthentication yes

# With sed
sed -i 's/^PasswordAuthentication\ no/PasswordAuthentication\ yes/' /etc/ssh/sshd_config
```

## Link

- https://www.ssh.com/academy/ssh/keygen
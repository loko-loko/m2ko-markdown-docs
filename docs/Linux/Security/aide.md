---
tags: [System, Linux, Security]
title: Aide
---

# Linux - Aide

## Setup

Install with yum:

```bash
yum install aide
```

Init database:

```bash
sudo aide --init
```

Rename database:

```bash
sudo mv /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz
```

## Commands

Check files:

```bash
sudo aide --check
```

Update database:

```bash
sudo aide --check
```


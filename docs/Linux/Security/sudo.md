---
tags: [System, Linux, Security]
title: Sudo
---

# Linux - Sudo

## Commands

Run command with another user:

```bash
sudo -E -u {user} {command}
# Ex: sudo -E -u oracle /path/to/script.sh
```

Check conformity of all files from `sudoers`:

```bash
visudo -c
```

Check conformity of `sudoers` file:

```bash
visudo -cf /etc/sudoers.d/etude
```

## Sudoers

Grant sudo access to a group:

```bash
# /etc/sudoers.d/dba
%dba        ALL=(ALL)       NOPASSWD: ALL
```

Grant sudo access to `systemctl` command to `apache` user:

```ini
# /etc/sudoers.d/apache_pj3xx1
User_Alias APACHE = apache
Cmnd_Alias CMD_APACHE = /usr/bin/systemctl * apache_svc*
APACHE ALL=(root) NOPASSWD: CMD_APACHE
```

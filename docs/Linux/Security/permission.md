---
tags: [System, Linux, Security]
title: Permission
---

# Linux - Permission

## Commands

Display permission detail of a file:

```bash
getfacl {file}
# Or
stat {file}
```

Backup permissions of a repository:

```bash
getfacl -R {path} > {backup-file}
# Ex: getfacl -R /home/lako } /tmp/backup-lako.permission
```

Restore permissions of a repository:

```bash
setfacl --restore=/tmp/backup-permissions.txt
```

Grant additional access to a user:

```bash
setfacl (-R) -m {type}:{name}:{right} {file}
# Ex: setfacl -R -m u:loko:rx /data
#     setfacl -m g:grp:rwx /data/file.txt
```

## SUID/SGID/Sticky Bits

### SUID Bit (4)

When the SUID bit is set on an executable file, this means that the file will be executed with the same permissions as the `owner` of the executable file.

Add SUID Bit:

```bash
chmod u+s {file/directory}
# Or
chmod 4775 {file/directory}
```

### SGID Bit (2)

SGID is similar to SUID. With the SGID bit set, any user executing the file will have same permissions as the `group` owner of the file.

Add SGID Bit:

```bash
chmod g+s {file/directory}
# Or
chmod 2775 {file/directory}
```

### Sticky Bit (1)

The sticky bit works on the directory. With sticky bit set on a directory, all the files in the directory can only be deleted or renamed by the file owners only or the root.

Add Sticky Bit:

```bash
chmod o+t {directory}
# or
chmod 1775 {directory}
```

Remove Sticky Bit:

```bash
chmod -t {directory}
# Or
chmod 0777 {directory}
```

### Errors

In case of file/directory have no executable write the Bit will be in Upper case:

```bash
$ ls -l plop/test.sh
# -rwSrw-r-- 1 user group 0 Dec 29 17:31 plop/test.sh
```

Change the right to fix this problem:

```bash
$ chmod 4766 plop/test.sh
$ ls -l plop/test.sh
# -rwsrw-r-- 1 user group 0 Dec 29 17:31 plop/test.sh
```

### Examples

Set the Sticky Bit on `/tmp` folder:

```bash
chmod o+t /tmp
# Or
chmod 1777 /tmp
```

Add SUID and SGID Bits on a file:

```bash
chmod u+s /path/to/file
chmod g+s /path/to/file
# Or
chmod 6777 /path/to/file
```

## Extended Permission

Display extended attribute:

```bash
lsattr {file}
```

Set/Unset file to immutable:

```bash
chattr +i {file}
chattr -i {file}
```

## Link

- https://linuxhandbook.com/suid-sgid-sticky-bit/

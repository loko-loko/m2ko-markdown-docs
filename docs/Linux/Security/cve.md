---
tags: [System, Linux, Security]
title: CVE
---

# Linux - CVE

## What is CVE

The Common Vulnerabilities and Exposures (`CVE`) system provides a reference-method for publicly known
information-security vulnerabilities and exposures. The United States' National Cybersecurity FFRDC,
operated by The Mitre Corporation, maintains the system, with funding from the US National Cyber Security
Division of the US Department of Homeland Security. The system was officially launched for the public in
September 1999.

The Security Content Automation Protocol uses CVE, and CVE IDs are listed on Mitre's system as well as
in the US National Vulnerability Database.

## CVE Database

There are many website which reference CVE.

See some useful websites:

- https://nvd.nist.gov
- https://access.redhat.com

## Examples of CVE

### CVE-2021-44228

- https://nvd.nist.gov/vuln/detail/CVE-2021-44228
- https://access.redhat.com/security/cve/cve-2021-44228

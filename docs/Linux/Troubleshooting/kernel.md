---
tags: [System, Linux]
title: Kernel
---

# Linux - Kernel

## Tips

Modify `sysctl` parameters:

```bash
/etc/sysctl.conf
/etc/sysctl.d/00-app.conf
```

## Library

List lib from a binary:

```bash
ldd /usr/bin/mail
```

Trace lib call:

```bash
ltrace
```

See libs of a rpm package:

```bash
rpm -qR mailx-12.5-19.el7.x86_64
```

Display loaded libs:

```bash
ldconfig -p
```

## Procedures

Rebuilding the initial ramdisk image (RHEL):

```bash
# Tested on RHEL 5, 6 & 7

# Sometimes it may be necessary to rebuild the initial ramdisk (also known as initrd or
# initramfs) to include new kernel modules, files, etc.

# RHEL 3, 4 & 5: Rebuilding the initrd
cp /boot/initrd-$(uname -r).img /boot/initrd-$(uname -r).img.bak
mkinitrd -f -v /boot/initrd-$(uname -r).img $(uname -r)

# If we are in a kernel version different to the initrd we are building we must specify
# the full kernel version, without architecture:
mkinitrd -f -v /boot/initrd-2.6.18-348.2.1.el5.img 2.6.18-348.2.1.el5

# For RHEL 6 & 7: Rebuilding the initramfs
cp /boot/initramfs-$(uname -r).img /boot/initramfs-$(uname -r).img.bak
dracut -f

# If we are in a kernel version different to the initrd we are building we must specify
# the full kernel version, including architecture:
dracut -f /boot/initramfs-2.6.32-220.7.1.el6.x86_64.img 2.6.32-220.7.1.el6.x86_64

# *** Consider adding a new grub/grub2 entry pointing to the initial ramdisk image, that
# will allow you to choose the old version at boot time without restoring the backup
```

Change boot order:

```bash
# Verify current version
uname -a

# Display kernel config
cat /etc/sysconfig/kernel

# Display Kernel list
awk -F\' '$1=="menuentry " {print $2}' /etc/grub2.cfg

# Change default
grub2-set-default 2

# Make grub config
grub2-mkconfig -o /boot/grub2/grub.cfg

# Restart
reboot 

# Verify new version
uname -a
```

---
tags: [System, Linux]
title: Rescue/Recovery
---

# Linux - Rescue/Recovery

## Grub Lines

Emergency boot:

```ini
systemd.unit=emergency.target
```

Start execution step by step with confirmation:

```ini
systemd.confirm_spawn=true
```

Disable SELinux:

```ini
selinux=0
```

## Procedures

### Rescue from Grub

Start rescue from Grub menu:

```bash
# Depuis le menu Grub:
[e]

# Edit line with linux16 and add:
init=/bin/bash 

# Save with:
[ctrl-x]

# After boot, Remount /:
mount -o rw,remount /
```

!!! note

    In some case, type the following command to create LVM special files in `/dev`:
    
    ```bash
    vgmknode
    ```

### Rescue from Boot CD-Rom

Rescue Mode after CD-Rom Boot:

```bash
# Change keyboard to FR
loadkeys fr

# Enable rootvg
vgchange -ay rootvg

# Create target directory
mkdir -p /target{,/usr,/home,/dev,/proc,/sys}

# Mount system FS
mount /dev/mapper/rootvg-root /target
mount /dev/mapper/rootvg-usr /target/usr
mount /dev/sda1 /target/boot
mount --bind /dev /target/dev
mount -t proc none /target/proc
mount -t sysfs none /target/sys

# Change root to target dir
chroot /target
```

### Force FSCK in boot

**RedHat 7|8**

Edit this file:

```bash
/etc/default/grub
```

Add `fsck.mode=force`and `fsck.repair=yes` options to `GRUB_CMDLINE_LINUX` vars:

```bash
# /etc/default/grub
...
GRUB_CMDLINE_LINUX="crashkernel=auto resume=/dev/mapper/rhel-swap rd.lvm.lv=rhel/root rd.lvm.lv=rhel/swap rhgb quiet fsck.mode=force fsck.repair=yes"
```

Rebuild grub2 file:

```bash
grub2-mkconfig -o /boot/grub2/grub.cfg
```

## Scripts

### Filesystem Corrupt

```bash
file -s /dev/dm* |grep errors |awk '{print $1}' |sed 's|:||'|sed 's|^.*/dm-||' |while read DM; do
  LV=$(dmsetup ls |grep "(253:$DM)" | awk '{print $1}')
  FS=$(df -ThP |grep -w $LV |awk '{print $NF}')
  [[ -z $FS ]] && FS=NOTMOUNTED
  echo $LV $FS
done
```

```bash
file -s /dev/dm* |grep errors |awk '{print $1}' |sed 's|:||'|sed 's|^.*/dm-||' |while read DM; do
  LV=$(dmsetup ls |grep "(253:$DM)" |awk '{print $1}')
  FS=$(df -ThP |grep -w $LV |awk '$NF != "NOTMOUNTED" {print $NF}')
  [[ -z $FS ]] && FS=NOTMOUNTED ;echo $LV $FS
done |awk '{print $NF}' |xargs -i lsof {} |awk '{print $2}' |sort -u |grep -v PID |xargs -i kill -9 {}
```

```bash
file -s /dev/dm* |grep errors |awk '{print $1}' |sed 's|:||'|sed 's|^.*/dm-||' |while read DM; do
  LV=$(dmsetup ls |grep "(253:$DM)" |awk '{print $1}')
  FS=$(df -ThP |grep -w $LV |awk '$NF != "NOTMOUNTED" {print $NF}')
  [[ -z $FS ]] && FS=NOTMOUNTED ;echo $LV $FS
done |awk '{print $NF}' |xargs -i umount {}
```

```bash
file -s /dev/dm* |grep errors |awk '{print $1}' |sed 's|:||'|sed 's|^.*/dm-||' |while read DM; do
  LV=$(dmsetup ls |grep "(253:$DM)" |awk '{print $1}')
  FS=$(df -ThP |grep -w $LV |awk '{print $NF}')
  [[ -z $FS ]] && FS=NOTMOUNTED ;echo $LV $FS
done |awk '$2 == "NOTMOUNTED" { print "fsck -y /dev/mapper/"$1 }' |sh
```

```bash
findmnt -kt ext4,xfs -O ro |sed -e 's|^|'`uname -n`';|' |grep -v TARGET
```


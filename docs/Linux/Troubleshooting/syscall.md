---
tags: [System, Linux]
title: SysCall
---

# Linux - Troubleshooting: SysCall

## Strace

Trace system call of a process:

```bash
strace -f -p {process}
# -f: display forked process
```

Trace system call of a command:

```bash
strace {command}
# Example: strace ip a 
```

Trace network calls of a curl request:

```bash
strace -f -e trace=network 2>&1 curl -k http://m2ko.net
```


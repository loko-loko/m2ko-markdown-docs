---
tags: [System, Linux, Logs, Security]
title: Auditd
---

# Linux - Auditd

## Config

Example of config (`/etc/audit/auditd.conf`):

```ini
local_events = yes
write_logs = yes
log_file = /var/log/audit/audit.log
log_group = root
log_format = ENRICHED
flush = INCREMENTAL_ASYNC
freq = 50
max_log_file = 20
num_logs = 20
priority_boost = 4
name_format = NONE
##name = mydomain
max_log_file_action = rotate
space_left = 75
space_left_action = email
verify_email = yes
action_mail_acct = root
admin_space_left = 50
admin_space_left_action = rotate
disk_full_action = SUSPEND
disk_error_action = SUSPEND
use_libwrap = yes
##tcp_listen_port = 60
tcp_listen_queue = 5
tcp_max_per_addr = 1
##tcp_client_ports = 1024-65535
tcp_client_max_idle = 0
transport = TCP
krb5_principal = auditd
##krb5_key_file = /etc/audit/audit.key
distribute_network = no
q_depth = 400
overflow_action = SYSLOG
max_restarts = 10
plugin_dir = /etc/audit/plugins.d
```

Reload configuration:

```bash
service auditd restart
```

## Playbook

Playbook to update auditd config:

```yaml
- name: Update auditd config
  hosts: all
  user: root
  gather_facts: no
  vars:
    auditd_parameters:
      num_logs: "30"
      space_left: "75"
      admin_space_left: "50"
      log_format: "ENRICHED"
      admin_space_left_action: "ROTATE"
      disk_full_action: "SUSPEND"
      disk_error_action: "SUSPEND"
      max_log_file_action: "keep_logs"
      space_left_action: "email"

  tasks:
    - ansible.builtin.lineinfile:
        path: /etc/audit/auditd.conf
        state: present
        regex: '^#?{{ item.key }} = .*'
        line: "{{ item.key }} = {{ item.value }}"
      register: auditd_config
      with_items: "{{ auditd_parameters|dict2items }}"
      
    - ansible.builtin.command: "service auditd restart"
      when: auditd_config.changed
```

## Link

- https://man7.org/linux/man-pages/man5/auditd.conf.5.html

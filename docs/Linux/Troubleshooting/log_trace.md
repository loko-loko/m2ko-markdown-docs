---
tags: [System, Linux]
title: History/Logs
---

# Linux - History/Logs

## Logs

### Journalctl

Display logs:

```bash
# All logs
journactl (-f)

# Service logs
journactl -u {service} (-f)

# Error logs
journactl -p err (-f)
```

Display logs with timestamp:

```bash
journalctl --since "2020-06-10 00:00" --until "2020-06-10 23:00"
```

Display only kernel logs:

```bash
journalctl [-k|--dmesg]
```

List boot ids:

```bash
journalctl --list-boots
```

Display only boot logs:

```bash
journalctl --boot ({boot-id})
```

Logs purge:

```bash
journalctl --vacuum-time=15d
```

!!! note

    You can add this command on crontab:
    
    ```bash
    # /etc/crontab  
    # *  *  *  *  * user-name  command to be executed
    0 0 * * * root journalctl --vacuum-time=15d
    ```

### Dmesg

Display dmesg logs:

```
dmesg (-T)
```

## History

Change history format:

```
export HISTTIMEFORMAT='%F %T '
```

Add permanently:

```
echo 'export HISTTIMEFORMAT="%F %T "' >> ~/.bashrc
source ~/.bashrc
```

## Logrotate

### Example

Logrotate avec un max de 10 archives:

```bash
/var/log/mysqld.log {
	rotate 10                # Maximum de fichiers rotate
	size 50M                 # Taille maximum avant rotation
	nodateext                # Pas de date pour l'extension (*.1)
	missingok                # Pas d'erreur en cas de non présence du fichier
	notifempty               # Pas de rotate si le fichier est vide
	compress                 # Compression
	copytruncate             # Permet de garder le même fichier de log
	create 0664 mysql mysql  # Création du nv fichier avec droits custom
}
```

Modification logrotate syslog avec rotate 5 + weekly + size 100M + compress:

```bash
/var/log/cron
/var/log/maillog
/var/log/messages
/var/log/secure
/var/log/spooler
{
    rotate 5
    weekly
    size 100M
    compress
    sharedscripts
    postrotate
        /bin/kill -HUP `cat /var/run/syslogd.pid 2> /dev/null` 2> /dev/null || true
    endscript
}
```

Logrotate avec un max de 10 archives:

```bash
/var/log/cacti/cacti.log {
        su cacti apache
        rotate 10
        size 50M
        nodateext
        missingok
        notifempty
        compress
        create 0664 cacti apache
}
```

En crontab:

```bash
*/5  *  *  *  * /usr/sbin/logrotate -f /etc/logrotate.d/mysqld.new
```


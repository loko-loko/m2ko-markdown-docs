---
tags: [System, Linux]
title: Filesystem
---

# Linux - Troubleshooting: FS

## Tips

Display FS with needed FSCK:

```bash
file -s /dev/dm-*
```

## Procedures

### Display ReadOnly FS

With `/proc/mounts`:

```bash
EXCLUDEFS="/sys/fs/cgroup /some/other/fs"
for mntpt in $(cat /proc/mounts |awk '{print $2}'); do
  grep -qw $mntpt <<< $EXCLUDEFS && continue
  [[ -w $mntpt ]] || echo -e "ro -- $mntpt"
done
```

With `findmnt`:

```bash
findmnt -kt ext4,xfs -PO ro |sed 's|^|'`uname -n`';|'
```

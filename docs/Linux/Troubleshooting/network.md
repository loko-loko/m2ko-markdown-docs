---
tags: [System, Linux, Network]
title: Network
---

# Linux - Troubleshooting: Network

## TCPDump

Scan traffic from a host:

```bash
tcpdump -i {interface} host {host|ip}
# Ex: tcpdump -i eth0 host 10.30.28.2
```

Scan traffic from a subnet:

```bash
tcpdump -i {interface} net {cidr}
# Ex: tcpdump -i eth0 net 10.12.0/24
```

Scan traffic from a subnet with mac:

```bash
tcpdump -i {interface} host {host|ip} -en
```

## ARP Table

List arp table:

```bash
arp
```

Get ARP response from interface for given IP:

```bash
arping -c {count} -I {interface} {address}
# Ex: arping -c 1 -I br-ex 10.30.28.2
```

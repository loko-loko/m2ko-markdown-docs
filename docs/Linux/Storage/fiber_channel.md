---
tags: [System, Linux]
title: Fiber Channel
---

# Linux - Fiber Channel

## Commands

List FC cards:

```bash
lspci -nn | grep Fibre
```

Retrieve FC host info with this path:

```bash
find /sys | grep  "15:00.0" | grep port_name
```

Display FC card detail:

```bash
lspci -v -s "05:00.0"
```

Display FC driver version:

```bash
modinfo -F version qla2xxx
modinfo qla2xxx | grep version
```

Check that FC driver is load in kernel:

```bash
lsmod | grep qla2xxx
```

Display FC host port name:

```bash
cat /sys/class/fc_host/host5/port_name
```

Display FC host info:

```bash
for port in /sys/class/fc_host/host?/port_*; { echo "$port: $(cat $port)"; }
```

Display speed of card:

```bash
grep -Hv "zz" /sys/class/fc_host/host*/speed
```

## Procedures

Disable HBA Port via FC Driver:

```bash
# Get FC host:
ls -l /sys/class/fc_host

# Deactivate card from driver
echo "0000:3c:00.0" > /sys/bus/pci/drivers/qla2xxx/unbind

# Reactivate card from driver
echo "0000:3c:00.0" > /sys/bus/pci/drivers/qla2xxx/bind
```

Enable verbose log from FC driver:

```bash
chmod u+w /sys/module/qla2xxx/parameters/ql2xextended_error_logging

echo "1" &gt; /sys/module/qla2xxx/parameters/ql2xextended_error_logging

sysctl -w dev.scsi.logging_level=0x1003
```

---
tags: [System, Linux, CIFS, Network]
title: CIFS
---

# Linux - CIFS

## Examples

Fstab CIFS line example:

```bash
# Add to /etc/fstab
//cifs-server/share /data cifs credentials=/root/creds.cifs,rw,async,exec,auto,uid=user1,forceuid,gid=group1,forcegid,file_mode=0664,dir_mode=0775,vers=3.0,noserverino,nounix 0 0
```

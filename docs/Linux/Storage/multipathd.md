---
tags: [System, Linux]
title: Multipath
---

# Linux - Multipath

## Install

Install package:

```bash
yum install device-mapper-multipath
```

## Commands

### Multipath

List paths:

```bash
multipath -ll
```

Rescan paths:

```bash
# BEFORE: Scan path
rescan-scsi-bus.sh (-i)

# Scan
multipath

# Add verbosity
multipath -v3

# Force devmap reload
multipath -r
```

Clean paths:

```bash
multipath -F
```

### Multipathd

Start interactive shell:

```bash
multipathd -k
```

Delete path:

```bash
multipathd -f {path}
```

## Procedures

### Resize volume

Get path of device:

```bash
multipath -ll mpatha
```

Rescan paths:

```bash
echo "1" > '/sys/class/block/sdb/device/rescan'
echo "1" > '/sys/class/block/sdd/device/rescan'
```

Reload multipath config:

```bash
multipath -F
multipath -v2 mpatha
```

Resize map:

```bash
multipathd -k"resize map mpatha" -vv
```

Resize volume:

```bash
pvresize /dev/mapper/mpatha
```

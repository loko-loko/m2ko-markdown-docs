---
tags: [System, Linux]
title: Disk Management
---

# Linux - Disk Management

## Commands

### Block

List volumes:

```bash
lsblk
fdisk -l # RedHat 5.*
```

Display volume with UUID and fs type:

```bash
lsblk -fe7 -o +size
```

Display scsi devices:

```bash
cat /proc/scsi/scsi
```

Scan devices:

```bash
for h in $(ls /sys/class/scsi_host/); do echo "- - -" > /sys/class/scsi_host/$h/scan; done
```

Delete scsi device:

```bash
echo 1 > /sys/class/scsi_device/4\:0\:0\:0/device/delete
```

Delete lun device:

```bash
echo 1 > /sys/block/sdf/device/delete
```

### Device Mapper

List DMs:

```bash
dmsetup ls
```

Display detail of DMs:

```bash
dmsetup info -c
```

Display state of DMs:

```bash
file -s /dev/dm*
```

### FileSystem

Check FS parameters:

```bash
tune2fs -l {fs}
```

Change FS parameters:

```bash
tune2fs -c {parameter} {fs}
```

Check FS:

```bash
fsck {options} {device}
# Ex:
#   fsck /dev/sdb    -> Check FS
#   fsck -f /dev/sdb -> Force Check
#   fsck -c /dev/sdb -> Check Bad Sectors
#   fsck -y /dev/sdb -> Always Answer Yes (! Use with Caution)
```

## Tips

Get directory size from a FS with excluding all child FS:

```bash
du -hs /var/* --exclude-from <(df | awk '$NF ~ /\/var\/*/ {print $NF}')
```

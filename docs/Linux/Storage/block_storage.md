---
tags: [System, Linux, Storage]
title: Block Storage
---

# Linux - Block Storage

## Commands

### ISCSI

List volumes:

```bash
lsblk
# Or
fdisk -l
# Or
blockdev --report
```

Display scsi devices:

```bash
cat /proc/scsi/scsi
```

Scan devices:

```bash
for h in $(ls /sys/class/scsi_host/); do echo "- - -" > /sys/class/scsi_host/$h/scan; done
```

Delete scsi device:

```bash
echo 1 > /sys/class/scsi_device/4\:0\:0\:0/device/delete
```

Delete lun device:

```bash
echo 1 > /sys/block/sdf/device/delete
```

### Fiber Channel

List fiber devices:

```bash
lspci -nn | grep Fibre
```

Retrieve host info with path:

```bash
find /sys | grep  "15:00.0" | grep port_name
```

Display port name of a FC host:

```bash
cat /sys/devices/pci0000:00/0000:00:05.0/0000:15:00.0/host5/fc_host/host5/port_name

```

Display WWNs of FC hosts:

```bash
for port in /sys/class/fc_host/host[0-9]/port_name; { echo -n "$port : "; cat $port; }
```

Display detail of a FC card:

```bash
lspci -v -s 05:00.0
```

### Partition

Partition detail:

```bash
fdisk -l /dev/sdb
```

Partition size:

```bash
fdisk -s /dev/sdb
```

Remove partition (Option <kbd>d</kbd>):

```bash
fdisk {pv-path}
# Ex:
#   fdisk /dev/sdb
#     List partitions:  p
#     Remove partition: d
#       Select Partition: {part-id}
```

Inform OS that partition table changes:

```bash
partprobe 
```

List partition:

```bash
partprobe -s
```

### Troubleshooting

Display header of a volume:

```bash
hexdump -C {pv-path}
# Ex: hexdump -C /dev/sdd
```

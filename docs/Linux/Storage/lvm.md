---
tags: [System, Linux]
title: LVM
---

# Linux - LVM

## Globals

Install LVM:

```bash
yum install lvm2
```

Config file:

```bash
/etc/lvm/lvm.conf
```

Archive path which contain all previous lvm map:

```bash
/etc/lvm/archive
# Ex of file: vg_data_00002-1038365342.vg
```

## Commands

### Logical Volume (LV)

LV detail:

```bash
lvdisplay -v {lv-path}
# Ex: lvdisplay -v /dev/Debian/lv_dynatrace
```

Display all lv:

```bash
lvs
lvscan
```

Extend FS:

```bash
lvextend -L +{size} {lv-path} -r
# Ex: lvextend -L +2G /dev/vg_data/lv_data -r
```

Create LV with max size available:

```bash
lvcreate -l 100%FREE -n {lv} {vg}
# Ex: lvcreate -l 100%FREE -n lv_tempdb vg_data
```

List LV by devices:

```bash
lvs -a -o +devices
```

Delete PV:

```bash
pvremove {pv-path}
# Ex: pvremove /dev/sdc
```

### Volume Group (VG)

Disable VG:

```bash
vgchange -an {vg}
```

Enable VG:

```bash
vgchange -ay {vg}
```

List all LVs of a VG:

```bash
lvdisplay {vg}
```

Create VG:

```bash
vgcreate {vg} {pv-path} (-s {block-size})
# Ex: vgcreate vg_data /dev/sdb -s 128M
```

Remove PV of a VG:

```bash
vgreduce {vg} {pv-path}
# Ex: vgreduce vg_data /dev/sdc
```

## Troubleshooting

Force refresh of metadata:

```bash
systemctl restart lvm2-lvmetad.service
```

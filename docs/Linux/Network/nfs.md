---
tags: [System, Linux, NFS, Network]
title: NFS
---

# Linux - NFS

## Commands

Display mount point of a NFS server:

```bash
showmount -e {nfs-server}
```

Display NFS mount point:

```bash
nfsstat -m
```

Display NFS stats:

```bash
nfsiostat
```

Display process state of a NFS server:

```bash
rpcinfo -p {nfs-server}
```

Display `rpc.mountd` state of a NFS server:

```bash
rpcinfo -u {nfs-server} mountd
```

## Procedures

### Specify NFS version (v3)

From `fstab`:

```bash
# /etc/fstab
nfs-server:/volume1 /data/share nfs nfsvers=3 0 0
nfs-server:/volume1 /data/share nfs vers=3 0 0    # Same as `nfsvers`
```

From NFS config:

```bash
# /etc/nfsmount.conf
Defaultvers=3
```

## Links

- [RHEL - NFS Client Config Options](https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/7/html/storage_administration_guide/s1-nfs-client-config-options)
- [NFS Mount Options](https://linux.die.net/man/5/nfs)


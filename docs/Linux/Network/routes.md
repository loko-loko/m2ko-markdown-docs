---
tags: [System, Linux, Network]
title: Routes
---

# Linux - Routes

## Commands

### Netstat

Route list:

```bash
route -n
netstat -rn
```

### IP

Display route:

```bash
ip route
ip -4 route
ip -6 route
```

Add new route:

```bash
ip route add {interface}/{mask} via {gateway}

# Persiste route
echo "{interface}/{mask} via {gateway} dev {interface}" >> /etc/sysconfig/network-scripts/route-{interface}

# Example:
#   ip route add 10.4.0.2/32 via 10.1.0.1
#   echo "10.4.0.2/32 via 10.1.0.1 dev eth1" >> /etc/sysconfig/network-scripts/route-eth1
```

Delete route:

```bash
ip route del {interface} via {gateway}

# From network-scripts config
vim /etc/sysconfig/network-scripts/route-{interface}
# Delete -> {interface}/{mask} via {gateway} dev {interface}

# Example:
#   ip route del 10.4.0.2 via 10.1.0.1
#   + Delete line from /etc/sysconfig/network-scripts/route-eth1
```

### NMCli

Add new route:

```bash
nmcli connection modify {interface} +ipv4.routes "{ip}/{mask} {gateway}"
# Example:
#   nmcli connection modify eth1 +ipv4.routes "10.3.0.4/32 10.2.0.1"
```

Delete route:

```bash
nmcli connection modify {interface} -ipv4.routes "{ip}/{mask} {gateway}"
# Example:
#   nmcli connection modify eth1 -ipv4.routes "10.3.0.4/32 10.2.0.1"
```

Reload route:

```bash
nmcli connection up  {interface}
# Example: nmcli connection up eth1
```
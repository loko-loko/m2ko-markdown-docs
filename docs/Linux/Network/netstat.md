---
tags: [System, Linux, Network]
title: Netstat
---

# Linux - Netstat

## Commands

Route list:

```bash
netstat -rn
```

Display active connection with associate pid/progam:

```bash
netstat -taupen
# -t: tcp
# -a: all
# -u: udp
# -p: progam
# -e: extend (user/inode)
# -n: display numeric port
```

## Tips

Group TCP connection's states

```bash
netstat -an |awk '/tcp/ {print $6}' |sort |uniq -c
```


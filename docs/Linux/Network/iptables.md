---
tags: [System, Linux, Network]
title: IPTables
---

# Linux - IPTables

## Commands

Display rule's commands:

```bash
iptables -S
```

Display rules with index:

```bash
iptables -L --line-numbers
```

Display detail of rules:

```bash
iptables -L -v -n
```

Create ACCEPT rule:

```bash
iptables -A {type} -s {host/ip/cidr} -p {protocol} -j ACCEPT (--dport {port}}
# Ex: iptables -A INPUT -s myhost -p tcp -j ACCEPT --dport 9300
```
Create REJECT rule:

```bash
iptables -A {type} -p {protocol} -m {protocol} --dport {port} -j REJECT (--reject-with {rejected-rule})
# Ex: iptables -A INPUT -p tcp -m tcp --dport 9300 -j REJECT --reject-with icmp-port-unreachable
```

Delete rule:

```bash
iptables -D {rule-type} {rule-id}
# Ex:
#  iptables -D INPUT 1
#  iptables -D OUTPUT 3
```

## Backup/Restore

Backup iptables rules to a file:

```bash
iptables-save > {file}
# Ex:
#   iptables-save > /root/iptables.dump
#   iptables-save > /etc/sysconfig/iptables (To persist on system)
```

Restore iptables rules from a file:

```bash
iptables-restore < {file}
# Ex:
#   iptables-restore < /root/iptables.dump
```

## Tips

Watch intercepted package by rule:

```bash
watch -d -n2 'iptables -nvL | grep -v "0 0"'
```

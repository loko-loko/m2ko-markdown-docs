---
tags: [System, Linux, Network]
title: Network Manager
---

# Linux - Network Manager

## Configs

Example of Networkmanager configs file:

```ini
# /etc/NetworkManager/conf.d/90-dns.conf
[main]
dns=default
systemd-resolved=false

[global-dns]
searches=m2ko.com,datacenter.m2ko.grp
options=timeout:2,attempts:1,rotate
[global-ns-domain-*]
servers=8.8.8.8,1.1.1.1
```

> Restart service to apply modification: `systemctl restart NetworkManager`

## Commands

### Globals

Check service state:

```bash
systemctl status NetworkManager
```

Display general status:

```bash
nmcli general
```

### Info

List interface:

```bash
nmcli connection show
```

List all devices:

```bash
nmcli device status
```

List all devices detail:

```bash
nmcli device show
```

Detail of an interface:

```bash
nmcli con show {interface}
# Ex: nmcli con show bond0
```

### Action

Start connection:

```bash
nmcli connection up {interface}
# Ex: nmcli connection up bond0
```

Disconnect device:

```bash
nmcli dev disconnect {interface}
# Ex: nmcli dev disconnect bond0
```

Add interface with tagged VLAN:

```bash
nmcli connection add \
  type vlan \
  ifname {interface}.{vlan_id} \
  con-name {interface}.{vlan_id} \
  id {vlan_id} \
  dev {interface} \
  ip4 {ip}/{sub} \
  gw4 {gateway}

# Example:
#   nmcli connection add \
#     type vlan \
#     ifname eth0.5 \
#     con-name eth0.5 \
#     id 5 \
#     dev eth0 \
#     ip4 192.168.1.2/24
```

Add interface without tagged VLAN:

```bash
nmcli connection add \
  type ethernet \
  ifname {interface} \
  con-name {interface} \
  ip4 {ip}/{sub}

# Example:
#   nmcli connection add \
#     type ethernet \
#     ifname eth0 \
#     con-name eth0 \
#     ip4 192.168.1.2/24
```

Modify interface:

```bash
nmcli connection modify {interface} {parameter} {value}
# Ex: nmcli connection modify eth1 ipv4.dns-search "m2ko.net,m4ko.com"
```

Edit connection:

```bash
nmcli connection edit {interface}
# Ex: nmcli connection edit eth1 ('print' to display all value)
```

Start edit command(s) from terminal:

```bash
echo '{cmd1}
{cmd2}' | nmcli con edit {interface}

# Ex:
#   echo 'print ipv4.gateway
#   set ipv4.gateway 192.168.3.1
#   save' | nmcli con edit bond0.10
```

Create static route:

```bash
nmcli connection modify {interface} +ipv4.routes "{cidr} {gateway}"
# Ex: nmcli connection modify eth0 +ipv4.routes "10.1.0.0/16 10.2.0.1"
```

## Procedures

### Remove Gateway from Interface

Edit connection:

```bash
nmcli connection edit bond0
```

Remove + display new value:

```bash
> remove ipv4.gateway
> print ipv4.gateway
```

Save + activate configuration:

```bash
> save persistent
> activate bond0
```

## Scripts

Delete all connections:

```bash
# Display only:
nmcli --fields UUID con show | awk '! /UUID/ {printf "nmcli con delete uuid %s\n", $1}'

# Execute:
nmcli --fields UUID con show | awk '! /UUID/ {printf "nmcli con delete uuid %s\n", $1}' | sh
```

Create bond with tagged interface:

```bash
DRY_RUN=1

bond_name="bond0"
bond_slaves=("eno1np0" "eno2np1")
vlan_id="10"
ip="10.19.1.3"
sub="28"
gateway="10.19.1.1"
dns_search="m2ko.net"
dns=("4.4.4.4" "8.8.8.8")

function exec_cmd() {
    cmd=$(echo $1 | sed 's/\ +/\ /g')
    echo "COMMAND: $cmd"
    [[ $DRY_RUN == 0 ]] && $cmd
}

# Reload connection
exec_cmd "nmcli connection reload"

# Add bond
exec_cmd "nmcli connection add type bond \
            ifname ${bond_name} \
            con-name ${bond_name} \
            mode 802.3ad miimon 110"

# Modify bond
exec_cmd "nmcli connection modify ${bond_name} \
            ipv4.method disabled \
            ipv6.method ignore"

# Add slaves
for slave in ${bond_slaves[@]}; do
    exec_cmd "nmcli connection add type bond-slave \
                ifname $slave \
                con-name $slave \
                master ${bond_name}"
done

# Start connection
exec_cmd "nmcli connection up ${bond_name}"

# Create tagged interface
exec_cmd "nmcli connection add type vlan \
            ifname ${bond_name}.${vlan_id} \
            con-name ${bond_name}.${vlan_id} \
            id ${vlan_id} \
            dev ${bond_name} \
            ip4 ${ip}/${sub} \
            gw4 ${gateway} \
            ipv4.dns \"$(echo ${dns[@]})\" \
            ipv4.dns-search \"${dns_search}\""
  
# Start tagged interface
exec_cmd "nmcli connection up ${bond_name}.${vlan_id}"
```
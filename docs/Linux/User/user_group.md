---
tags: [System, Linux]
title: User/Group
---

# Linux - User/Group

## Commands

Create user:

```bash
useradd {user} -u {uid} -G {groups} -d {home} -s {shell}
# Ex:
#  useradd lako -u 500 -G docker -d /home/lkone -s /bin/bash
```

Create group and user:

```bash
groupadd -g {gid} {group}
useradd -u {gid} -g {gid} {name}

# Ex:
#  groupadd -g 500 lako
#  useradd -u 500 -g 500 lako
```

Add user into a group:

```bash
usermod -a -G {group} {user}
# Ex: usermod -a -G docker lako
```

Change user password:

```bash
echo "{password}" | passwd --stdin {user}
echo '{user}:{password}' | chpasswd

# Ex:
#  echo "@pass" | passwd --stdin lako
#  echo 'lako:@pass' | chpasswd
```

Force password change to next connection:

```bash
chage -d 0 {user}
# Ex: chage -d 0 lako
```

## Files

Important files:

|path |description
|--- |---
|`/etc/passwd` |Contain all users info
|`/etc/group` |Contain all groups info
|`/etc/shadow` |Contain all passwords info

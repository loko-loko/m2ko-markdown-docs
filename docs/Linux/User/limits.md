---
tags: [System, Linux]
title: Limits
---

# Linux - Limits

## Commands

Display user's limits:

```bash
ulimit -a
# Example Output:
#   core file size          (blocks, -c) 0
#   data seg size           (kbytes, -d) unlimited
#   scheduling priority             (-e) 0
#   file size               (blocks, -f) unlimited
#   pending signals                 (-i) 31116
#   max locked memory       (kbytes, -l) 64
#   max memory size         (kbytes, -m) unlimited
#   open files                      (-n) 1024
#   pipe size            (512 bytes, -p) 8
#   POSIX message queues     (bytes, -q) 819200
#   real-time priority              (-r) 0
#   stack size              (kbytes, -s) 8192
#   cpu time               (seconds, -t) unlimited
#   max user processes              (-u) 31116
#   virtual memory          (kbytes, -v) unlimited
#   file locks                      (-x) unlimited
```

List Hard/Soft Open files user limit:

```bash
ulimit -Hn
ulimit -Sn
```

Display current limit of a process:

```bash
cat /proc/{pid}/limits
```

## Procedures

Add limits:

```bash
# Edit file /etc/security/limits.conf
# Add the following values:
#   {user} soft nofile 65536
#   {user} hard nofile 65536
```
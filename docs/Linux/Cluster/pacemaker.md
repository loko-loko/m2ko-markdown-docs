---
tags: [System, Linux, Cluster]
title: Pacemaker
---

# Linux - Pacemaker

## Commands

Switch to maintenance mode:

```bash
pcs property set maintenance-mode=true
```

Display status:

```bash
pcs status
pcs cluster status
```

Start/Stop cluster:

```bash
pcs cluster [start|stop]
```

Display config:

```bash
pcs config
```

Clean resource error count:

```bash
pcs resource cleanup {resource}
```

Cluster logs path:

```bash
/var/log/cluster
```

Watch status:

```bash
crm_mon
```
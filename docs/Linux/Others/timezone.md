---
tags: [System, Linux]
title: TimeZone
---

# Linux - TimeZone

## Commands

Modify timezone:

```bash
mv /etc/localtime /etc/localtime.backup
ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
```

Display date:

```bash
date
# With Formating:
date '+%y/%m/%d %H:%M:%Y'
```

Display Kernel date:

```bash
hwclock
```

### Timedatectl

Timezones list:

```bash
timedatectl list-timezones
```

Display TZ info:

```bash
timedatectl status
```

Set new timezone:

```bash
timedatectl set-timezones Europe/Paris
```

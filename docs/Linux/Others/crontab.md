---
tags: [System, Linux]
title: Crontab
---

# Linux - Crontab

## Configuration

### Editor

Change default editor:

```bash
export VISUAL=/usr/bin/vi
```

!!! note

    To persist editor, add `export VISUAL=/usr/bin/vi` to `~/.bash_profile`

## Commands

Diaply crontab:

```bash
crontab -l
```

Delete crontab:

```bash
crontab -r
```

Edit crontab:

```bash
crontab -e
```

Display crontab of a user:

```bash
crontab -u {user} -l
```

## Syntax

```bash
# Example of job definition :
#
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  *  user command to be executed
```

See values detail:

```bash
mm hh jj MMM JJJ [user] task > log
```

*   `mm` : minutes (00-59).
*   `hh` : hours (00-23) .
*   `jj` : day of month (01-31).
*   `MMM` : month (`01-12` or: jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec).
*   `JJJ` : day of week (`1-7` or: mon, tue, wed, thu, fri, sat, sun).
*   `user (optional)` : Use which execute task.
*   `task` : command to execute.
*   `> log (optional)` : redirect to a file.

For each units, we can use the following annotation:

*   `x-y` : Unit time from `x` to `y` (Ex: `1-6`).
*   `*/x` : All `x` time unit (Ex: `*/6`).
*   `x,y,i` : Unit time (Ex: `2,7`).

## Examples

Execute `plop.sh` all the days of week at `02:00`:

```
0 2 * * * /home/user/plop.ksh > plop.log
```

Execute `plop.sh` every 2 hours:

```
0 */2 * * * /home/user/plop.ksh > plop.log
```

Execute `plop.sh` every 1 hours and 30 minutes:

```
30 */1 * * * /home/user/plop.ksh > plop.log
```

Execute `plop.sh` every 30 minutes:

```
*/30 * * * * /home/user/plop.ksh > plop.log
```

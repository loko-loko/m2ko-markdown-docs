---
tags: [System, Linux]
title: Mail
---

# Linux - Mail

##  Commandes

Envoyer un mail:

```bash
MAIL_TEXT="Texte du mail"
MAIL_OBJECT="Objet du mail"
MAIL_DEST="lkone@m2ko.com"

echo "$MAIL_TEXT" | mailx -s "$MAIL_OBJECT" $MAIL_DEST
```

Envoyer un mail avec fichier:

```bash
MAIL_TEXT="Texte du mail"
MAIL_OBJECT="Objet du mail"
MAIL_FILE="/save/appli/passerelle/CD/logs/RECAP_013472_20190114.txt"
MAIL_DEST="lkone@m2ko.com"

echo "$MAIL_TEXT" | mailx -s "$MAIL_OBJECT" -a $MAIL_FILE $MAIL_DEST
```

Envoyer une commande par mail:

```bash
MAIL_TEXT="Warning: DRBD volume sync problem on ZABBIX proxy $(hostname)"
MAIL_DEST="lkone@m2ko.com"

drbdadm status | mailx -s "$MAIL_TEXT" "$MAIL_DEST"
```

## Procedures

### Vérifier la configuration mail

```bash
# Vérifier que le serveur smtp répond
nc -v {server} 25

# Vérifier l'état du serveur postfix:
systemctl status postfix  # RHEL7/8
service postfix status    # RHEL6

# Vérifier la configuration postfix:
cat /etc/postfix/main.cf

# Si pas présente, ajouter dans le fichier main.cf:
mydomain = m2ko.net
relayhost = {smtp-server}

# Et reload de la configuration
systemctl reload postfix  # RHEL7/8
service postfix reload    # RHEL6
```

## Link

- Configurer MailX: https://www.starmate.fr/envoyer-des-courriels-via-mailmailx/

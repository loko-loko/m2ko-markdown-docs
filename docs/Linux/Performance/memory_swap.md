---
tags: [System, Linux]
title: Memory/Swap
---

# Linux - Memory/Swap

## Commandes

Display available memory:

```bash
free -m
```

Display consumed swap:

```bash
swapon -s
```

Disable swap:

```bash
# For a specific swap lv
swapoff {lv-swap}

# For all
swapoff -a
```

Enable swap:

```bash
# For a specific swap lv
swapon {lv-swap}

# For all
swapon -a
```

Flush the swap:

```bash
swapoff -a && swapon -a
```

Display `vm.swappiness` value:

```bash
sysctl -n vm.swappiness
```

Set new value of `vm.swappiness`:

```bash
sysctl -w vm.swappiness={value}
# Ex: sysctl -w vm.swappiness=5
```

Create swap:

```bash
mkswap {lv-swap}
# Ex:
#   lvcreate -L 2G -n lv_swap2 rootvg
#   mkswap /dev/rootvg/lv_swap2 
```

Memory stats:

```bash
vmstat 1
```

Display process that use swap:

```bash
for f in /proc/*/status ; do swp=$(awk '/VmSwap|Name/ {printf "%s %s\n", $2, $3}' $f); echo $swp; done | grep -E 'B$' | sort -k 2 -nr
```

## Procedures

### Drop Memory Cache

```bash
# Display memory
free -m

# Flush FS buffers
sync; sync; sync

# Drop cache
echo 1 > /proc/sys/vm/drop_caches
```

#### Add Swap

```bash
# Create swap 
lvcreate -L 2G -n lv_swap2 rootvg 
mkswap /dev/rootvg/lv_swap2 

# Add new entry in /etc/fstab 
/dev/mapper/rootvg-lv_swap2 swap swap defaults 0 0 

# Activate swap
swapon -v /dev/mapper/rootvg-lv_swap2

# Verify the swap
free -m 
```

---
tags: [System, Security, Tools]
title: OpenSSL
---

# Tools - OpenSSL

## Glossary

Here some definitions:

- `CSR`: Certificate Signing Request

## Commands

### Creation

Generate a CSR certificate with your private key:

```bash
openssl req -out {csr-cert} -new -newkey rsa:2048 -nodes -keyout {key-cert}
```

Generate a self-signed certificate:

```bash
openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout {key-cert} -out {crt-cert}
```

Generate a CSR certificate from an existing key:

```bash
openssl req -out {csr-cert} -key {key-cert} -new
```

Generate a CSR certificate from an existing certificate:

```bash
openssl x509 -x509toreq -in {crt-cert} -out {csr-cert} -signkey {key-cert}
```

Delete the passphrase on a key:

```bash
openssl rsa -in {key-cert} -out {new-key-cert}
```

### Verification

Check a CSR certificate:

```bash
openssl req -text -noout -verify -in {csr-cert}
```

Check a private key:

```bash
openssl rsa-in {key-cert} -checkvbash
```

Check a certificate:

```bash
openssl x509 -in {crt-cert} -text -noout
```

Retrieve the MD5 hash of the public key to ensure that it corresponds to the CSR and the private key:

```bash
openssl x509 -noout -modulus -in {crt-cert} | openssl md5
openssl rsa -noout -modulus -in {key-cert} | openssl md5
openssl req -noout -modulus -in {csr-cert} | openssl md5
```

Check an SSL connection (All certificates, including intermediates will be displayed):

```bash
openssl s_client -connect {host}:{port}
```

Display certificat of remote server:

```bash
echo | openssl s_client -showcerts -servername localhost -connect {host}:{port} 2>/dev/null | openssl x509 -inform pem -noout -text
```

Display End date certiifcate of remote server:

```bash
echo | openssl s_client -showcerts -servername localhost -connect {host}:{port} 2>/dev/null | openssl x509 -inform pem -noout -enddate
```
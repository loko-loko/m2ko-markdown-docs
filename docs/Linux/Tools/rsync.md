---
tags: [System, Tools]
title: Rsync
---

# Tools - Rsync

## Commands

Migrate data to a remote server:

```bash
rsync -av /data/ server:/new_data/
```

Exclude file or directory:

```bash
rsync -av --exclude '.snapshot' /data/ /new_data/
```

Delete missing files on remote:

```bash
rsync -av --delete /data/ /new_data/
```

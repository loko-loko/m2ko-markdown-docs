---
tags: [System, Linux, Tools, RedHat]
title: Satellite
---

# Linux - Satellite

## Globals

Configuration of subscription manager:

```bash
/etc/rhsm/
```

## Commands

### Yum/Sub.Manager

List Satellite managers:

```bash
subscription-manager list
```

Repos list:

```bash
subscription-manager repos
```

Bypass a repo if not available:

```bash
yum-config-manager --save --setopt=repo.skip_if_unavailable=true
```

Disable repo:

```bash
yum-config-manager --disable repo
```

Modify config parameter (`/etc/rhsm/rhsm.conf`):

```bash
subscription-manager config --server.{parameter}={value}
# Ex: subscription-manager config --server.no_proxy=*
```
Show subscription-manager identity of host:

```bash
subscription-manager identity
```

### Maintain

Display Satellite components states:

```bash
satellite-maintain service status
```

Stop Satellite components:

```bash
satellite-maintain service stop
```

Start Satellite components:

```bash
satellite-maintain service start
```

### Hammer

Get health of Satellite components:

```bash
hammer ping
```

List Organization:

```bash
hammer organization list
```

List content-view:

```bash
hammer content-view list
```

List activation-key:

```bash
hammer activation-key list
```

Detail of an activation-key:

```bash
hammer activation-key show --name {ak}
```

List associate subscription of an activation-key:

```bash
hammer activation-key subscriptions --name {ak}
```

List hosts

```bash
hammer host list
```

## Scripts

Disable PHP repos:

```bash
subscription-manager repos | awk '/Repo ID/ && /PHP/ {printf "yum-config-manager --disable %s\n", $NF}'
```

## Procedures

### Register Satellite 6 sur un host:

```bash
# Installer katello:
rpm -Uvh http://satellite.local/pub/katello-ca-consumer-latest.noarch.rpm

# Register (CentOS):
/usr/sbin/subscription-manager register \
  --org 'Org' \
  --name 'myserver1' \
  --activationkey 'AK-RHEL8-SERVER' \
  --serverurl=https://satellite.local:443/rhsm \
  --baseurl=https://satellite.local/pulp/repos \
  --force

# Installer l'agent katello
yum -y install katello-agent
```

---
tags: [System, Tools, Web]
title: Apache
---

# Tools - Apache

## Commands

Module list:

```bash
apachectl -M
apache2ctl -M
httpd -M
```

## Packages

Install php mod for apache:

```bash
yum install php_mod
```
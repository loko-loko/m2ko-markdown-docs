---
tags: [System, Tools, Multiplexer]
title: Screen
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Tools - Screen

## Description

`screen` is a "terminal multiplexer" allowing to open several terminals in the same console, to switch from one to the other and to recover them later.

It offers other very useful services such as:

*   The ability to attach and detach a session
*   The possibility of sharing a terminal with another user, ideal for helping a remote user.

## Installation

If `screen` is not already installed on your distribution, launch the installation simply as follows:

<Tabs>
<TabItem value="centos-rhat" label="CentOS/RedHat" default>

```
yum (or dnf) update
yum (or dnf) install screen
```

</TabItem>
<TabItem value="ubuntu" label="Ubuntu">

```
apt-get update
apt-get install screen
```

</TabItem>
</Tabs>

## Use it

### Some commands

Create a new session

```bash
screen -S <session_name>
```

Close a session:

```bash
exit
```

List&nbsp;current session:

```bash
screen -ls
```

Attach to an existing session:

```bash
screen -r
```

Force detachment from a session:

```bash
screen -d <Id|Name Session>
```

### Shortcuts

There are many keyboard shortcuts for interacting with sessions.

#### Kill screen

|Combination |Description
|--- |---
|<kbd>Ctrl</kbd> + <kbd>D</kbd> |Equivalent to exit. When there is only one console left screen
|<kbd>Ctrl</kbd> + <kbd>A</kbd> |Also quits and closes. When there is only one console left screen

#### Detach&nbsp;screen

|Combination |Description
|--- |---
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then <kbd>d</kbd> |Detach screen
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then <kbd>DD</kbd> |Detach screen and close session

#### Split console

|Combination |Description
|--- |---
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then <kbd>\|</kbd> |Separates the current console into two consoles vertically (the current console becomes the one on the left).
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then <kbd>S</kbd> |Separates the current console into two consoles horizontally (the current console becomes the one above).
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then <kbd>Tab</kbd> |Once the console is separated, this shortcut allows you to position yourself on the next console.
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then​​​​​​​ <kbd>Q</kbd> |Delete all consoles except the current console.
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then​​​​​​​ <kbd>K</kbd> |Kills the current console and the processes found there.
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then​​​​​​​ <kbd>X</kbd> |Closes the current session.

#### Various

|Combination |Description
|--- |---
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then​​​​​​​ <kbd>F</kbd> |Resize the window to the size of its terminal. Useful when you are several on the same console and you get angry at not having a 16:9 screen.
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then​​​​​​​ <kbd>H</kbd> |Log everything that goes on the screen in a file `$HOME/screenlog.XX` (XX: console number)
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then​​​​​​​ <kbd>?</kbd> |Display a little help

#### Move and Copy

|Combination |Description
|--- |---
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then​​​​​​​ <kbd>Esc</kbd> |Enter copy `/scrollback` mode. You can move around the screen with the directional arrows.
|<kbd>Enter</kbd> |Starts a selection. Press <kbd>Enter</kbd> again to complete the selection.
|<kbd>Ctrl</kbd> + <kbd>a</kbd> then​​​​​​​ <kbd>]</kbd> |Pastes the previously selected content.

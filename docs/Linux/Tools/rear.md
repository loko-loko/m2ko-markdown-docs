---
tags: [System, Backup, Tools]
title: Rear
---

# Tools - Rear

## Install

Installation of package:

```bash
yum install -y \
  rear \
  grub2-efi-x64.x86_64 \
  grub2-efi-x64-modules.noarch
```

## Config

Example of rear config file:

```bash
# /etc/rear/local.conf
OUTPUT=ISO
BACKUP=NETFS
OUTPUT_URL=file:///tmp/backup/
BACKUP_URL=iso:///tmp/backup/
```

## Usage

### Backup

Start backup:

```bash
rear -v mkbackup
```
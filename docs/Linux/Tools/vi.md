---
tags: [System, Tools, Editor]
title: Vi Editor
---

# Tools - Vi Editor

## Gestion Fichier

|Commands |Description
|--- |---
|<kbd>:w</kbd> |Sauvegarde le contenu du fichier (l'enregistrer), penser à write
|<kbd>:x</kbd> |Sauvegarde puis quitte vi (exit)
|<kbd>:wq</kbd> |Sauvegarde puis quitte (write and quit)
|<kbd>ZZ</kbd> |Sauvegarde si nécessaire puis quitte
|<kbd>:q</kbd> |Quitte vi sans sauvegarder les modifications (quit)
|<kbd>:e!</kbd> |Ignore les modifications et recharge le fichier (le point d'exclamation marque l'impératif)
|<kbd>:q!</kbd> |Quitte immédiatement, sans rien faire d'autre (donc sans sauvergarder ni demande de confirmation)
|<kbd>:w nom_fichier</kbd> |Sauvegarde le fichier sous le nom nom_fichier
|<kbd>:w! nom_fichier</kbd> |Remplace le contenu du fichier nom_fichier

## D&eacute;placement

### Dans la Page

|Commands |Description
|--- |---
|<kbd>Ctrl</kbd> + <kbd>f</kbd> |Descend d'une page (forward, en avant)
|<kbd>Ctrl</kbd> + <kbd>b</kbd> |Remonte d'une page (back, en arrière)
|<kbd>Ctrl</kbd> + <kbd>d</kbd> |Descend d'1/2 page
|<kbd>Ctrl</kbd> + <kbd>u</kbd> |Remonte d'1/2 page
|<kbd>:X</kbd> |Va à la ligne numérotée X (X est un entier)

### Dans le Fichier

|Commands |Description
|--- |---
|<kbd>H</kbd> |En haut de l'écran
|<kbd>M</kbd> |Au milieu de l'écran
|<kbd>L</kbd> |En bas de l'écran (lower, au plus bas)
|<kbd>h</kbd> |Décale d'un caractère à gauche
|<kbd>j</kbd> |Descend d'une ligne
|<kbd>k</kbd> |Monte d'une ligne
|<kbd>l</kbd> |Décale d'un caractère à droite
|<kbd>0</kbd> |Au début de la ligne
|<kbd>$</kbd> |A la fin de la ligne ('$', dans un motif de regexp, désigne la fin d'une ligne)
|<kbd>w</kbd> |Au début du mot suivant (word, mot)
|<kbd>e</kbd> |A la fin du mot suivant (end, fin)
|<kbd>b</kbd> |Recule d'un mot (back, retour)
|<kbd>{</kbd> |Recule jusqu'au paragraphe suivant
|<kbd>}</kbd> |Avance jusqu'au paragraphe précédent
|<kbd>(</kbd> |Recule jusqu'à la phrase suivante
|<kbd>)</kbd> |Avance jusqu'à la phrase précédente

## Insertion

|Commands |Description
|--- |---
|<kbd>i</kbd> |Active le mode insertion
|<kbd>a</kbd> |Active le mode insertion, un caractère après le curseur (append, ajouter)
|<kbd>I</kbd> |Insère au début de la ligne
|<kbd>A</kbd> |Insère à la fin de la ligne
|<kbd>O</kbd> |Insère une ligne au-dessus du curseur et passe en mode insertion (open, ouvrir)
|<kbd>o</kbd> |Insère une ligne en dessous du curseur et passe en mode insertion
|<kbd>Esc</kbd> |Quitte le mode insertion, revient en mode commande

## Remplacement

|Commands |Description
|--- |---
|<kbd>rx</kbd> |Remplace le caractère à la position du curseur par x (x remplace ici n'importe quel caractère)
|<kbd>R</kbd> |Remplace le caractère à la position du curseur par x (x remplace ici n'importe quel caractère)
|<kbd>cw</kbd> |Remplace uniquement le mot à la position du curseur (word, mot) ([ESC] pour terminer)
|<kbd>cnw</kbd> |Remplace n mots ([ESC] pour terminer)
|<kbd>C</kbd> |Remplace la ligne ([ESC] pour terminer)

# Suppression

|Commands |Description
|--- |---
|<kbd>x</kbd> |Supprime un caractère
|<kbd>dw</kbd> |Supprime un mot (delete, détruire)
|<kbd>dnw</kbd> |Supprime n mots
|<kbd>dd</kbd> |Supprime une ligne
|<kbd>ndd</kbd> |Supprime n lignes
|<kbd>:%d</kbd> |Supprime toutes les lignes

## Copier/Coller

### Local

|Commands |Description
|--- |---
|<kbd>Y</kbd> |Copie une ligne, donc la place dans un tampon, pour pouvoir ensuite la coller (yank, tirer)
|<kbd>nY</kbd> |Copie n lignes
|<kbd>P</kbd> |Colle les lignes avant le curseur (paste, coller)
|<kbd>p</kbd> |Colle les lignes après le curseur

## Autre Fichier

|Commands |Description
|--- |---
|<kbd>ma</kbd> |Marque la position "a" dans le fichier
|<kbd>mb</kbd> |Marque la position "b" dans le fichier
|<kbd>'a,'b nom_fichier</kbd> |Copie dans le fichier nommé nom_fichier le texte situé entre la position "a" et la position "b"

## Annuler/R&eacute;p&eacute;ter

|Commands |Description
|--- |---
|<kbd>u</kbd> |Annule la dernière modification (undo, défaire)
|<kbd>U</kbd> |Annule toutes les modifications effectuées sur la ligne courante
|<kbd>. (Point)</kbd> |Répète les dernières modifications


## Rechercher/Remplacer

|Commands |Description
|--- |---
|<kbd>/motif</kbd> |Recherche motif en allant vers la fin du document
|<kbd>?motif</kbd> |Recherche motif en allant vers le début du document
|<kbd>n</kbd> |Répète la dernière recherche (next, suivant)
|<kbd>N</kbd> |Retourne au résultat de la précédente recherche effectuée
|<kbd>//[RET]</kbd> |Répète la dernière recherche
|<kbd>:x,y s/motif/motif2/g</kbd> |Recherche le motif, en allant de la ligne numérotée x à la ligne y, et le remplace par motif2
|<kbd>:g/motif/s//motif2/g</kbd> |Recherche dans tous les fichiers le motif et la remplace par motif2 (global)
|<kbd>:g/motif/s//motif2/gc</kbd> |Idem, mais demande une confirmation avant de remplacer


---
tags: [Linux, System, Tools]
title: Udev
---

# Linux - Udev

## Commandes

Appliquer les règles udev:

```bash
udevadm trigger
```

Exemple de règle udev:

```bash
ACTION=="add|change", SUBSYSTEM=="block", ATTR{device/model}=="SYMMETRIX", ATTR{queue/max_sectors_kb}="128"
```


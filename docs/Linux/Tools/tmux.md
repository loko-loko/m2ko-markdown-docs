---
tags: [System, Tools, Multiplexer]
title: Tmux
---

# Tools - Tmux

## Install

Install tmux 3.* (`Centos`):

```bash
# Install centos endpoint repository
yum install https://packages.endpoint.com/rhel/7/os/x86_64/endpoint-repo-1.10-1.x86_64.rpm

# Install Tmux 3.*
yum install tmux
```

## Plugins

Install Tmux Plugin Manager:

```bash
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

Add following line from `tmux.conf` file:

```bash
# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'github_username/plugin_name#branch'
# set -g @plugin 'git@github.com:user/plugin'
# set -g @plugin 'git@bitbucket.com:user/plugin'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
```

Source it:

```bash
tmux source ~/.tmux.conf
```

Next, to install a plugin add plugin from `tmux.conf` file and source it with:

```bash
# Install
[suffix] + [I]

# Uninstall
[suffix] + [U]
```

## Config

Example of config:

```bash
# Bind appropriate commands similar to screen.
# lockscreen ^X x
unbind ^X
bind ^X lock-server
unbind x
bind x lock-server

# screen ^C c
unbind ^C
bind ^C new-window
unbind c
bind c new-window

# detach ^D d
unbind ^D
bind ^D detach

# displays *
unbind *
bind * list-clients

# next ^@ ^N sp n
unbind ^@
bind ^@ next-window
unbind ^N
bind ^N next-window
unbind " "
bind " " next-window
unbind n
bind n next-window

# title A
unbind A
bind A command-prompt "rename-window %%"

# other ^A
unbind ^A
bind ^A last-window

# prev ^H ^P p ^?
unbind ^H
bind ^H previous-window
unbind ^P
bind ^P previous-window
unbind p
bind p previous-window
unbind BSpace
bind BSpace previous-window

# windows "
unbind '"'
bind '"' list-windows

# quit \
unbind '\'
bind '\' confirm-before "kill-server"

# kill K k
unbind K
bind K confirm-before "kill-window"
unbind k
bind k confirm-before "kill-window"

# redisplay ^L l
unbind ^L
bind ^L refresh-client
unbind l
bind l refresh-client

# split -v |
unbind |
bind | split-window -h

# split -h -
unbind -
bind - split-window -v

# :kB: focus up
unbind Tab
bind Tab select-pane -t:.+
unbind BTab
bind BTab select-pane -t:.-

# " windowlist -b
unbind w
bind w choose-window


# On utlise control + flèches pour naviguer entre les terminaux
bind-key -n C-right next
bind-key -n C-left prev

# On utilise alt + flèches our naviguer entre les panels
bind-key -n M-left select-pane -L
bind-key -n M-right select-pane -R
bind-key -n M-up select-pane -U
bind-key -n M-down select-pane -D

# Les fenêtres commencent par 1 et non par 0
set -g base-index 1

##################################
# Changements visuels
##################################

# On met les panneaux non actif en gris
set -g pane-border-style fg=colour244,bg=default

# On met le panneau actif en rouge
set -g pane-active-border-style fg=colour124,bg=default

# On met la barre de status en gris
set -g status-style fg=black,bg=white

# On surligne les fenêtres actives dans la barre de status en gris foncés
set-window-option -g window-status-current-style fg=white,bg=black

# config synchronize-panes
# removes "*" from status-current, uses bg color for both current/others
bind C-x setw synchronize-panes
setw -g window-status-current-format '#{?pane_synchronized,#[bg=red],}#{?window_zoomed_flag,#[bg=blue],}#I:#W(#{E:window_panes})'
setw -g window-status-format         '#{?pane_synchronized,#[bg=red],}#{?window_zoomed_flag,#[bg=blue],}#I:#W(#{E:window_panes})'

# send tmux buffer to clipboard to windows
bind C-y run-shell -b "tmux show-buffer | xclip -selection clipboard -i"

### SAM'S TWEAKS #####

# VI mode
set-window-option -g mode-keys vi

# Fix issue with vim slow escape time
set -sg escape-time 10

# Increase history
set-option -g history-limit 50000

##################################
# TPM
##################################
# Tmux plugin manager
set -g @plugin 'tmux-plugins/tpm'

# List of plugins
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'roosta/tmux-fuzzback'
set -g @fuzzback-popup 1
set -g @fuzzback-bind s
set -g @fuzzback-popup-size '90%'
set -g @fuzzback-fzf-bind 'ctrl-y:execute-silent(echo -n {3..} | xsel -ib)+abort'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
```

!!! note

    This config required tmux 3.* + `xclip` package


---
tags: [Linux, System, Tools]
title: Utils
---

# Linux - Utils

## Some Utils Tools

### Fuzzy Search

Fuzzy Search is a usefull search bar.

Install from git:

```bash
FZF_VERSION=0.29.0

git clone --branch ${FZF_VERSION} --depth 1 --single-branch https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install --all
```

How to use it:

```bash
# Source .bashrc or .zshrc, and:
[ctrl] + [r]
```

### Meteo

Display meteo in ASCII:

```bash
curl wttr.in/{city}
# Example:
#  curl wttr.in/Palaiseau
#   Weather report: Palaiseau
#   
#                   Overcast
#          .--.     15 °C          
#       .-(    ).   ↙ 17 km/h      
#      (___.__)__)  9 km           
#                   0.1 mm         
#                                                          ┌─────────────┐                                                       
#   ┌──────────────────────────────┬───────────────────────┤  Mon 20 Jun ├───────────────────────┬──────────────────────────────┐
#   │            Morning           │             Noon      └──────┬──────┘     Evening           │             Night            │
#   ├──────────────────────────────┼──────────────────────────────┼──────────────────────────────┼──────────────────────────────┤
#   │               Overcast       │               Cloudy         │     \   /     Sunny          │     \   /     Sunny          │
#   │      .--.     17 °C          │      .--.     18 °C          │      .-.      +25(26) °C     │      .-.      20 °C          │
#   │   .-(    ).   ↓ 8-10 km/h    │   .-(    ).   ↓ 13-14 km/h   │   ― (   ) ―   ↙ 14-16 km/h   │   ― (   ) ―   ↙ 17-26 km/h   │
#   │  (___.__)__)  10 km          │  (___.__)__)  10 km          │      `-’      10 km          │      `-’      10 km          │
#   │               0.0 mm | 0%    │               0.0 mm | 0%    │     /   \     0.0 mm | 0%    │     /   \     0.0 mm | 0%    │
#   └──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘
```

---
tags: [System, Tools, Performance]
title: NMON
---

# Tools - NMON

## Commandes

Afficher les top processus:
```bash
nmon -t
```

## Stats

Lancer des stats sur un serveur:
```bash
nmon -F /tmp/$(uname -s).stat -A -T -I0.1 -d -t -P -K -M -N -V -L -^ -S -s 10 -c 720
```

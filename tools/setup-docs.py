import os
from jinja2 import Template


def _generate_index(docs_path: str):
    indexes = []
    for root, dirs, files in os.walk(os.path.join(docs_path)):
        f_root = root.replace(docs_path, "")
        sorted_files = sorted(files)

        if "_images" in root:
            continue

        indent_count = len(f_root.split("/")) - 1
        indent_suffix = "    " * indent_count + "- "
        b_root = os.path.basename(f_root)

        if not b_root:
            continue

        indexes.append(indent_suffix + b_root)

        for f in sorted_files:
            if f == "index.md":
                continue

            f_path = os.path.join(f_root, f).replace(".md", "")
            f_indent_suffix = "    " * (indent_count + 1) + "- "
            formatted_name = " ".join(
                [d.capitalize() for d in os.path.basename(f_path).split("_")]
            )

            indexes.append("%(suffix)s[%(name)s](%(href)s)" % dict(
                suffix=f_indent_suffix,
                name=formatted_name,
                href=f_path
            ))
    return indexes

def build_indexes(docs_path: str, template_path: str):
    # Get index data
    indexes = _generate_index(
        docs_path=docs_path
    )
    index_tpl_file = os.path.join(
        template_path,
        "index.md.j2"
    )

    # Read template
    with open(index_tpl_file, "r") as f:
        template = Template(f.read())

    # Build
    output = template.render(
        indexes=indexes
    )

    # Write templating file
    index_output_file = os.path.join(docs_path, "index.md")
    with open(index_output_file, "w") as f:
        f.write(output)


def main():

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    DOCS_DIR = os.path.join(BASE_DIR, "docs/")
    TEMPLATE_DIR = os.path.join(BASE_DIR, "templates/")

    build_indexes(
        docs_path=DOCS_DIR,
        template_path=TEMPLATE_DIR
    )

if __name__ == "__main__":
    main()


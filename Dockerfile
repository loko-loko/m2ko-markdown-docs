FROM python:3.12.0-slim-bullseye

WORKDIR /app

RUN pip install -U pip \
    && pip install \
         mkdocs==1.3.1 \
         mkdocs-material==8.2.11 \
         mkdocs-material-extensions==1.0.3

COPY entrypoint.sh /

RUN chmod 755 /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

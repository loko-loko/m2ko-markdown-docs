#!/bin/sh

DOCS_ARCHIVE="m2ko-markdown-docs-master"
DOCS_ARCHIVE_URL="https://gitlab.com/loko-loko/m2ko-markdown-docs/-/archive/master/${DOCS_ARCHIVE}.tar.gz"

# Download docs
echo "INFO: Download docs from: ${DOCS_ARCHIVE_URL}"
python3 -c "import urllib.request;urllib.request.urlretrieve('$DOCS_ARCHIVE_URL', '$DOCS_ARCHIVE.tar.gz')"

# Uncompress
echo "INFO: Uncompress docs archive"
tar xvzf "${DOCS_ARCHIVE}.tar.gz" > /dev/null 2>&1

# Move content to docs folder
echo "INFO: Move file to (/app/docs)"
mv ${DOCS_ARCHIVE}/* /app/
rm -rf "${DOCS_ARCHIVE}*"

echo "INFO: Build new index"
python3 /app/tools/setup-docs.py

# Build new docs
echo "INFO: Build website"
python3 -m mkdocs serve -a 0.0.0.0:3000

